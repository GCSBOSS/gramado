cls
deno test --fail-fast --allow-read --allow-write --coverage=coverage --unstable ./deno/test/reader-test.js ./deno/test/parser-test.js ./deno/test/scripter-test.js
deno coverage --unstable coverage
rm coverage/*