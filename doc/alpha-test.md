
# For Alpha Testers

## Instalation

1. Install [VSCode](https://code.visualstudio.com/download)
2. Install [Deno](https://github.com/denoland/deno/releases/latest)
3. Install the [gramado language extension](vscode:extension/GCSBOSS.gramado-language) on VSCode
4. Clone the One repo with

```
git clone https://gitlab.com/one-lang/one.git
```

## Running

1. Download binary from releases page
2. Run with `one eval my-file.one`
