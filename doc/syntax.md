
# One Syntax

This document is an official guide through the syntax of the One Language and
it's contructions.


## 1. Lexical structure
The One Language works with UTF-8 encoded files. The case of characters in source files is significant. All syntactic constructs except identifiers and certain literals may be separated by an arbitrary number of whitespace characters and comments. The whitespace characters are space and tab. Newlines works as whitespace only when expressions obviously continues to the next line.

Below you will find details on every lexical token of which One expressions are composed.

### 1.1 Identifiers

```one
foobar
oneIsSimple
```

One identifiers must start with an alphabet letter or underscore followed by zore or many alphabets, decimal digits, dash symbol, or underscore characters. There are no restrictions on the lengths of One identifiers.

### 1.2 Comments

```one
// this is a comment line

/* This is
a multi-line comment */
```

One uses C-style comments

Single-line comments start with `//` outside of a string and all following text until the end of the line.

Multi-line comment start with `/*` outside of a string and end with a `*/`.

### 1.3 Reserved words

The following words are reserverd and cannot be used as identifiers:

```one
break   count  downto  else    elsif  exit  false  for
from    fx     if      is      log    next  null   of
return  to     true    unless  until  var   while
```
### 1.4 Numeric Literals

Numeric values can be represented in any of the following forms:

- Integer: `123`
- Floating Point: `123.45`
- Fraction: `.25`

### 1.5 String Literals

```one
"this is a
    string expression"
"a + c = ${a + b}"
'My name is $myName'
" Break the line \r\n Done! "
```

String literals begin and end with double or single quote marks.

Whenever a character is preceded by `\` it's considered a scape sequence according to the same rules of ES scape sequences.

> Pending link to ES scape sequences above

The sequence `"${a + b}"` will cause the expression `a + b` to be evaluated and then joined to the string in the exact position where encountered.

The sequence `"$varname"` will cause the value of `varname` to be joined to the string in the exact position where encountered.

### 1.6 Regular Expression Literals

```one
/foo\ ba[rhz]/g
```

One regular expression literals mirror the [ES spec](https://tc39.es/ecma262/#sec-regexp-regular-expression-objects).

## 2. Program

```one
log "hello world"
```

One *programs* are sequence of statements. Each statement is delimited by a newline or EOF

Check below the existing statements and their rules.

### 2.1 Expressions

The most basic form of statements are expressions. Expressinos are any constructions that may result in a value.

Expressions may be composed of one or many sub-expressions.

Below you will find the various expressions allowed.

#### 2.1.1 Variables

```one
iAmAVariable
_meToo
```

Variables are identifiers that *represent data* and can be used in almost all constructions in the One language.

#### 2.1.2 Constant Keywords

These keywords function as constant identifiers in that they represent a specific immutable value across the entire language.

- `null` - Represents an undefined value
- `true` - Boolean yes
- `false` - Boolean no

#### 2.1.3 Arrays

```one
[1, 2, 3]
```

**Syntax:** `'[' [expr {',' expr} ] ']'`

Returns an `Array` ccontaining the results of each expression inside it.

This syntax closely mirrors the one of the [ES Spec](https://tc39.es/ecma262/#sec-array-initializer) with small exceptions.

#### 2.1.4 Objects

```one
{ a: 1, b: 'foo' }
```

**Syntax:** `'{' [ident ':' expr {',' ident ':' expr} ] '}'`

Returns an `Object` ccontaining the key and value pairs inside it.

This syntax mirrors the one of the [ES Spec](https://tc39.es/ecma262/#sec-object-initializer) with small exceptions.

#### 2.1.5 Binary Operations

```
'foo' + a * 3 / 4
```

**Syntax:** `expr (op expr)...`

Binary Operations performe system defined logic involving two values. Some operators have predecendy over others meaning they will always evaluate first in the expression.

The available operators and their precedences are described below:

| Precedence | Symbol | Operation |
|-|-|-|
| 1 | `\|` | Logical Or |
| 2 | `&` | Logical And |
| 3 | `>` | Lesser Than |
| 3 | `<` | Greateres Than |
| 3 | `<=` | Lesser or Equal to |
| 3 | `>=` | Greater or Equal to |
| 3 | `==` | Equals |
| 3 | `!=` | Different |
| 4 | `+` | Sum |
| 4 | `-` | Subtraction |
| 5 | `*` | Multiplication |
| 5 | `/` | Division |
| 6 | `??` | Coalesce |

#### 2.1.6 Property Access

```one
myVarFoo.itsPropBar
another['prop']
34.something
(a + b)[0]
```

**Syntax:** `expr'.'ident` or `expr'['expr']'`

Returns the value of a property inside an object.

#### 2.1.7 Assignments

```one
foo = 'bar'
foo[0] = bar
foo.bar = 23 + 45
```

**Syntax:** `(prop | ident) '=' expr`

Assignment expressions are used to assign the value of expressions in the right-hand side to the variable or property in the left-hand side.

#### 2.1.8 Self Assignments

```one
foo += 'bar'
foo -= .45
```

**Syntax:** `(prop | ident) ('-='|'+=') expr`

These assignments will essentially concat, sum or subtract the value in the right-hand side to the data in the left-hand side.

#### 2.1.9 Function

```one
fx scream(text)
    log text.toUpperCase()
```

**Syntax:** `'fx [ident] '(' [ ident [',' ident]...  ] ')' (block | '\n' stat)`

Creates a new function object.
Function name is optional.
A function may have one or more parameters.
A function must have a block or a single statement as it's body.

#### 2.1.10 Function Call

```one
foo.bar()
foo(a, b, c)
bar()
bar a, b, c
```

**Syntax:** `expr '(' [ expr (',' expr)... ] ')'` or `expr  expr (',' expr)...`

Function call expression invokes the function returnes by the left-hand side expression passing the values of any remaning expressions as arguments.

The left-hand side expression must be something that *can possibly* return a function.

#### 2.1.11 Arrow Function

```one
fx scream(text)
    log text.toUpperCase()
```

**Syntax:** `( ident | '(' [ ident [',' ident]...  ] ')' ) '=>' (block | '\n' stat | expr)`

Creates a new function object.
Similar to regular functions but can have no name and behave as a lexical scope.

#### 2.1.12 Ternary Operation

```one
side = isSith ? 'dark' : 'light'
side == 'dark' ? forcePull()
```

**Syntax:** `expr '?' expr [':' expr]`

Results in a different value depending on the initial condition expression.

Will return `null` in case a 'no' branch is not defined for a condition that evaluates to `false`.

#### 2.1.13 Unary NOT

```one
c = !cool
d = !!c
```

**Syntax:** `'!' expr`

Returns boolean opposite of the expression.

#### 2.1.14 Unary Minus

```one
a = -3
b = -a
```

**Syntax:** `'-' expr`

Returns expression but with inverted signal.

#### 2.1.15 Precedence Specifier

```one
a = (a + b) * c
c = (d)
```

**Syntax:** `'(' expr ')'`

Groups an expression to ensure it's evaluated before it's sorroundings.

#### 2.1.16 Log

```one
a = 'bill'
log a, b, c
log('My name is %s', a)
```

**Syntax:** `'log' '(' [ expr (',' expr)... ] ')'` or `'log'  expr (',' expr)...`

Output expressions to stdout or the debug console. Is equivalent of Javascript `console.log´.

#### 2.1.17 Type Checking

```one
a is number
b is string
c is null
```

**Syntax:** `expr 'is' ('string' | 'null' | 'empty' | 'date' | 'array' | 'object' | 'fx' | 'boolean' | 'number')`

Evaluates to boolean whether the expressino is of the given kind. Kinds behavior is listed below.

| word | behavior |
|-|-|
| string | Check for type `'string'` |
| number | Check for type `'number'` |
| object | Check for type `'object'` |
| boolean | Check for type `'boolean'` |
| fx | Check for type `'function'` |
| array | Check for `Array` object |
| date | Check for `Date` object |
| empty | Check for `null`, `undefined`, `''` or empty array |
| null | Check for `null` or `undefined` |

### 2.2 Blocks

```one
{
    a = 2
    foo.bar()
}
```

Blocks are a list of statements to be executed. Blocks never appear as a sole structure but rather are parts of larger structures such as functions or control structures.

### 2.3 Control Structures

The second type of statements are Control Structures. That allow the programmer to tap into the program flow as in structures programming.

#### 2.3.1 If

```one
if age >= 12
    log "adult fee\n"
else
    log "child fee\n"
```

**Syntax:** `'if' expr (block | '\n' stat) [ 'elsif' expr (block | '\n' stat) ]... [ 'else' (block | '\n' stat) ]`

If statements are used for conditional or branched execution.

Notice One uses `elsif`, not `else if` nor `elif`.

#### 2.3.2 Unless

```one
unless imABaby
    feed('meat')
else
    feed 'milk'
```

**Syntax:** `'unless' expr (block | '\n' stat) [ 'else' (block | '\n' stat) ]`

Unless expressions are used for reverse conditional execution. It is equivalent to:

```one
if !(cond)
    ...
else
    ...
```

#### 2.3.3 While

```one
while sunshine {
	work()
	rest()
}
```

**Syntax:** `'while' expr (block | '\n' stat)`

Executes body while condition expression returns true.

#### 2.3.4 Until

```one
until sunshine {
	sleep()
	snore()
}
```

**Syntax:** `'until' expr (block | '\n' stat)`

Executes body until condition expression returns true.

#### 2.3.5 For

```one
for k: v of someArray
    doSomething(k)

for k: of someObject
    doSomething(k)

for v of someArray
    doSomething k

for k: v of someArray count i
    log i
```

**Syntax:** `'for' [ident ':'][ident] 'of' expr ['count' ident] (block | '\n' stat)`

Run the block for every item in the array or object, exposes the iteration key, value and count, all optional, yet one of key or value must be exposed.

#### 2.3.6 From

```one
from 1 to 100 count i
    log i

from a downto 100
    a += 1
```

**Syntax:** `'from' expr ('to' | 'downto') expr ['count' ident] (block | '\n' stat)`

Run the block for the number of times equals the difference between the first and the second expressions.


#### 2.3.7 Modifiers

```one
cry() if baby
walk() unless tired
work() while awake
sleep() until sunshine
call n for n of names
gas i from 0 to 100 count i
```

All control structures can be used in a one-line *modifier* form.

### 2.4 Break

```one
while a > b {
    break if a == 20
    a += 2
}
```

**Syntax:** `'break'`

Immediately interrupts a loop.

### 2.5 Next

```one
while a > b {
    a += 1
    next if a / 2 == 0
    log a
}
```

**Syntax:** `'next'`

Jump to the next iteration of the loop.

### 2.6 Return


```one
fx sum(a, b)
    return a + b
```

**Syntax:** `'return' expr`

Interrupt a function and define it's result.

Only allowed inside a function body.

Must have a value.

### 2.6 Exit

```one
fx sum(a, b)
    exit
```

**Syntax:** `'exit'`

Interrupt a function with an undefined result.

### 2.6 Variable Definition

```one
var a, b = 3, c
```

**Syntax:** `'var' ident [ '=' expr ] (',' ident [ '=' expr ])...`

Declare the gven variables to be used in the current scope.
In the One language, variables cannot be used unless they are first declared using `var`.
