
export interface ASTReader<I, T> {

    OPER(token: I): T,
    ASSIGN(token: I): T,
    IF(token: I): T,
    FOR(token: I): T,
    UNLESS(token: I): T,
    FROM(token: I): T,
    WHILE(token: I): T,
    UNTIL(token: I): T,
    COND(token: I): T,
    LIST(token: I): T,
    CALL(token: I): T,
    SCALL(token: I): T,
    ARRAY(token: I): T,
    OBJECT(token: I): T,
    IS(token: I): T,
    ADD(token: I): T,
    SUB(token: I): T,
    NOT(token: I): T,
    MINUS(token: I): T,
    PROP(token: I): T,
    INDEX(token: I): T,
    VAR(token: I): T,
    AFX(token: I): T,
    RETURN(token: I): T,
    LOG(token: I): T,
    FX(token: I): T,
    PAIR(token: I): T,
    STRING(token: I): T,

    getType(token: I): string,
    getValue(token: I): T,

    expand(token?: I): T,
    statements(tokens?: I[]): T[]
}

type TransformerSpec<I, T> = Record<string, (token: I) => T>;

export abstract class Transformer<I, T> {

    spec: TransformerSpec<I, T>;
    statementList: T[][] = [];
    functionContext: boolean[] = [];

    constructor(){
        this.spec = this as unknown as TransformerSpec<I, T>;
    }

    abstract getType(something: unknown): string;
    abstract getValue(token: I): T;

    expand(token?: I): T{
        if(!token)
            throw new Error('Invalid token');

        const type = this.getType(token);
        if(type in this.spec)
            return this.spec[type].call(this, token);

        return this.getValue(token);
    }

    statements(tokens: I[]): T[]{
        const list: T[] = [];
        this.statementList.push(list);

        for(const t of tokens){
            if(this.getType(t) == 'EOF')
                break;
            list.push(this.expand(t));
        }

        this.statementList.pop();
        return list;
    }

}
