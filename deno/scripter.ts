import { ONode, OLeave } from './lex.ts';
import { OOp, OList, OUnary, OPair, OCall, OCondBlock, OIf, OFx, OAFx,
    OTernary, OAccess, OFor, OFrom } from './syn.ts';
import { Transformer, ASTReader } from './transformer.ts';

export class DenoTranspiler extends Transformer<ONode, string> implements ASTReader<ONode, string> {

    needIsNull = false;
    needIsEmpty = false;
    needHalt = false;

    getType(token: ONode): string{
        return token.type;
    }

    getValue(token: OLeave): string {
        return token.value;
    }

    OPER(token: OOp): string{
        const rename: Record<string, string> = { '|': '||', '&': '&&' };
        return this.expand(token.left) + (rename[token.op] || token.op)
            + this.expand(token.right);
    }

    ASSIGN(token: OOp): string {
        return this.expand(token.left) + ' = ' + this.expand(token.right);
    }

    LIST(token: OList): string {
        return '(' + token.items.map((i: ONode) =>
            this.expand(i)).join(', ') + ')';
    }

    ADD(token: OOp): string {
        return this.expand(token.left) + ' += ' + this.expand(token.right);
    }

    SUB(token: OOp): string {
        return this.expand(token.left) + ' -= ' + this.expand(token.right);
    }

    NOT(token: OUnary): string {
        return '!(' + this.expand(token.subject) + ')';
    }

    MINUS(token: OUnary): string {
        return ' -(' + this.expand(token.subject) + ')';
    }

    PROP(token: OAccess): string {
        const p = token.subject.type == 'number'
            ? '(' + this.expand(token.subject) + ')'
            : this.expand(token.subject);
        return p + '.' + this.expand(token.property);
    }

    INDEX(token: OAccess): string {
        return this.expand(token.subject) + '[' + this.expand(token.property) + ']';
    }

    RETURN(token: OUnary): string {
        return 'return ' + this.expand(token.subject);
    }

    ARRAY(token: OList): string {
        return '[ ' + token.items.map((i: ONode) =>
            this.expand(i)).join(', ') + ' ]';
    }

    PAIR(token: OPair): string {
        const key = [ 'word', 'number', 'string' ].includes(token.key.type)
            ? this.expand(token.key)
            : '[' + this.expand(token.key) + ']';

        return key + ': ' + this.expand(token.val);
    }

    OBJECT(token: OList): string {
        return '({ ' + token.items.map((i: ONode) =>
            this.expand(i)).join(', ') + ' })';
    }

    LOG(token: OList): string {
        return 'console.log(' + token.items.map((i: ONode) =>
            this.expand(i)).join(', ') + ')';
    }

    CALL(token: OCall): string {
        return this.expand(token.subject) + '(' + token.arguments!.map((i: ONode) =>
            this.expand(i)).join(', ') + ')';
    }

    SCALL(token: OCall): string {
        return this.expand(token.subject) + '(' + token.arguments!.map((i: ONode) =>
            this.expand(i)).join(', ') + ')';
    }

    WHILE(token: OCondBlock): string {
        return ' while(' + this.expand(token.cond) + '){' +
            this.statements(token.block).join('; ') + '}'
    }

    UNTIL(token: OCondBlock): string {
        return ' while(!(' + this.expand(token.cond) + ')){' +
            this.statements(token.block).join(';') + '}'
    }

    VAR(token: OList): string {
        return ' var ' + token.items.map((i: ONode) =>
            this.expand(i)).join(', ');
    }

    UNLESS(token: OCondBlock): string {
        return ' if(!(' + this.expand(token.cond) + ')){' +
            this.statements(token.block).join(';') + '}'
    }

    IF(token: OIf): string {

        let r = ' if(' + this.expand(token.cond) + '){' +
            this.statements(token.block).join(';') + '}';

        if(token.elsifs)
            for(const ei of token.elsifs)
                r += ' else if(' + this.expand(ei.cond) + '){' +
                    this.statements(ei.block).join(';') + '}';

        if(token.elseBlock)
            r += ' else {' + this.statements(token.elseBlock).join(';') + '}';

        return r;
    }

    FX(token: OFx): string {
        let r = ' function';

        if(token.name)
            r += ' ' + this.expand(token.name);

        r += '(' + token.items!.map((i: ONode) =>
            this.expand(i)).join(', ') + ')';

        this.functionContext.unshift(true);
        r += '{ ' + this.statements(token.block).join(';') + ' }';
        this.functionContext.shift();

        return r;
    }

    FOR(token: OFor): string {
        let r = 'const $$subject = ' + this.expand(token.subject) + '; ';

        if(token.count)
            r += 'let ' + token.count.value + ' = -1; ';

        r += ' for(';

        r += token.key
            ? 'let ' + token.key.value
            : 'const $$fakeKey';

        r += ' in $$subject){';

        if(token.val)
            r += 'let ' + token.val.value + ' = $$subject[' +
                (token.key ? token.key.value : '$$fakeKey') + ']; '

        if(token.count)
            r += token.count.value + '++; ';

        return r + this.statements(token.block).join(';') + '}';
    }

    IS(token: OOp): string {

        const left = this.expand(token.left);
        const right = token.right as OLeave;

        if(right.value == 'null'){
            this.needIsNull = true;
            return '$$isNull(' + left + ')';
        }

        if(right.value == 'empty'){
            this.needIsEmpty = true;
            return '$$isEmpty(' + left + ')';
        }

        if(right.value == 'array')
            return 'Array.isArray(' + left + ')';

        // if(token.right!.value == 'date')
            // return '(' + left + ' instanceof Date)';

        if(right.value == 'string')
            return '(typeof ' + left + ' == "string")';

        if(right.value == 'fx')
            return '(typeof ' + left + ' == "function")';

        if(right.value == 'boolean')
            return '(typeof ' + left + ' == "boolean")';

        // if(right.value == 'number')
        return '(typeof ' + left + ' == "number")';

        // Should never get here
        // return 'false';
    }

    STRING(token: OList): string {
        const items = token.items as OLeave[];

        let r = '`' + items[0].value + '`';

        const remaining = items.slice(1, items.length);

        for(const i of remaining){
            r += i.type == 'string' || i.type == 'scape'
                ? '+`' + i.value + '`'
                : '+(' + this.expand(i) + ')';
        }

        return r;
    }

    FROM(token: OFrom): string {

        const cn = token.count ? token.count.value : '$$count';
        const e = this.expand(token.end);

        let r = 'for(let ' + cn + ' = ' + this.expand(token.start) + '; ' + cn;

        r += token.op == 'downto'
            ? ' >= ' + e + '; ' + cn + '--'
            : ' <= ' + e + '; ' + cn + '++';

        return r + '){' + this.statements(token.block).join(';') + '}';
    }

    AFX(token: OAFx): string {

        let r = '(' + token.items.map((i: ONode) =>
            this.expand(i)).join(', ') + ') => ';

        if(token.block){
            this.functionContext.unshift(true);
            r += '{ ' + this.statements(token.block).join(';') + ' }';
            this.functionContext.shift();
        }
        else if(token.subject)
            r = '(' + r + this.expand(token.subject) + ')';

        return r;
    }

    COND(token: OTernary): string {
        let r = this.expand(token.cond) + ' ? ' + this.expand(token.left) + ' : ';
        r += token.right ? this.expand(token.right) : 'null';
        return r;
    }

    kwExit(): string {
        if(!this.functionContext[0])
            this.needHalt = true;
        return 'return;';
    }

    transpile(program: ONode[]): Promise<string> {
        let main = this.statements(program).join(';');

        if(this.needIsEmpty)
            main = `const $$isEmpty = v => v === null || typeof v == 'undefined'
                        || v === '' || (Array.isArray(v) && v.length == 0);` + main;

        if(this.needIsNull)
            main = `const $$isNull = v => v === null || typeof v == 'undefined';` + main;

        if(this.needHalt)
            main = '(function(){' + main + '})()';

        return Promise.resolve(main);
    }
}

type Rec = Record<string, unknown>;

export async function transpile(program: ONode[]): Promise<string>{
    return await new DenoTranspiler().transpile(program);
}
