
// const charToInteger: { [char: string]: number } = {};
// const integerToChar: { [integer: number]: string } = {};

// 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/='
// 	.split('')
// 	.forEach(function(char, i) {
// 		charToInteger[char] = i;
// 		integerToChar[i] = char;
// 	});

// export function encode(value: number | number[]): string {
// 	let result: string;

// 	if (typeof value === 'number') {
// 		result = encodeInteger(value);
// 	} else {
// 		result = '';
// 		for (let i = 0; i < value.length; i += 1) {
// 			result += encodeInteger(value[i]);
// 		}
// 	}

// 	return result;
// }

// function encodeInteger(num: number): string {
// 	let result = '';

// 	if (num < 0) {
// 		num = (-num << 1) | 1;
// 	} else {
// 		num <<= 1;
// 	}

// 	do {
// 		let clamped = num & 31;
// 		num >>>= 5;

// 		if (num > 0) {
// 			clamped |= 32;
// 		}

// 		result += integerToChar[clamped];
// 	} while (num > 0);

// 	return result;
// }

// type Location = {
//     col: number,
//     line: number,
// 	file: string
// };

// export class SourceMap {

// 	mappings = '';
//     sourcesContent: string[] = [];

//     sources: string[] = [];
// 	sourcesIndex: Record<string, number> = {};

//     names: string[] = [];
// 	namesIndex: Record<string, number> = {};
// 	index = 0;

//     add(source: Location, generated: number, name?: string){
// 		let m = encode(this.index);
// 		this.index += generated;

// 		if(!(source.file in this.sourcesIndex))
// 			this.sourcesIndex[source.file] = this.sources.push(source.file) - 1;
// 		m += encode(this.sourcesIndex[source.file]);

// 		m += encode(source.line);
// 		m += encode(source.col);

// 		if(name){
// 			if(!(name in this.namesIndex))
// 				this.namesIndex[name] = this.names.push(name) - 1;
// 			m += encode(this.namesIndex[name]);
// 		}

// 		this.mappings += m + ',';
//     }

// 	break(){
// 		this.mappings += ';';
// 		this.index = 0;
// 	}

// }

// Deno.test('source-map generation', function(){

// 	/*
// 		let a = 3
// 		console.log("abc")
// 	*/

// 	let sm = new SourceMap();
// 	sm.add({ col: 0, line: 1, file: './main.js' }, 3);
// 	sm.add({ col: 4, line: 1, file: './main.js' }, 1, 'a');
// 	sm.add({ col: 6, line: 1, file: './main.js' }, 1);
// 	sm.add({ col: 8, line: 1, file: './main.js' }, 1);
// 	sm.add({ col: 0, line: 2, file: './main.js' }, 7, 'console');
// 	sm.add({ col: 7, line: 2, file: './main.js' }, 1);
// 	sm.add({ col: 8, line: 2, file: './main.js' }, 3, 'log');
// 	sm.add({ col: 11, line: 2, file: './main.js' }, 1);
// 	sm.add({ col: 12, line: 2, file: './main.js' }, 5);
// 	sm.add({ col: 17, line: 2, file: './main.js' }, 1);
// 	sm.break();

// 	console.log(sm.mappings);

// });