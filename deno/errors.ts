
import { ONode, ParserState } from './lex.ts';

/*

Error message UX
    > probably belongs to another phase of the pipeline
    > ie.: we should output JS objects for now since this
    > code will be used inside other code rather than face a
    > human directly

---

Hi! I could not parse some of the code.

--- my-file/path.gr ----------------------------

In this line state what the problem found was (computer talking here)

23|      This is = 'a raw line of code';
              ^^^^

- This is 1 solution or insight that might help the user

- This is the next solution

    > We can have nested solutions for ease

*/

export type ParseError = {
    stack: string[],
    code: number,
    type: string,
    impostor: ONode,
    data: ParserState
}

const ERROR_CODES: Record<string, number> = {
    unfinishedString: 1000,
    missingEncloser: 1001,
    missingKeyClosingBracket: 1002,
    missingArrayClosing: 1003,
    unexpectedObjectKey: 1004,
    missingObjectClosing: 1005,
    unknownIsParameter: 1006,
    missingIndexClosing: 1007,
    unexpectedSpaceAfterDot: 1008,
    unexpectedTokenAfterDot: 1009,
    missingVarWord: 1010,
    unexpected: 1011,
    invalidOperand: 1012,
    missingCallSeparator: 1013,
    missingBlockOpen: 1014,
    missingInterpolationClose: 1015,
    unexpectedParam: 1016,
    missingForSpecifier: 1017,
    missingOfKeyword: 1018,
    missingForCounter: 1019,
    missingTo: 1020,
    missingLogArguments: 1021,
    returnNotAllowed: 1022,
    breakNotAllowed: 1023,
    nextNotAllowed: 1024
};

export function createError(data: ParserState, type: string, impostor: ONode){
    return {
        stack: new Error().stack!.split(/\r?\n\ +/g).slice(1, -1),
        code: ERROR_CODES[type], type, impostor, data
    }
}

function formatParseError(err: ParseError){

    // TODO In this line state what the problem found was (computer talking here)
    let o = err.code + ': ' + err.type + '\n\n';

    o += (err.impostor.startLine! + 1) + '| ' +
        err.data.reader.lines[err.impostor.startLine] + '\n  ' +
        ''.padEnd(String(err.impostor.startLine + 1).length, ' ') +
        ''.padEnd(err.impostor.startCol!, ' ') + '\u001b[38;5;203m' +
        ''.padEnd(err.impostor.endCol - err.impostor.startCol, '^') +
        '\u001b[0m\n\n';

    //     - This is 1 solution or insight that might help the user
    //     - This is the next solution
    //         > We can have nested solutions for ease

    return o;
}

export function formatParseErrors(errors: ParseError[]): string{

    if(!Array.isArray(errors))
        throw errors;

    let o = '\n\u001b[38;5;153mHi! I could not parse some of the code.\u001b[0m\n\n';

    for(const err of errors)
        o += '------------------------------------------------\n\n' +
            formatParseError(err);

    return o;
}