import { SourceReader } from './reader.ts'

type ExtraData = {
    endLine: number,
    startLine: number,
    endCol: number,
    startCol: number
}

export interface ONode {
    type: string,
    startLine: number,
    startCol: number,
    endLine: number,
    endCol: number,
}

export interface OLeave extends ONode {
    value: string,
    prev: OLeave | undefined,
    spaceBefore?: boolean,
    spaceAfter?: boolean,
    breakBefore?: boolean,
    breakAfter?: boolean,
}

export interface OOperator extends OLeave {
    level: number,
    op: string,
    operator: boolean
}

export type ParserState = {
    stringMode: string,
    stringDelimiter: string,
    stackedTokens: OLeave[],
    line: number,
    col: number,
    tokens: OLeave[],
    statements: ONode[],
    lastFinalToken?: OLeave,
    lastToken?: OLeave,
    returnStack: boolean[],
    loopStack: boolean[],
    justBroke?: boolean,
    errors: Array<Record<string, unknown>>,
    reader: SourceReader
}

const KEYWORDS = Object.create(null);

export function registerKeywords(...words: string[]){
    for(const w of words)
        KEYWORDS[w] = 1;
}

/*
    app: 1, await: 1, case: 1, catch: 1, component: 1, const: 1,
    delete: 1, do: 1, export: 1, finally: 1, then: 1,
    import: 1, in: 1, switch: 1, this: 1, throw: 1, try: 1, view: 1
*/

const DOUBLE_SYMBOLS = [ '??', '==', '!=', '>=', '<=', '=>', '+=', '-=' ];

const OPERATORS: { [key: string]: number } = {
    '|': 1,
    '&': 2,
    '>': 3, '<': 3, '==': 3, '>=': 3, '<=': 3, '!=': 3,
    '+': 5, '-': 5,
    '*': 6, '/': 6,
    '??': 6.5
};

async function pushToken(data: ParserState, type: string, value: string, extra?: ExtraData): Promise<OLeave>{
    const t: OLeave = { type, value,
        startCol: data.col, startLine: data.line,
        endLine: data.line, endCol: data.col + value.length - 1,
        prev: data.lastFinalToken
    };

    data.col += value.length;

    if(extra){
        Object.assign(t, extra);
        data.col = extra.endCol;
        data.line = extra.endLine;
    }

    if(t.type != 'lineBreak' && t.type != 'space'){
        if(!data.lastFinalToken){
            t.breakBefore = true;
            t.spaceBefore = true;
        }

        data.lastFinalToken = t;

        if(data.lastToken?.type == 'space')
            t.spaceBefore = true;

        const n = await data.reader.lookahead();
        if('\r\n\ \t'.indexOf(n) > -1)
            t.spaceAfter = true;

        if(data.justBroke){
            t.breakBefore = true;
            t.spaceBefore = true;
            data.justBroke = false;
        }
    }

    data.lastToken = t;

    data.tokens.push(t);
    return t;
}

async function scanLineBreak(data: ParserState){
    let value = '';
    let c = await data.reader.advance();
    while(c == '\n' || c == '\r'){
        if(c == '\r' && await data.reader.lookahead() != '\n' || c == '\n')
            data.line++;

        value += c;
        c = await data.reader.advance();
    }
    data.reader.backtrack();
    data.justBroke = true;
    if(data.lastFinalToken){
        data.lastFinalToken.breakAfter = true;
        data.lastFinalToken.spaceAfter = true;
    }

    data.col = -value.length;

    pushToken(data, 'lineBreak', value);
}

function isWordMaterial(p: string){
    return p == '-' || p == '_' || (p >= 'a' && p <= 'z') || (p >= 'A' && p <= 'Z') || (p >= '0' && p <= '9');
}

function isWordStarter(p: string){
    return p == '_' || (p >= 'a' && p <= 'z') || (p >= 'A' && p <= 'Z');
}

async function scanWord(data: ParserState): Promise<OLeave>{

    let value = '';
    let c = await data.reader.advance();
    while(isWordMaterial(c)){
        value += c;
        c = await data.reader.advance();
    }

    data.reader.backtrack();

    const type = value in KEYWORDS
        ? 'kw' + value.charAt(0).toUpperCase() + value.slice(1)
        : 'word';

    return await pushToken(data, type, value);
}

async function scanScapeSeq(data: ParserState): Promise<OLeave>{
    // TODO add special multi character escapes (u x ...)
    return pushToken(data, 'scape', await data.reader.advance() + await data.reader.advance());
}

async function scanInterpolation(data: ParserState): Promise<OLeave>{
    // Consumes $
    await data.reader.advance();
    const c2 = await data.reader.lookahead();

    if(c2 == '{'){
        // Consumes {
        await data.reader.advance();
        data.stringMode = 'inter';
        return await pushToken(data, 'concat', '${');
    }

    data.stringMode = 'word';
    return await pushToken(data, 'concat', '$');
}

async function scanCloseInterpolation(data: ParserState): Promise<OLeave> {
    data.stringMode = 'string';
    return await pushToken(data, 'concat', await data.reader.advance());
}

async function scanString(data: ParserState): Promise<OLeave>{
    const extra = { endCol: data.col, endLine: data.line,
        startLine: data.line, startCol: data.col };

    let c = await data.reader.advance();
    let value = '';
    let delimit;

    if(data.stringMode)
        delimit = data.stringDelimiter;
    else {
        extra.endCol++;
        data.stringDelimiter = delimit = value = c;
        c = await data.reader.advance();
    }

    while( c != delimit ){

        if(!c)
            return await pushToken(data, '!badString', value, extra);

        const c2 = await data.reader.lookahead();
        if(c == '\\' ||
        (delimit == '"' && c == '$' && (isWordStarter(c2) || c2 == '{'))){
            data.stringMode = 'string';
            data.reader.backtrack();
            return await pushToken(data, 'string', value, extra);
        }

        if(c == '\r' && await data.reader.lookahead() != '\n' || c == '\n'){
            extra.endCol = -1;
            extra.endLine++;
        }

        value += c;
        c = await data.reader.advance();
        extra.endCol++;
    }

    data.stringMode = '';
    value += c;
    return await pushToken(data, 'string', value, extra);
}

async function scanComment(data: ParserState){

    const c1 = await data.reader.advance();
    const c2 = await data.reader.advance();

    if(c1 + c2 == '//'){

        let value = '';
        let c = await data.reader.advance();
        while(c != '\r' && c != '\n'){
            value += c;
            c = await data.reader.advance();
        }

        await data.reader.backtrack();

        pushToken(data, 'comment', value);
    }

    else if(c1 + c2 == '/*'){
        let value = '/*';
        const extra = { endLine: data.line, endCol: data.col,
            startLine: data.line, startCol: data.col };

        while( await data.reader.lookahead(2) != '*/' ){
            const c = await data.reader.advance();

            if(c == '\r' && await data.reader.lookahead() != '\n' || c == '\n'){
                extra.endCol = 0;
                extra.endLine++;
            }

            if(!c)
                return pushToken(data, 'bad comment', value, extra);

            extra.endCol++;
            value += c;
        }

        pushToken(data, 'comment', value + await data.reader.advance() + await data.reader.advance(), extra);
    }

}

function isNum(p: string){
    return p >= '0' && p <= '9';
}

async function scanNumericSeq(data: ParserState): Promise<string>{

    let value = '';
    let c = await data.reader.advance();
    while(isNum(c)){
        value += c;
        c = await data.reader.advance();
    }

    await data.reader.backtrack();

    return value;
}

async function scanRegexp(data: ParserState): Promise<OLeave>{

    let r = await data.reader.advance();
    let c = await data.reader.advance();

    while(c != '/'){
        if(c == '\\'){
            r += c;
            c = await data.reader.advance();
        }
        r += c;
        c = await data.reader.advance();
    }

    r += c;

    c = await data.reader.advance();
    if('dgimsuy'.indexOf(c) > -1)
        while('dgimsuy'.indexOf(c) > -1){
            r += c;
            c = await data.reader.advance();
        }

    data.reader.backtrack();

    return pushToken(data, 'regexp', r);
}

async function scanSymbol(data: ParserState): Promise<OLeave>{
    const [ c1, c2 ] = await data.reader.lookahead(2);

    if(c1 + c2 == '/*' || c1 + c2 == '//'){
        await scanComment(data);
        return scanUnknown(data);
    }

    if(c1 == '.' && isNum(c2))
        return pushToken(data, 'number', await data.reader.advance() + scanNumericSeq(data));

    const lt = data.lastFinalToken;
    if(c1 == '/' && (!lt || (lt as OOperator).operator || [ 'sym !', 'sym (', 'sym }', 'syn {',
        'sym [', 'sym ,', 'sym =' ].includes(lt.type)))
        return scanRegexp(data);

    let s = await data.reader.advance();
    if(DOUBLE_SYMBOLS.includes(c1 + c2))
        s += await data.reader.advance();

    const t = await pushToken(data, 'sym ' + s, s);
    if(t.value as string in OPERATORS){
        (t as OOperator).operator = true;
        (t as OOperator).level = OPERATORS[t.value as string];
    }

    return t;
}

async function scanNumber(data: ParserState): Promise<OLeave>{
    let string = await scanNumericSeq(data);

    const [ c1, c2 ] = await data.reader.lookahead(2);
    if(c1 == '.' && isNum(c2)){
        string += await data.reader.advance();
        string += await scanNumericSeq(data);
    }

    return pushToken(data, 'number', string);
}

async function scanSpace(data: ParserState){
    let value = '';
    let c = await data.reader.advance();

    while(c == ' ' || c == '\t'){
        value += c;
        c = await data.reader.advance();
    }
    data.reader.backtrack();
    if(data.lastFinalToken)
        data.lastFinalToken.spaceAfter = true;

    pushToken(data, 'space', value);
}

async function scanUnknown(data: ParserState): Promise<OLeave> {
    const char = await data.reader.lookahead();

    if(!char)
        return pushToken(data, 'EOF', '');

    if(data.stringMode == 'string'){
        if(char == '\\')
            return scanScapeSeq(data);

        if(data.stringDelimiter == '"' && char == '$')
            return scanInterpolation(data);

        // Middle and closing string
        return scanString(data);
    }

    if(data.stringMode == 'inter' && char == '}')
        return scanCloseInterpolation(data);

    if(data.stringMode == 'word'){
        data.stringMode = 'done-word';
        return scanWord(data);
    }

    if(data.stringMode == 'done-word'){
        data.stringMode = 'string';
        return pushToken(data, 'concat', '');
    }

    // number
    if('0123456789'.indexOf(char) > -1)
        return scanNumber(data);

    // word (letter or undercore)
    if(isWordStarter(char))
        return scanWord(data);

    // string (quotes)
    if(char == '\'' || char == '"')
        return scanString(data);

    // symbols
    if('!*/+-=)(}{][,.<>?:|&'.indexOf(char) > -1)
        return scanSymbol(data);

    // new line
    if(char == '\n' || char == '\r'){
        await scanLineBreak(data);
        return scanUnknown(data);
    }

    if(char == ' ' || char == '\t'){
        await scanSpace(data);
        return scanUnknown(data);
    }

    return pushToken(data, 'invalid', await data.reader.advance());
}

export async function nextToken(data: ParserState): Promise<OLeave> {
    return data.stackedTokens.pop() || await scanUnknown(data);
}

export function returnToken(data: ParserState, t: OLeave){
    data.stackedTokens.push(t);
}