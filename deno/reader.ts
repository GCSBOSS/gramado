
export interface SourceReader {
    lines: string[];
    advance(): Promise<string>,
    lookahead(amount?: number): Promise<string>,
    backtrack(amount?: number): void
}

export class StringSourceReader implements SourceReader {

    source: string;
    index: number;
    lines: string[];

    constructor(source: string){
        this.source = source;
        this.index = 0;
        this.lines = source.split(/(?:\r\n?|\n)/g);
    }

    advance() {
        this.index++
        return Promise.resolve(this.source[this.index - 1]);
    }

    backtrack(amount = 1) {
        this.index -= amount;
    }

    lookahead(amount = 1) {
        return Promise.resolve(amount == 1
            ? this.source[this.index]
            : this.source.substr(this.index, amount));
    }

}

type FileSourceReaderOptions = {
    chunkSize?: number
};

const CHUNK_SIZE = 2e3; // bytes

const A = 0b10000000;
const C = 0b11100000;
const D = 0b11110000;
const E = 0b11111000;
const F = 0b00111111;

function getCharLen(byte: number) {
    if ((byte & A) === 0b00000000) return 0;
    if ((byte & C) === 0b11000000) return 1;
    if ((byte & D) === 0b11100000) return 2;
    if ((byte & E) === 0b11110000) return 3;
    throw new Error('Fatal: Failed reading UTF-8 file');
}

export class FileSourceReader implements SourceReader {

    file?: Deno.File;
    expected = 0;
    bytesRead = 0;
    done = false;
    lines: string[] = [];
    path: string;
    buffer: Uint8Array;
    chunkSize: number;
    queue: string[] = [];
    consumed: string[] = [];

    constructor(path: string, opts: FileSourceReaderOptions = {}){
        this.path = path;
        this.chunkSize = opts.chunkSize ?? CHUNK_SIZE;
        this.buffer = new Uint8Array(this.chunkSize);
    }

    private async read(){

        if(this.done)
            return;

        if(!this.file)
            this.file = await Deno.open(this.path, { read: true });

        let chunkLen = await this.file.read(this.buffer) || 0;

        if(chunkLen < this.chunkSize){
            this.file.close();
            this.done = true;
        }

        let index = 0, expected = 0, codePoint = 0;
        let lastBreak = 0;
        let lastByte = 0, byte = 0;

        while(index < chunkLen){
            this.bytesRead++;
            lastByte = byte;
            byte = this.buffer[index];
            index++;

            if(expected === 0){
                expected = getCharLen(byte);

                if(expected === 0) {
                    this.queue.unshift(String.fromCharCode(byte));

                    // Ensure \n after \r don't disappear
                    if(lastByte == 13 && byte == 10)
                        this.lines[this.lines.length - 1] += '\n';

                    // Only pushes lines for \n if not following \r
                    if(byte == 13 || byte == 10 && lastByte != 13){
                        const slice = this.queue.slice(0, this.queue.length - lastBreak);
                        this.lines.push(slice.reverse().join(''));
                        lastBreak += slice.length + 1;
                    }

                    codePoint = 0;
                    continue;
                }

                const missingBytes = expected - (chunkLen - index);

                if(missingBytes > 0){

                    // Ignore trailing if stream ended
                    if(this.done){
                        this.buffer = new Uint8Array(0);
                        return;
                    }

                    // Read expected bytes
                    const mba = new Uint8Array(missingBytes);
                    if(await this.file.read(mba) != mba.length)
                        return;

                    // Slice trailing bytes
                    const tba = this.buffer.slice(index, chunkLen);

                    // Reset buffer with next char
                    this.buffer.set(tba);
                    this.buffer.set(mba, tba.length);
                    chunkLen = expected;
                    index = 0;
                }

                codePoint = (byte & (2 ** (6 - expected) - 1)) << (6 * expected);
                continue;
            }

            expected--;
            codePoint |= (byte & F) << (expected * 6);

            if (expected === 0) {
                this.queue.unshift(String.fromCodePoint(codePoint));
                codePoint = 0;
            }
        }

        if(this.done){
            // Add last line even if there's no break
            const slice = this.queue.slice(0, this.queue.length - lastBreak);
            this.lines.push(slice.reverse().join(''));
        }
    }

    async advance() {
        if(this.queue.length == 0)
            await this.read();

        const c = this.queue.pop() || '';
        this.consumed.push(c);
        return c;
    }

    backtrack(amount = 1) {
        for(let i = 0; i < amount; i++)
            this.queue.push(this.consumed.pop() as string);
    }

    async lookahead(amount = 1) {

        if(this.queue.length < amount)
            await this.read();

        const l = this.queue.length;
        return this.queue.slice(l - amount, l).reverse().join('');
    }

}