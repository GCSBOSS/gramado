
import { nextToken } from './lex.ts';
import { parseProgram } from './syn.ts';
import { SourceReader } from './reader.ts';

export async function parse(reader: SourceReader){

    const data = {
        stackedTokens: [], line: 0, col: 0, tokens: [], returnStack: [],
        reader, statements: [], errors: [], loopStack: [], stringMode: '',
        stringDelimiter: ''
    };

    await parseProgram(data);

    return data;
}

export async function scan(reader: SourceReader){
    const data = {
        stackedTokens: [], line: 0, col: 0, tokens: [], returnStack: [],
        reader, statements: [], errors: [], loopStack: [], stringMode: '',
        stringDelimiter: ''
    };

    let t;
    do {
        t = await nextToken(data);
    } while(t.type != 'EOF');

    return data.tokens;
}

// function print(s){
//     const text = new TextEncoder().encode(s)
//     Deno.writeAll(Deno.stdout, text)
// }

// function consoleHighlightTokens(tokens){
//     for(const t of tokens){
//         let c = '';
//         if(t.type.indexOf('keyword') == 0)
//             c = '\u001b[34;1m';
//         if(t.type == 'word')
//             c = '\u001b[38;5;45m';
//         if(t.type == 'number' || t.type == 'tag')
//             c = '\u001b[38;5;222m';
//         if(t.type == 'string')
//             c = '\u001b[38;5;209m';
//         if(t.type == 'scape' || t.type == 'concat')
//             c = '\u001b[38;5;202m';
//         if(t.type == 'comment')
//             c = '\u001b[38;5;64m';
//         if(t.type == 'id')
//             c = '\u001b[38;5;49m';
//         if(t.type == 'invalid' || t.type == 'unfinished string')
//             c = '\u001b[41;1m';
//         print(c + t.value + '\u001b[0m');
//     }
// }