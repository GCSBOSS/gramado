
import { createError } from './errors.ts';

import { nextToken, returnToken, registerKeywords, ParserState, ONode, OLeave,
    OOperator } from './lex.ts';

const IS_WORDS = [ 'string', 'null', 'empty', 'array', 'object', 'date', 'fx', 'boolean', 'number' ];

registerKeywords('false', 'is', 'null', 'true', 'log');

export interface OList extends ONode {
    items: ONode[]
}

export interface OPair extends ONode {
    key: ONode,
    val: ONode
}

export interface OFx extends OList {
    name?: OLeave,
    block: ONode[]
}

export interface OUnary extends ONode {
    subject: ONode;
}

export interface OAFx extends OList {
    block?: ONode[],
    subject?: ONode;
}

export interface OCall extends OUnary {
    arguments: ONode[]
}

export interface OOp extends ONode {
    left: ONode,
    right: ONode,
    op: string
}

export interface OTernary extends ONode {
    cond: ONode,
    left: ONode,
    right?: ONode
}

export interface OCondBlock extends ONode {
    block: ONode[],
    cond: ONode
}

export interface OIf extends OCondBlock {
    elsifs?: OIf[],
    elseBlock?: ONode[]
}

export interface OAccess extends ONode {
    subject: ONode,
    property: ONode
}

export interface OFor extends ONode {
    count?: OLeave,
    key?: OLeave,
    val?: OLeave,
    subject: ONode,
    block: ONode[]
}

export interface OFrom extends ONode {
    count?: OLeave,
    end: ONode,
    start: ONode,
    op: string,
    block: ONode[]
}

type ParseOptions = {
    allowNull?: boolean,
    operLevel?: number,
    noObjSCall?: boolean
}

type Pos = {
    startLine: number,
    startCol: number,
    endLine: number,
    endCol: number
}

function copyPos(start: ONode, end: ONode): Pos{
    return {
        startLine: start.startLine, startCol: start.startCol,
        endLine: end.endLine, endCol: end.endCol
    };
}

function createNode(data: ParserState, type: string): ONode{
    return { type, startCol: data.col, endCol: data.col, startLine: data.line,
        endLine: data.line };
}

function safeError(data: ParserState, type: string, node: ONode){
    data.errors.push(createError(data, type, node));
}

function isEnclosed(node: ONode){
    return node.type as string in { INDEX: 1, PROP: 1, string: 1, STRING: 1,
        number: 1, word: 1, tag: 1, ARRAY: 1, OBJECT: 1, 'kwTrue': 1,
        'kwFalse': 1, CALL: 1, regexp: 1, NOT: 1, MINUS: 1, LIST: 1 };
}

function isCallable(node: ONode){
    return [ 'word', 'INDEX', 'PROP', 'tag', 'CALL' ].includes(node.type);
}

function isExprStarter(token: OLeave){
    return [ 'sym /', 'word', 'sym (', 'number', 'string', 'sym [', 'sym {',
        'tag', 'kwTrue', 'kwFalse', 'kwNull', 'sym !',
        'kwFx', 'kwLog' ].includes(token.type) ||
        (token.type == 'sym -' && !token.spaceAfter);
}

export function isReturnable(node: ONode){
    return isEnclosed(node) || node.type == 'FX' || node.type == 'OPER' ||
        node.type == 'SCALL' || node.type == 'LOG';
}

async function parseEnclosed(data: ParserState): Promise<OList>{

    // Open parenthesis
    const first = await nextToken(data);

    const items = [];

    let nt;
    while(true){

        nt = await nextToken(data);
        if(nt.type == 'sym )')
            break;
        returnToken(data, nt);

        // TODO spread params
        const i = await parseExpression(data);
        // TODO default params
        items.push(i);

        nt = await nextToken(data);
        if(nt.type == 'sym ,')
            continue;
        else if(nt.type == 'sym )')
            break;
        else
            throw createError(data, 'missingEncloser', nt);
    }

    return { ...copyPos(first, nt), type: 'LIST', items };
}

async function parseArray(data: ParserState): Promise<OList>{
    const open = await nextToken(data);
    const items: ONode[] = [];

    let nt = await nextToken(data);
    if(nt.type != 'sym ]'){
        returnToken(data, nt);

        do{
            const el = await parseExpression(data);
            items.push(el);

            nt = await nextToken(data);
            // TODO allow breaking array item with new line if it would error

        } while(nt.type == 'sym ,')

        if(nt.type != 'sym ]')
            throw createError(data, 'missingArrayClosing', nt);
    }

    return { type: 'ARRAY', items, ...copyPos(open, nt) };
}

async function parseObject(data: ParserState): Promise<OList>{
    const pairs: ONode[] = [];
    const open = await nextToken(data);

    let nt;

    do{
        const t1 = await nextToken(data);
        let key: ONode;
        if(t1.type == 'sym }'){
            nt = t1;
            break;
        }
        else if(t1.type == 'sym ['){
            key = await parseExpression(data);

            const sq = await nextToken(data);
            if(sq.type != 'sym ]'){
                returnToken(data, sq);
                safeError(data, 'missingKeyClosingBracket', sq);
            }
        }
        else if([ 'word', 'number', 'string' ].includes(t1.type))
            key = t1;
        else{
            safeError(data, 'unexpectedObjectKey', t1);
            key = createNode(data, '!badKey');
        }

        // TODO allow spread syntax

        let val: ONode;
        const colon = await nextToken(data);
        if(colon.type == 'sym :')
            val = await parseExpression(data);
        else {
            val = key;
            returnToken(data, colon);
        }

        pairs.push({ ...copyPos(key, val), type: 'PAIR', key, val } as ONode);

        nt = await nextToken(data);
        if(nt.type != 'sym ,')
            returnToken(data, nt);

    } while(nt.type == 'sym ,')

    if(nt.type != 'sym }')
        throw createError(data, 'missingObjectClosing', nt);

    const close = await nextToken(data);

    return { type: 'OBJECT', ...copyPos(open, close), items: pairs };
}

async function parseString(data: ParserState): Promise<ONode | OList>{

    const node: OLeave = await nextToken(data);

    var t1 = await nextToken(data);
    if(t1.breakBefore || ![ 'concat', 'scape', 'string' ].includes(t1.type)){
        returnToken(data, t1);
        return node;
    }

    // Strip opening quote
    node.value = node.value!.substr(1);

    const items: ONode[] = [ node ];

    while([ 'concat', 'scape', 'string' ].includes(t1.type)){

        if(t1.type == 'scape')
            items.push(t1);

        else if(t1.type == 'concat'){
            items.push(await parseExpression(data));

            const nt = await nextToken(data);
            if(nt.type != 'concat')
                throw createError(data, 'missingInterpolationClose', nt);
        }

        else if(t1.type == 'string' && t1.startLine === t1.prev!.endLine)
            items.push(t1);

        // STRING IN A NEW LINE interrupts the composition
        else
            break;

        t1 = await nextToken(data);
    }

    if(t1.type == '!badString')
        throw createError(data, 'unfinishedString', t1);

    // Strip closing quote
    const last = items[items.length - 1] as OLeave;
    last.value = last.value.substr(0, last.value.length - 1);

    returnToken(data, t1);

    return { type: 'STRING', ...copyPos(node, last), items };
}

registerKeywords('fx');
async function parseFunction(data: ParserState): Promise<OFx>{
    const kw = await nextToken(data);

    const params: ONode[] = [];

    let n = await nextToken(data);
    let name;
    if(n.type == 'word' && !n.breakBefore){
        name = n;
        n = await nextToken(data);
    }

    if(n.type == 'sym (' && !n.breakBefore){

        n = await nextToken(data);

        if(n.type != 'sym )'){

            returnToken(data, n);
            const i = await parseExpression(data);
            // TODO default arg
            params.push(i);

            n = await nextToken(data);
            while(n.type == 'sym ,'){

                // TODO spread params
                const i = await parseExpression(data);
                // TODO default arg
                params.push(i);

                n = await nextToken(data);
            }

            if(n.type != 'sym )'){
                safeError(data, 'missingEncloser', n);
                returnToken(data, n);
            }
        }
    }
    else
        returnToken(data, n);

    data.returnStack.unshift(true);
    data.loopStack.unshift(false);
    const block = await parseBlock(data);
    data.returnStack.shift();
    data.loopStack.shift();

    return { type: 'FX', ...copyPos(kw, data.lastFinalToken!), items: params,
        name, block };
}

async function parseLog(data: ParserState): Promise<OList>{
    const kw = await nextToken(data);

    let cn;
    const nt = await nextToken(data);

    if(!nt.spaceBefore && nt.type == 'sym (')
        cn = await parseCall(data, kw);

    // space call
    else if(nt.spaceBefore && !nt.breakBefore && isExprStarter(nt)){
        returnToken(data, nt);
        cn = await parseSpaceCall(data, kw);
    }
    else {
        safeError(data, 'missingLogArguments', nt);
        return { ...kw, type: 'LOG', items: [] };
    }

    return { type: 'LOG', items: cn.arguments, ...copyPos(kw, cn) };
}

async function parseUnary(data: ParserState, type: string): Promise<OUnary>{
    const op = await nextToken(data);
    const subject = await parseExpression(data, { operLevel: 6.2 });
    return { type, subject, ...copyPos(op, subject) };
}

async function parsePrimary(data: ParserState, opts: ParseOptions = {}): Promise<ONode>{
    const token: OLeave = await nextToken(data);

    if(token.type == 'kwNull' && opts.allowNull)
        return token;

    if(token.type == '!badString')
        throw createError(data, 'unfinishedString', token);

    returnToken(data, token);
    let node;
    if(token.type == 'sym !')
        node = await parseUnary(data, 'NOT');
    else if(token.type == 'sym -')
        node = await parseUnary(data, 'MINUS');
    else if(token.type == 'kwLog')
        node = await parseLog(data);
    else if(token.type == 'kwFx')
        node = await parseFunction(data);
    else if(token.type == 'sym (')
        node = await parseEnclosed(data);
    else if(token.type == 'sym [')
        node = await parseArray(data);
    else if(token.type == 'sym {')
        node = await parseObject(data);
    else if(token.type == 'string')
        node = await parseString(data);
    else
        node = await nextToken(data);

    if(!isReturnable(node))
        throw createError(data, 'invalidOperand', node);

    return node;
}

async function parseCall(data: ParserState, subject: ONode): Promise<OCall>{
    // here first parenthesis should be consumed already
    const args: ONode[] = [];

    let nt = await nextToken(data);

    if(nt.type != 'sym )'){
        returnToken(data, nt);
        args.push(await parseExpression(data, { allowNull: true }));
        nt = await nextToken(data);
    }

    while(nt.type != 'sym )'){

        if(nt.type != 'sym ,')
            safeError(data, 'missingCallSeparator', nt);

        args.push(await parseExpression(data, { allowNull: true }));
        nt = await nextToken(data);
    }

    return { ...copyPos(subject, nt), type: 'CALL', subject, arguments: args };
}

async function parseSpaceCall(data: ParserState, subject: ONode): Promise<OCall>{
    const args: ONode[] = [];

    args.push(await parseExpression(data, { allowNull: true }));

    let nt = await nextToken(data);
    while(nt.type == 'sym ,' || (nt.breakBefore && isExprStarter(nt))){

        if(nt.type != 'sym ,')
            returnToken(data, nt);

        args.push(await parseExpression(data, { allowNull: true }));
        nt = await nextToken(data);
    }
    returnToken(data, nt);

    const last = args[args.length - 1];
    return { ...copyPos(subject, last), type: 'SCALL', subject, arguments: args };
}

function isAssigner(token: ONode){
    return [ 'word', 'PROP', 'INDEX' ].includes(token.type);
}

async function parseTernary(data: ParserState, cond: ONode): Promise<OTernary>{
    const left = await parseExpression(data, { operLevel: 0.4 });

    const nt = await nextToken(data);
    let right;
    if(nt.type == 'sym :')
        right = await parseExpression(data, { operLevel: 0.4 });
    else
        returnToken(data, nt);

    return { type: 'COND', cond, ...copyPos(cond, right || left), right, left };
}

async function parseArrow(data: ParserState, params: ONode): Promise<OAFx>{
    const items = (params.type == 'LIST' ? (params as OList).items : [ params ]);

    items.forEach(i => i.type != 'word' &&
        safeError(data, 'unexpectedParam', i));

    const nt = await nextToken(data);
    returnToken(data, nt);
    let block, subject;
    if(nt.type != 'sym {')
        subject = await parseExpression(data);
    else{
        data.loopStack.unshift(false);
        data.returnStack.unshift(true);
        block = await parseBlock(data);
        data.returnStack.shift();
        data.loopStack.shift();
    }

    return { type: 'AFX', items, block, subject,
        ...copyPos(params, data.lastFinalToken!) };
}

async function parseExpression(data: ParserState, opts: ParseOptions = {}): Promise<ONode>{

    let node = await parsePrimary(data, { allowNull: opts.allowNull });

    let next = await nextToken(data);
    let oper = next as OOperator;
    while(true){

        if(next.type == 'sym =>')
            node = await parseArrow(data, node);

        else if(next.type == 'kwIs' && (!opts.operLevel || 4 >= opts.operLevel)){
            const rn = await nextToken(data);
            if(!IS_WORDS.includes(rn.value as string))
                throw createError(data, 'unknownIsParameter', rn);
            node = { type: 'IS', left: node, right: rn, ...copyPos(node, rn) } as ONode;
        }

        // Space before means a function execution with array parameter
        else if(!next.spaceBefore && isEnclosed(node) && next.type == 'sym [' && (!opts.operLevel || 7 >= opts.operLevel)){
            const property = await parseExpression(data, { operLevel: 8 });

            const cb = await nextToken(data);
            if(cb.type != 'sym ]')
                throw createError(data, 'missingIndexClosing', cb);

            node = { type: 'INDEX', subject: node, property, ...copyPos(node, cb) } as ONode;
        }

        else if(isAssigner(node) && next.type == 'sym =' && (!opts.operLevel || 0 >= opts.operLevel)){
            const rn = await parseExpression(data, { operLevel: 0, allowNull: true });
            node = { type: 'ASSIGN', left: node, right: rn, ...copyPos(node, rn) } as ONode;
            if(rn.type == 'kwNull')
                return node;
        }

        else if(isAssigner(node) && (next.type == 'sym -=' || next.type == 'sym +=') && (!opts.operLevel || 0 >= opts.operLevel)){
            const right = await parseExpression(data, { operLevel: 0 });
            node = { type: next.value == '+=' ? 'ADD' : 'SUB', left: node, right,
                ...copyPos(node, right) } as ONode;
        }

        else if(isEnclosed(node) && !next.spaceBefore && next.type == 'sym .' && (!opts.operLevel || 7 >= opts.operLevel)){

            const rn = await parseExpression(data, { operLevel: 8 });

            if(next.spaceAfter)
                throw createError(data, 'unexpectedSpaceAfterDot', rn);

            if(rn.type != 'word')
                throw createError(data, 'unexpectedTokenAfterDot', rn);

            node = { type: 'PROP', subject: node, property: rn, ...copyPos(node, rn) } as ONode;
        }

        else if(!next.spaceBefore && isCallable(node) && next.type == 'sym (' && (!opts.operLevel || 6.5 >= opts.operLevel))
            node = await parseCall(data, node);

        else if(next.type == 'sym ?' && (!opts.operLevel || 0.5 >= opts.operLevel))
            node = await parseTernary(data, node);

        else if(oper.operator && (next.type != 'sym -' || next.spaceAfter) && (!opts.operLevel || oper.level >= opts.operLevel)){
            const right = await parseExpression(data, { noObjSCall: opts.noObjSCall,
                operLevel: 1 + oper.level });
            node = { type: 'OPER', level: oper.level, op: next.value as string,
                left: node, right, ...copyPos(node, right) } as ONode;
        }

        // space call
        else if(
            (!opts.noObjSCall || next.value != '{') &&
            next.spaceBefore &&
            !next.breakBefore &&
            isCallable(node) &&
            isExprStarter(next) &&
            (!opts.operLevel || 6.5 >= opts.operLevel)

        ){
            returnToken(data, next);
            node = await parseSpaceCall(data, node);
        }

        else
            break;

        next = await nextToken(data);
        oper = next as OOperator;
    }

    returnToken(data, oper);
    return node;
}

registerKeywords('var');
async function parseVar(data: ParserState): Promise<OList>{
    const items: ONode[] = [];

    const kw = await nextToken(data);
    var nt;

    do{
        const left = await nextToken(data);
        if(left.type != 'word')
            throw createError(data, 'missingVarWord', left);

        nt = await nextToken(data);
        if(nt.type == 'sym ,'){
            items.push(left);
            continue;
        }

        if(nt.type != 'sym ='){
            items.push(left);
            break;
        }

        const right = await parseExpression(data);
        items.push({ type: 'ASSIGN', left, right, ...copyPos(left, right) } as ONode);

        nt = await nextToken(data);
    } while(nt.type == 'sym ,')

    returnToken(data, nt);

    return { type: 'VAR', items, ...copyPos(kw, items[items.length - 1]) };
}

async function parseBlock(data: ParserState): Promise<ONode[]>{
    let nt = await nextToken(data);

    if(nt.type != 'sym {' && nt.breakBefore){
        returnToken(data, nt);
        return [ await parseStatement(data) ];
    }

    if(nt.type != 'sym {')
        throw createError(data, 'missingBlockOpen', nt);

    const array = [];
    nt = await nextToken(data);

    while(nt.type != 'sym }'){
        try{
            returnToken(data, nt);
            const s = await parseStatement(data);
            if(s.type == 'EOF')
                return array;

            array.push(s);
        }
        catch(err){
            if(err instanceof Error)
                throw err;
            data.errors.push(err);
        }

        nt = await nextToken(data);
    }

    return array;
}

registerKeywords('if', 'else', 'elsif');
async function parseIf(data: ParserState): Promise<OCondBlock>{
    const kw = await nextToken(data);
    const cond = await parseExpression(data, { noObjSCall: true });

    if(shouldModify(data, kw)){
        const block = [ data.statements.pop()! ];
        return { type: 'IF', block, cond, ...copyPos(block[0], cond) };
    }

    const block = await parseBlock(data);

    let elseBlock, elsifs, nt = await nextToken(data);
    if(nt.type == 'kwElsif'){
        elsifs = [];
        while(nt.type == 'kwElsif'){
            const cond = await parseExpression(data, { noObjSCall: true });
            elsifs.push({ type: 'ELSIF', block: await parseBlock(data), cond });
            nt = await nextToken(data);
        }
    }

    if(nt.type == 'kwElse'){
        elseBlock = await parseBlock(data);
        nt = await nextToken(data);
    }

    returnToken(data, nt);
    return { type: 'IF', block, cond, elseBlock, elsifs,
        ...copyPos(kw, data.lastFinalToken!) } as OCondBlock;
}

const MODIFIABLES = [ 'CALL', 'SCALL', 'ASSIGN', 'RETURN', 'kwExit', 'ADD',
    'SUB', 'kwNext', 'kwBreak' ];

function shouldModify(data: ParserState, kw: OLeave){
    const ls = data.statements[data.statements.length - 1];
    const r = ls && !kw.breakBefore && MODIFIABLES.includes(ls.type as string);
    if(!r && !kw.breakBefore)
        throw createError(data, 'unexpected', kw);

    return r;
}

registerKeywords('unless');
async function parseUnless(data: ParserState): Promise<OCondBlock>{
    const kw = await nextToken(data);
    const cond = await parseExpression(data, { noObjSCall: true });

    const sm = shouldModify(data, kw);
    const block = sm
        ? [ data.statements.pop()! ]
        : await parseBlock(data);

    return { type: 'UNLESS', block, cond,
        ...copyPos(sm ? block[0] : kw, sm ? cond : data.lastFinalToken!) };
}

registerKeywords('while');
async function parseWhile(data: ParserState): Promise<OCondBlock>{
    const kw = await nextToken(data);
    const cond = await parseExpression(data, { noObjSCall: true });

    data.loopStack.unshift(true);
    const sm = shouldModify(data, kw);
    const block = sm
        ? [ data.statements.pop()! ]
        : await parseBlock(data);
    data.loopStack.shift();

    return { type: 'WHILE', block, cond,
        ...copyPos(sm ? block[0] : kw, sm ? cond : data.lastFinalToken!) };
}

registerKeywords('until');
async function parseUntil(data: ParserState): Promise<OCondBlock>{
    const kw = await nextToken(data);
    const cond = await parseExpression(data, { noObjSCall: true });

    data.loopStack.unshift(true);
    const sm = shouldModify(data, kw);
    const block = sm
        ? [ data.statements.pop()! ]
        : await parseBlock(data);
    data.loopStack.shift();

    return { type: 'UNTIL', block, cond,
        ...copyPos(sm ? block[0] : kw, sm ? cond : data.lastFinalToken!) };
}

registerKeywords('for', 'of', 'count');
async function parseFor(data: ParserState): Promise<ONode>{

    const kw = await nextToken(data);

    const f = await nextToken(data);
    if(f.type != 'word')
        throw createError(data, 'missingForSpecifier', f);


    let key, val, count;

    let n = await nextToken(data);

    if(n.type == 'sym :'){
        key = f;
        n = await nextToken(data);

        if(n.type == 'word'){
            val = n;
            n = await nextToken(data);
        }
    }
    else
        val = f;

    if(n.type != 'kwOf'){
        safeError(data, 'missingOfKeyword', n);
        returnToken(data, n);
    }

    const subject = await parseExpression(data, { noObjSCall: true });

    n = await nextToken(data);
    if(n.type == 'kwCount'){
        count = await nextToken(data);
        if(count.type != 'word' || count.breakBefore)
            throw createError(data, 'missingForCounter', count);
    }
    else
        returnToken(data, n);

    data.loopStack.unshift(true);
    const sm = shouldModify(data, kw);
    const block = sm ? [ data.statements.pop()! ] : await parseBlock(data);
    data.loopStack.shift();

    return { type: 'FOR', key, val, subject, count, block,
        ...copyPos(sm ? block[0] : kw, data.lastFinalToken!) } as ONode;
}

registerKeywords('from', 'to', 'downto');
async function parseFrom(data: ParserState): Promise<ONode>{

    const kw = await nextToken(data);

    const start = await parseExpression(data);

    const n = await nextToken(data);
    let op;

    if(n.type == 'kwTo' || n.type == 'kwDownto')
        op = n.value as string;
    else {
        safeError(data, 'missingTo', n);
        returnToken(data, n);
    }

    const end = await parseExpression(data, { noObjSCall: true });

    let count;
    const c = await nextToken(data);
    if(c.type == 'kwCount'){
        count = await nextToken(data);
        if(count.type != 'word' || count.breakBefore)
            throw createError(data, 'missingForCounter', count);
    }
    else
        returnToken(data, c);

    data.loopStack.unshift(true);
    const sm = shouldModify(data, kw);
    const block = sm ? [ data.statements.pop()! ] : await parseBlock(data);
    data.loopStack.shift();

    return { type: 'FROM', start, end, op, count, block,
        ...copyPos(sm ? block[0] : kw, data.lastFinalToken!) } as ONode;
}

registerKeywords('next');
async function parseNext(data: ParserState): Promise<OLeave>{
    const kw = await nextToken(data);
    if(!data.loopStack[0])
        safeError(data, 'nextNotAllowed', kw);
    return kw;
}

registerKeywords('break');
async function parseBreak(data: ParserState): Promise<OLeave>{
    const kw = await nextToken(data);
    if(!data.loopStack[0])
        safeError(data, 'breakNotAllowed', kw);
    return kw;
}

registerKeywords('return');
async function parseReturn(data: ParserState): Promise<OUnary>{
    const kw = await nextToken(data);
    if(!data.returnStack[0])
        safeError(data, 'returnNotAllowed', kw);
    return { type: 'RETURN', subject: await parseExpression(data),
        ...copyPos(kw, data.lastFinalToken!) };
}

registerKeywords('exit');
async function parseStatement(data: ParserState): Promise<ONode> {
    const t1 = await nextToken(data);
    returnToken(data, t1);

    if(t1.type == 'kwVar')
        return await parseVar(data);

    if(t1.type == 'kwFrom')
        return await parseFrom(data);

    if(t1.type == 'kwFor')
        return await parseFor(data);

    if(t1.type == 'kwIf')
        return await parseIf(data);

    if(t1.type == 'kwUnless')
        return await parseUnless(data);

    if(t1.type == 'kwWhile')
        return await parseWhile(data);

    if(t1.type == 'kwUntil')
        return await parseUntil(data);

    if(t1.type == 'kwReturn')
        return await parseReturn(data);

    if(t1.type == 'kwNext')
        return await parseNext(data);

    if(t1.type == 'kwBreak')
        return await parseBreak(data);

    if(t1.type == 'kwExit')
        return await nextToken(data);

    if(isExprStarter(t1))
        return await parseExpression(data);

    if(t1.type == 'EOF')
        return await nextToken(data);

    if(t1.type == '!badString')
        throw createError(data, 'unfinishedString', await nextToken(data));

    throw createError(data, 'unexpected', await nextToken(data));
}

export async function parseProgram(data: ParserState){
    let s;

    while(!s || s.type != 'EOF'){
        try {
            s = await parseStatement(data);
            data.statements.push(s);
        }
        catch(err){
            if(err instanceof Error)
                throw err;
            data.errors.push(err);
        }
    }
}
