import { readerFromStreamReader } from 'https://deno.land/std@0.95.0/io/streams.ts';
import { dirname, basename } from 'https://deno.land/std@0.95.0/path/mod.ts';
import { eclipt } from 'https://deno.land/x/eclipt@0.2.1/eclipt.ts';
import { tgz } from 'https://deno.land/x/compress@v0.3.8/mod.ts';
import { rgb8, dim } from 'https://deno.land/std@0.95.0/fmt/colors.ts';
import { transpile } from './scripter.ts';
import { FileSourceReader } from './reader.ts';
import { parse } from './parser.ts';
import { formatParseErrors } from './errors.ts';

const GITLAB_RELEASES = 'https://gitlab.com/api/v4/projects/25788257/releases';
const CURRENT_VERSION = 'a1';

eclipt('one', {
    description: 'One language to code them all',
    commands: {

        version: {
            action: () => {
                console.log('💍 One', rgb8(CURRENT_VERSION, 153));
                console.log('🦕 Deno 1.9.2');
            }
        },

        dump: {
            description: 'Transpile input file and execute as Deno script',
            action: async input => {
                try{
                    const sr = new FileSourceReader(input.args[0]);
                    const data = await parse(sr);

                    if(data.errors.length > 0){
                        console.error(formatParseErrors(data.errors));
                        Deno.exit(1);
                    }

                    console.log(await transpile(data.statements));
                }
                catch(err){
                    Deno.writeTextFile('./diag-error.log', JSON.stringify(err) +
                        '\r\n', { append: true });
                }
            },
            args: [ 'input.one' ]
        },

        update: {
            opts: {
                'dry-run': { alias: 'd', flag: true,
                    description: 'Only check latest release without installing it' }
            },
            description: 'Update the One binary to the latest version',
            action: async ({ opts }) => {

                console.log('\n🔎', dim('Looking up latest version'));
                const res = await fetch(GITLAB_RELEASES);
                const [ latest ] = await res.json();

                if(latest.tag_name == CURRENT_VERSION)
                    return console.log('✔️  Local version', rgb8(CURRENT_VERSION, 153), 'is up to date\n');

                console.log('✨ Found latest version', rgb8(latest.tag_name, 40));
                if(opts['dry-run'])
                    return console.log('\n' + latest.description + '\n');

                const curBin = Deno.execPath();
                if(basename(curBin).indexOf('deno') == 0)
                    return console.log('🦕', rgb8('Cannot replace Deno binary\n', 208));

                console.log('🌏', dim('Downloading package'));
                const link = latest.assets.links.find((l: Record<string, string>) =>
                    l.name.indexOf(Deno.build.os) > 0);
                const resBin = await fetch(link.url);
                const reader = readerFromStreamReader(resBin.body!.getReader());
                const pkg = dirname(curBin) + '/' + basename(link.url);

                try{
                    const file = await Deno.open(pkg, { create: true, write: true });
                    await Deno.copy(reader, file);

                    console.log('📦', dim('Unpacking binary'));
                    const newBin = dirname(curBin) + '/one.' + latest.tag_name;
                    await tgz.uncompress(pkg, newBin);
                    await Deno.remove(pkg);

                    console.log('⚙️', dim('Installing new binary'));
                    await Deno.rename(curBin, curBin + '.' + CURRENT_VERSION);
                    await Deno.rename(newBin, curBin);

                    console.log('💍 Update complete. Version is now', rgb8(latest.tag_name, 40), '\n');
                }
                catch(err){
                    if(err.name == 'PermissionDenied')
                        console.log('👮', rgb8('Please, try again with admin rights', 208), '\n');
                    else
                        console.log('\u001b[38;5;160m⚠️ ', err, '\u001b[0m\n');
                }
            }
        }
    }
});
