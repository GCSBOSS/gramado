import { assertEquals, assert } from "https://deno.land/std@0.92.0/testing/asserts.ts";

import { scan as s, parse as p } from "../parser.ts";
import { StringSourceReader } from "../reader.ts";

async function parse(str){
    const r = await p(new StringSourceReader(str));
    if(r.errors.length > 0)
        throw r.errors;
    return r.statements;
}

function scan(str){
    return s(new StringSourceReader(str));
}

const Reno = {
    test(name, fn){
        Deno.test({ name, fn, only: true })
    }
};
Reno;

Deno.test('[lexer] base tokenization', async () => {
    const r = await scan('this-Is9_Word + null "test"  \n \'string\\ve\' 23 4.66 .23 is++ "sdsd');
    assertEquals(r[0].type, 'word');
    assertEquals(r[0].startCol, 0);
    assertEquals(r[2].type, 'sym +');
    assertEquals(r[2].startCol, 14);
    assertEquals(r[4].type, 'kwNull');
    assertEquals(r[6].type, 'string');
    assertEquals(r[6].endCol, 26);
    assert(r[6].breakAfter);
    assertEquals(r[8].type, 'lineBreak');
    assertEquals(r[10].type, 'string');
    assert(r[10].breakBefore);
    assertEquals(r[11].type, 'scape');
    assertEquals(r[12].type, 'string');
    assertEquals(r[14].type, 'number');
    assertEquals(r[16].type, 'number');
    assertEquals(r[18].type, 'number');
    assertEquals(r[18].startLine, 1);
    assertEquals(r[24].type, '!badString');
});

Deno.test('[lexer] comments', async () => {
    const r = await scan('// this is a line comment \r\n /* This is \rmulti-line */ $/* bad comment');
    assertEquals(r[0].type, 'comment');
    assertEquals(r[1].type, 'lineBreak');
    assertEquals(r[3].type, 'comment');
    assertEquals(r[3].startLine, 1);
    assertEquals(r[3].endLine, 2);
    assertEquals(r[3].endCol, 12);
});

Deno.test('[lexer] multi-line strings', async () => {
    const r = await scan('"abcde\r\nabcfedg\r\nfghij"');
    assertEquals(r[0].type, 'string');
    assertEquals(r[0].endCol, 5);
    assertEquals(r[0].endLine, 2);
    assertEquals(r[0].startLine, 0);
    assertEquals(r[0].startCol, 0);
});

Deno.test('[lexer] composite strings', async () => {
    const r = await scan('"abc $b def ${a + b} ghi \\t jkl"');
    assertEquals(r[0].type, 'string');
    assertEquals(r[0].value, '"abc ');
    assertEquals(r[1].type, 'concat');
    assertEquals(r[2].type, 'word');
    assertEquals(r[2].value, 'b');
    assertEquals(r[3].type, 'concat');
    assertEquals(r[4].value, ' def ');
    assertEquals(r[13].value, '\\t');
    assertEquals(r[14].value, ' jkl"');
});

Deno.test('unterminated string', async () => {
    var done;
    try{
        await parse('\r\n"sd\nfs\nd');
    }
    catch(err){
        assertEquals(err[0].type, 'unfinishedString');
        done = true;
    }
    assert(done);

    try{
        await parse('a = 2 + "foobar');
    }
    catch(err){
        assertEquals(err[0].type, 'unfinishedString');
        done = false;
    }
    assert(!done);
});

Deno.test('unterminated string in composition', async () => {
    var done;
    try{
        await parse('"sd$r fsd');
    }
    catch(err){
        assertEquals(err[0].type, 'unfinishedString');
        done = true;
    }
    assert(done);
});

Deno.test('string scapes', async () => {
    const [ r ] = await parse('"abc\\ndef"');
    assertEquals(r.items.length, 3);
    assertEquals(r.items[1].type, 'scape');
});

Deno.test('string interpolation', async () => {
    const [ { right: r } ] = await parse('a = "abc\$\{ 3 + \n 4 }tsk \r foobar"');
    assertEquals(r.items.length, 3);
    assertEquals(r.items[1].type, 'OPER');
    assertEquals(r.items[2].startLine, 1);
    assertEquals(r.items[2].endLine, 2);

    const [ r2 ] = await parse('"abc $b foobar $- "');
    assertEquals(r2.items[0].type, 'string');
    assertEquals(r2.items[0].value, 'abc ');
    assertEquals(r2.items[1].type, 'word');
    assertEquals(r2.items[1].value, 'b');
    assertEquals(r2.items[2].type, 'string');
    assertEquals(r2.items[2].value, ' foobar $- ');
    assertEquals(r2.items.length, 3);
});

Deno.test('unterminated interpolation', async () => {
    var done;
    try{
        await parse('a = "abc ${ a + b abc');
    }
    catch(err){
        assertEquals(err[0].type, 'missingInterpolationClose');
        done = true;
    }
    assert(done);
});

Deno.test('one string at each line', async () => {
    const r = await parse(' "foo" \n "bar" ');
    assertEquals(r[0].type, 'string');
    assertEquals(r[1].type, 'string');
});

Deno.test('bad statement', async () => {
    var done;
    try{
        await parse('*');
    }
    catch(err){
        assertEquals(err[0].type, 'unexpected');
        done = true;
    }
    assert(done);
});

Deno.test('booleans', async () => {
    let [ r ] = await parse('true');
    assertEquals(r.type, 'kwTrue');
    [ r ] = await parse('false');
    assertEquals(r.type, 'kwFalse');
});

Deno.test('binary operation', async () => {
    const [ r ] = await parse('b - c');
    assertEquals(r.type, 'OPER');
    assertEquals(r.level, 5);
    assertEquals(r.right.value, 'c');
    assertEquals(r.left.value, 'b');
});

Deno.test('unterminated binary operation', async () => {
    var done;
    try{
        await parse('b +');
    }
    catch(err){
        assertEquals(err[0].type, 'invalidOperand');
        done = true;
    }
    assert(done);
});

Deno.test('stacked binary operations', async () => {
    const [ r ] = await parse('a + b + c');
    assertEquals(r.type, 'OPER');
    assertEquals(r.level, 5);
    assertEquals(r.right.value, 'c');
    assertEquals(r.left.type, 'OPER');
});

Deno.test('precedenting binary operations', async () => {
    let [ r ] = await parse('a * \'abc\' + c');
    assertEquals(r.type, 'OPER');
    assertEquals(r.level, 5);
    assertEquals(r.left.level, 6);

    [ r ] = await parse('a + \'abc\' * c');
    assertEquals(r.level, 5);
    assertEquals(r.right.level, 6);
});

Deno.test('specific order binary operations', async () => {
    let [ r ] = await parse('a * (3 + c)');
    assertEquals(r.type, 'OPER');
    assertEquals(r.level, 6);
    assertEquals(r.right.items[0].level, 5);
    assertEquals(r.right.type, 'LIST');

    [ r ] = await parse('(3 + c) * a');
    assertEquals(r.type, 'OPER');
    assertEquals(r.level, 6);
    assertEquals(r.left.items[0].level, 5);
    assertEquals(r.left.type, 'LIST');
});

Deno.test('missing closing parenthesis', async () => {
    var done;
    try{
        await parse('(a + b');
    }
    catch(err){
        assertEquals(err[0].type, 'missingEncloser');
        done = true;
    }
    assert(done);
});

Deno.test('not operator', async () => {
    let [ r ] = await parse('!b');
    assertEquals(r.type, 'NOT');
    assertEquals(r.subject.value, 'b');

    [ r ] = await parse('!b[0]');
    assertEquals(r.type, 'NOT');
    assertEquals(r.subject.type, 'INDEX');

    [ r ] = await parse('!\'a\'.b');
    assertEquals(r.type, 'NOT');
    assertEquals(r.subject.type, 'PROP');

    [ r ] = await parse('!(a + b)');
    assertEquals(r.type, 'NOT');
    assertEquals(r.subject.type, 'LIST');
});

Deno.test('unary minus operator', async () => {
    let [ r ] = await parse('-b');
    assertEquals(r.type, 'MINUS');
    assertEquals(r.subject.value, 'b');

    [ r ] = await parse('-b[0]');
    assertEquals(r.type, 'MINUS');
    assertEquals(r.subject.type, 'INDEX');

    [ r ] = await parse('-\'a\'.b');
    assertEquals(r.type, 'MINUS');
    assertEquals(r.subject.type, 'PROP');

    [ r ] = await parse('-(a + b)');
    assertEquals(r.type, 'MINUS');
    assertEquals(r.subject.type, 'LIST');
});

Deno.test('assignment', async () => {
    const [ r ] = await parse('a = 2');
    assertEquals(r.type, 'ASSIGN');
    assertEquals(r.left.value, 'a');
    assertEquals(r.right.value, '2');
});

Deno.test('assignment of assignment', async () => {
    const [ r ] = await parse('a = b = 3');
    assertEquals(r.type, 'ASSIGN');
    assertEquals(r.left.value, 'a');
    assertEquals(r.right.left.value, 'b');
    assertEquals(r.right.right.value, '3');
});

Deno.test('assign null value', async () => {
    const [ r ] = await parse('a = null');
    assertEquals(r.right.type, 'kwNull');
});

Deno.test('multiple statements', async () => {
    const [ s1, s2 ] = await parse('a = 2\r\n\n\nb = \'abc\'');
    assertEquals(s1.type, 'ASSIGN');
    assertEquals(s2.type, 'ASSIGN');
});

Deno.test('property access', async () => {
    const [ r ] = await parse('(3 + 5).b.c');
    assertEquals(r.type, 'PROP');
    assertEquals(r.subject.subject.type, 'LIST');
});

Deno.test('space after dot operator', async () => {
    var done;
    try{
        await parse('a. b');
    }
    catch(err){
        assertEquals(err[0].type, 'unexpectedSpaceAfterDot');
        done = true;
    }
    assert(done);
});

Deno.test('bad property token after dot', async () => {
    var done;
    try{
        await parse('a.\'foo\'');
    }
    catch(err){
        assertEquals(err[0].type, 'unexpectedTokenAfterDot');
        done = true;
    }
    assert(done);
});

Deno.test('property access inside expressions', async () => {
    let [ t ] = await parse('abc + 3.b.c');
    assertEquals(t.left.value, 'abc');
    assertEquals(t.right.property.value, 'c');

    [ t ] = await parse('abc = 5 + 3.b.c');
    assertEquals(t.left.value, 'abc');
    assertEquals(t.right.left.value, '5');
    assertEquals(t.right.right.property.value, 'c');

    [ t ] = await parse('abc = 3.b.c + 7');
    assertEquals(t.right.right.value, '7');
    assertEquals(t.right.left.property.value, 'c');
});

Deno.test('assignment to property access', async () => {
    const [ r ] = await parse('a.b.c = a.b');
    assertEquals(r.type, 'ASSIGN');
    assertEquals(r.left.property.value, 'c');
    assertEquals(r.right.property.value, 'b');
});

Deno.test('index access', async () => {
    const [ r ] = await parse('a.b["c"]');
    assertEquals(r.type, 'INDEX');
    assertEquals(r.property.value, '"c"');
    assertEquals(r.subject.type, 'PROP');
});

Deno.test('assignment to index access', async () => {
    const [ r ] = await parse('a[b][\'c\'] = a[0]');
    assertEquals(r.type, 'ASSIGN');
    assertEquals(r.left.property.value, '\'c\'');
    assertEquals(r.right.property.value, '0');
});

Deno.test('missing closing bracket on index access', async () => {
    var done;
    try{
        await parse('abc[0');
    }
    catch(err){
        assertEquals(err[0].type, 'missingIndexClosing');
        done = true;
    }
    assert(done);
});

Deno.test('array literal', async () => {
    let [ r ] = await parse('[ 1, 2, 3 ]');
    assertEquals(r.type, 'ARRAY');
    assertEquals(r.items[0].value, '1');
    assertEquals(r.items[2].value, '3');

    [ r ] = await parse('[ ]');
    assertEquals(r.type, 'ARRAY');
    assertEquals(r.items.length, 0);
});

Deno.test('array of arrays', async () => {
    const [ r ] = await parse('[ [ 1 ], [ 2, 3 ] ]');
    assertEquals(r.type, 'ARRAY');
    assertEquals(r.items[0].items[0].value, '1');
    assertEquals(r.items[1].items[1].value, '3');
});

Deno.test('missing array closing', async () => {
    var done;
    try{
        await parse('[a, b, 1');
    }
    catch(err){
        assertEquals(err[0].type, 'missingArrayClosing');
        done = true;
    }
    assert(done);
});

Deno.test('object literal', async () => {
    const [ r ] = await parse('{ a: 3, "r": abc, [1 + 2]: true }');
    assertEquals(r.type, 'OBJECT');
    assertEquals(r.items[0].val.value, '3');
    assertEquals(r.items[1].key.value, '"r"');
    assertEquals(r.items[2].key.type, 'OPER');
});

Deno.test('missing closing bracket in object key', async () => {
    var done;
    try{
        await parse('{ [a + b: \'a\' }');
    }
    catch(err){
        assertEquals(err[0].type, 'missingKeyClosingBracket');
        done = true;
    }
    assert(done);
});

Deno.test('nested objects', async () => {
    const [ r ] = await parse('{ a: { b }, c: 1, }');
    assertEquals(r.items[0].val.type, 'OBJECT');
    assertEquals(r.items[0].val.items[0].val.type, 'word');
    assertEquals(r.items.length, 2);
});

Deno.test('bad object key', async () => {
    var done;
    try{
        await parse('{ true: "abc" }');
    }
    catch(err){
        assertEquals(err[0].type, 'unexpectedObjectKey');
        done = true;
    }
    assert(done);
});

Deno.test('missing object closing brace', async () => {
    var done;
    try{
        await parse('{ cool: "abc"');
    }
    catch(err){
        assertEquals(err[0].type, 'missingObjectClosing');
        done = true;
    }
    assert(done);
});

Deno.test('\'is\' operator', async () => {
    const [ r ] = await parse('a is empty');
    assertEquals(r.type, 'IS');
    assertEquals(r.right.type, 'word');
});

Deno.test('\'is null\' operation', async () => {
    const [ r ] = await parse('a + b is null');
    assertEquals(r.type, 'IS');
    assertEquals(r.right.type, 'kwNull');
});

Deno.test('\'is\' with wrong parameter', async () => {
    var done;
    try{
        await parse('a is foobar');
    }
    catch(err){
        assertEquals(err[0].type, 'unknownIsParameter');
        done = true;
    }
    assert(done);
});

Deno.test('function call', async () => {
    const [ r ] = await parse('abc()');
    assertEquals(r.type, 'CALL');
});

Deno.test('function call arguments', async () => {
    let [ r ] = await parse('abc(a)');
    assertEquals(r.type, 'CALL');
    assertEquals(r.arguments[0].type, 'word');

    [ r ] = await parse('foo.abc(a, [ 2, 5 ], (true), null)');
    assertEquals(r.subject.type, 'PROP');
    assertEquals(r.arguments.length, 4);
    assertEquals(r.arguments[1].type, 'ARRAY');
});

Deno.test('function call of a call', async () => {
    const [ r ] = await parse('abc(a)(a, b, c)');
    assertEquals(r.type, 'CALL');
    assertEquals(r.arguments.length, 3);
    assertEquals(r.arguments[2].value, 'c');
    assertEquals(r.subject.type, 'CALL');
    assertEquals(r.subject.arguments.length, 1);
});

Deno.test('malformed call', async () => {
    var done;
    try{
        await parse('abc(1, 2\n 4)');
    }
    catch(err){
        assertEquals(err[0].type, 'missingCallSeparator');
        done = true;
    }
    assert(done);
});

Deno.test('space oriented function call', async () => {
    let [ r ] = await parse('abc a');
    assertEquals(r.type, 'SCALL');
    assertEquals(r.arguments[0].type, 'word');

    [ r ] = await parse('foo.abc a, [ 2, 5 ], (true), null');
    assertEquals(r.subject.type, 'PROP');
    assertEquals(r.arguments.length, 4);
    assertEquals(r.arguments[1].type, 'ARRAY');
});

Deno.test('var definition', async () => {
    const [ r ] = await parse('var a, b, c');
    assertEquals(r.type, 'VAR');
    assertEquals(r.items.length, 3);
    assertEquals(r.items[1].value, 'b');
});

Deno.test('var assignment', async () => {
    const [ r ] = await parse('var a = 8');
    assertEquals(r.type, 'VAR');
    assertEquals(r.items.length, 1);
    assertEquals(r.items[0].type, 'ASSIGN');
});

Deno.test('bad expression on \'var\'', async () => {
    var done;
    try{
        await parse('var a, 5');
    }
    catch(err){
        assertEquals(err[0].type, 'missingVarWord');
        done = true;
    }
    assert(done);
});

Deno.test('add assignment', async () => {
    const [ r ] = await parse('a.b += 2');
    assertEquals(r.type, 'ADD');
    assertEquals(r.left.subject.value, 'a');
    assertEquals(r.right.value, '2');
});

Deno.test('add assignment of assignment', async () => {
    const [ r ] = await parse('a += b = 3');
    assertEquals(r.type, 'ADD');
    assertEquals(r.left.value, 'a');
    assertEquals(r.right.left.value, 'b');
    assertEquals(r.right.right.value, '3');
});

Deno.test('subtract assignment', async () => {
    const [ r ] = await parse('a -= 2');
    assertEquals(r.type, 'SUB');
    assertEquals(r.left.value, 'a');
    assertEquals(r.right.value, '2');
});

Deno.test('subtract assignment of assignment', async () => {
    const [ r ] = await parse('a -= b = 3');
    assertEquals(r.type, 'SUB');
    assertEquals(r.left.value, 'a');
    assertEquals(r.right.left.value, 'b');
    assertEquals(r.right.right.value, '3');
});

Deno.test('if statement', async () => {
    let [ r ] = await parse('if a + b { }');
    assertEquals(r.type, 'IF');
    assertEquals(r.cond.type, 'OPER');
    assertEquals(r.block.length, 0);

    [ r ] = await parse('if test "123" {\n a = b \n c = d  }');
    assertEquals(r.type, 'IF');
    assertEquals(r.cond.type, 'SCALL');
    assertEquals(r.block.length, 2);
    assertEquals(r.block[0].type, 'ASSIGN');
    assertEquals(r.block[1].left.value, 'c');
});

Deno.test('if elsif statement', async () => {
    const [ r ] = await parse('if a \n a = 1 \n elsif b \n c = 2 \n elsif b > a \n c = 0');
    assertEquals(r.type, 'IF');
    assertEquals(r.block.length, 1);
    assertEquals(r.elsifs.length, 2);
    assertEquals(r.elsifs[0].type, 'ELSIF');
    assertEquals(r.elsifs[0].block.length, 1);
});

Deno.test('if else statement', async () => {
    const [ r ] = await parse('if a + b \n a = 1 \n else \n c = true');
    assertEquals(r.type, 'IF');
    assertEquals(r.cond.type, 'OPER');
    assertEquals(r.block.length, 1);
    assertEquals(r.elseBlock.length, 1);
});

Deno.test('missing block start', async () => {
    var done;
    try{
        await parse('if a + b }');
    }
    catch(err){
        assertEquals(err[0].type, 'missingBlockOpen');
        done = true;
    }
    assert(done);
});

Deno.test('issues inside blocks', async () => {
    var done;
    try{
        await parse('if a + b { \n a.b(  }');
    }
    catch(err){
        assertEquals(err[0].type, 'invalidOperand');
        done = true;
    }
    assert(done);
});

Deno.test('unless statement', async () => {
    let [ r ] = await parse('unless a + b { }');
    assertEquals(r.type, 'UNLESS');
    assertEquals(r.cond.type, 'OPER');
    assertEquals(r.block.length, 0);

    [ r ] = await parse('unless test "123" {\n a = b \n c = d  }');
    assertEquals(r.type, 'UNLESS');
    assertEquals(r.cond.type, 'SCALL');
    assertEquals(r.block.length, 2);
    assertEquals(r.block[0].type, 'ASSIGN');
    assertEquals(r.block[1].left.value, 'c');
});

Deno.test('while statement', async () => {
    let [ r ] = await parse('while a + b { }');
    assertEquals(r.type, 'WHILE');
    assertEquals(r.cond.type, 'OPER');
    assertEquals(r.block.length, 0);

    [ r ] = await parse('while test "123" {\n a = b \n c = d  }');
    assertEquals(r.type, 'WHILE');
    assertEquals(r.cond.type, 'SCALL');
    assertEquals(r.block.length, 2);
    assertEquals(r.block[0].type, 'ASSIGN');
    assertEquals(r.block[1].left.value, 'c');
});

Deno.test('until statement', async () => {
    let [ r ] = await parse('until a + b { }');
    assertEquals(r.type, 'UNTIL');
    assertEquals(r.cond.type, 'OPER');
    assertEquals(r.block.length, 0);

    [ r ] = await parse('until test "123" {\n a = b \n c = d  }');
    assertEquals(r.type, 'UNTIL');
    assertEquals(r.cond.type, 'SCALL');
    assertEquals(r.block.length, 2);
    assertEquals(r.block[0].type, 'ASSIGN');
    assertEquals(r.block[1].left.value, 'c');
});

Deno.test('regexp', async () => {
    let [ r ] = await parse('a = /^$%&%&*[a-z]$/g');
    assertEquals(r.right.type, 'regexp');

    [ r ] = await parse('!/abc\\/abc/');
    assertEquals(r.subject.type, 'regexp');
});

Deno.test('arrow function', async () => {
    let [ r ] = await parse('a = () => 24 + 3');
    assertEquals(r.right.type, 'AFX');
    assertEquals(r.right.items.length, 0);
    assertEquals(r.right.subject.type, 'OPER');

    [ r ] = await parse('(e, b) => { \n a = b + c \n}');
    assertEquals(r.type, 'AFX');
    assertEquals(r.items.length, 2);
    assertEquals(r.block.length, 1);

    [ r ] = await parse('e => { \n a = b + c \n b = c + d \n}');
    assertEquals(r.type, 'AFX');
    assertEquals(r.items.length, 1);
    assertEquals(r.block.length, 2);
});

Deno.test('bad arrow function param', async () => {
    var done;
    try{
        await parse('(a, 2) => "what"');
    }
    catch(err){
        assertEquals(err[0].type, 'unexpectedParam');
        done = true;
    }
    assert(done);

    try{
        await parse('2 => "what"');
    }
    catch(err){
        assertEquals(err[0].type, 'unexpectedParam');
        done = false;
    }
    assert(!done);
});

Deno.test('function definition', async () => {

    let [ r ] = await parse('fx foobar {\n 24 + 3 }');
    assertEquals(r.type, 'FX');
    assertEquals(r.name.type, 'word');
    assertEquals(r.items.length, 0);
    assertEquals(r.block.length, 1);

    [ r ] = await parse('fx foobar(a, b) \n 24 + 3 \n a = 2');
    assertEquals(r.type, 'FX');
    assertEquals(r.name.type, 'word');
    assertEquals(r.items.length, 2);
    assertEquals(r.block.length, 1);

    [ r ] = await parse('fx(a, b) \n 24 + 3 \n a = 2');
    assertEquals(r.type, 'FX');
    assert(!r.name);
    assertEquals(r.items.length, 2);
    assertEquals(r.block.length, 1);

    [ r ] = await parse('fx \n a + 3');
    assertEquals(r.type, 'FX');
    assert(!r.name);
    assertEquals(r.items.length, 0);
    assertEquals(r.block.length, 1);
});

Deno.test('return statement', async () => {

    let [ r ] = await parse('fx() { return  24 + 3 }');
    assertEquals(r.block[0].type, 'RETURN');
    assertEquals(r.block[0].subject.type, 'OPER');

    [ r ] = await parse('fx(a, b) \n return \n a = 2');
    assertEquals(r.block[0].type, 'RETURN');
    assertEquals(r.block[0].subject.type, 'ASSIGN');
});

Deno.test('return where it\'s not allowed', async () => {
    var done;
    try{
        await parse('return "foobar"');
    }
    catch(err){
        assertEquals(err[0].type, 'returnNotAllowed');
        done = true;
    }
    assert(done);
});

Deno.test('break statement', async () => {
    const [ r ] = await parse('while true \n break');
    assertEquals(r.block[0].type, 'kwBreak');
});

Deno.test('break where it\'s not allowed', async () => {
    var done;
    try{
        await parse('break');
    }
    catch(err){
        assertEquals(err[0].type, 'breakNotAllowed');
        done = true;
    }
    assert(done);
});

Deno.test('next statement', async () => {
    const [ r ] = await parse('while true \n next');
    assertEquals(r.block[0].type, 'kwNext');
});

Deno.test('next where it\'s not allowed', async () => {
    var done;
    try{
        await parse('next');
    }
    catch(err){
        assertEquals(err[0].type, 'nextNotAllowed');
        done = true;
    }
    assert(done);
});

Deno.test('bad function param list', async () => {
    var done;
    try{
        await parse('fx (a, 2 \n "what"');
    }
    catch(err){
        assertEquals(err[0].type, 'missingEncloser');
        done = true;
    }
    assert(done);
});

Deno.test('exit statement', async () => {
    const [ r ] = await parse('exit');
    assertEquals(r.type, 'kwExit');
});

Deno.test('ternary', async () => {
    let [ r ] = await parse('a ? 2');
    assertEquals(r.type, 'COND');
    assertEquals(r.left.type, 'number');

    [ r ] = await parse('a == 3 ? 2 : abc');
    assertEquals(r.type, 'COND');
    assertEquals(r.right.type, 'word');

    [ r ] = await parse('a | 3 ? 2 : abc');
    assertEquals(r.type, 'COND');
    assertEquals(r.cond.type, 'OPER');

    [ r ] = await parse('a | 3 ? foo ? bar : "baz" : abc');
    assertEquals(r.type, 'COND');
    assertEquals(r.left.type, 'COND');
    assertEquals(r.left.left.type, 'word');
    assertEquals(r.left.right.type, 'string');
});

Deno.test('for statement', async () => {

    let [ r ] = await parse('for k: of subj { exit }');
    assertEquals(r.type, 'FOR');
    assertEquals(r.subject.type, 'word');
    assertEquals(r.key.type, 'word');
    assert(!r.val);

    [ r ] = await parse('for v of subj { exit }');
    assertEquals(r.type, 'FOR');
    assertEquals(r.subject.type, 'word');
    assertEquals(r.val.type, 'word');
    assert(!r.key);

    [ r ] = await parse('for k: v of subj \n exit');
    assertEquals(r.type, 'FOR');
    assertEquals(r.subject.type, 'word');
    assertEquals(r.key.type, 'word');
    assertEquals(r.val.type, 'word');
    assert(!r.count);

    [ r ] = await parse('for k: v of subj count i { exit }');
    assertEquals(r.type, 'FOR');
    assertEquals(r.subject.type, 'word');
    assertEquals(r.key.type, 'word');
    assertEquals(r.val.type, 'word');
    assertEquals(r.count.type, 'word');
});

Deno.test('bad for speficier', async () => {
    var done;
    try{
        await parse('for 1 of data {  }');
    }
    catch(err){
        assertEquals(err[0].type, 'missingForSpecifier');
        done = true;
    }
    assert(done);
});

Deno.test('missing for of', async () => {
    var done;
    try{
        await parse('for a data {  }');
    }
    catch(err){
        assertEquals(err[0].type, 'missingOfKeyword');
        done = true;
    }
    assert(done);
});

Deno.test('bad for counter', async () => {
    var done;
    try{
        await parse('for a of data count {  }');
    }
    catch(err){
        assertEquals(err[0].type, 'missingForCounter');
        done = true;
    }
    assert(done);
});

Deno.test('from statement', async () => {

    let [ r ] = await parse('from 0 to 100 { exit }');
    assertEquals(r.type, 'FROM');
    assertEquals(r.start.type, 'number');
    assertEquals(r.op, 'to');
    assertEquals(r.end.type, 'number');

    [ r ] = await parse('from a to 100 { exit }');
    assertEquals(r.type, 'FROM');
    assertEquals(r.start.type, 'word');
    assertEquals(r.op, 'to');
    assertEquals(r.end.type, 'number');

    [ r ] = await parse('from a = 0 downto a \n exit');
    assertEquals(r.type, 'FROM');
    assertEquals(r.start.type, 'ASSIGN');
    assertEquals(r.op, 'downto');
    assertEquals(r.end.type, 'word');

    [ r ] = await parse('from a downto b = 2 { exit }');
    assertEquals(r.type, 'FROM');
    assertEquals(r.start.type, 'word');
    assertEquals(r.op, 'downto');
    assertEquals(r.end.type, 'ASSIGN');

    [ r ] = await parse('from a downto b = 2 count i { exit }');
    assertEquals(r.type, 'FROM');
    assertEquals(r.start.type, 'word');
    assertEquals(r.op, 'downto');
    assertEquals(r.end.type, 'ASSIGN');
    assertEquals(r.count.value, 'i');
});

Deno.test('bad from to', async () => {
    var done;
    try{
        await parse('from a 2 {  }');
    }
    catch(err){
        assertEquals(err[0].type, 'missingTo');
        done = true;
    }
    assert(done);

    try{
        await parse('from a to 2 count {  }');
    }
    catch(err){
        assertEquals(err[0].type, 'missingForCounter');
        done = false;
    }
    assert(!done);
});

Deno.test('control modifiers', async () => {

    let [ r ] = await parse('a() unless c');
    assertEquals(r.type, 'UNLESS');
    assertEquals(r.block[0].type, 'CALL');

    [ r ] = await parse('a = 2 if c > 2');
    assertEquals(r.type, 'IF');
    assertEquals(r.block[0].type, 'ASSIGN');

    [ r ] = await parse('exit while true');
    assertEquals(r.type, 'WHILE');
    assertEquals(r.block[0].type, 'kwExit');

    [ r ] = await parse('a[0] = 2 until abc()');
    assertEquals(r.type, 'UNTIL');
    assertEquals(r.block[0].type, 'ASSIGN');

    [ r ] = await parse('a -= "string" for a: of abc');
    assertEquals(r.type, 'FOR');
    assertEquals(r.block[0].type, 'SUB');

    [ r ] = await parse('a = null from 1 to 100');
    assertEquals(r.type, 'FROM');
    assertEquals(r.block[0].type, 'ASSIGN');
});

Deno.test('modifying bad statement', async () => {
    var done;
    try{
        await parse('a unless b');
    }
    catch(err){
        assertEquals(err[0].type, 'unexpected');
        done = true;
    }
    assert(done);
});

Deno.test('log expression', async () => {
    let [ r ] = await parse('a ? log 1');
    assertEquals(r.left.type, 'LOG');
    assertEquals(r.left.items[0].type, 'number');

    [ r ] = await parse('log(abc)');
    assertEquals(r.type, 'LOG');

    [ r ] = await parse('log(a, [ 2, 5 ], (true), null)');
    assertEquals(r.type, 'LOG');
    assertEquals(r.items.length, 4);

    [ r ] = await parse('log a, [ 2, 5 ]\n (true), null');
    assertEquals(r.type, 'LOG');
    assertEquals(r.items.length, 4);
});

Deno.test('log without arguments', async () => {
    var done;
    try{
        await parse('log');
    }
    catch(err){
        assertEquals(err[0].type, 'missingLogArguments');
        done = true;
    }
    assert(done);
});
