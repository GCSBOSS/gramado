
import { assertEquals, assertThrowsAsync } from "https://deno.land/std@0.92.0/testing/asserts.ts";
import { FileSourceReader } from "../reader.ts";

Deno.test('[file reader] looking ahead', async () => {
    const p = await Deno.makeTempFile();
    await Deno.writeTextFile(p, 'a');
    const r = new FileSourceReader(p);
    assertEquals('a', await r.lookahead());
    await Deno.remove(p);
});

Deno.test('[file reader] backtrack', async () => {
    const p = await Deno.makeTempFile();
    await Deno.writeTextFile(p, 'a');
    const r = new FileSourceReader(p);
    await r.advance();
    r.backtrack();
    assertEquals('a', await r.advance());
    await Deno.remove(p);
});

Deno.test('[file reader] reading a file', async () => {
    const p = await Deno.makeTempFile();
    await Deno.writeTextFile(p, 'a = \n123');
    const r = new FileSourceReader(p);
    let s = await r.advance();
    let c = await r.advance();
    while(c){
        s += c;
        c = await r.advance();
    }
    assertEquals(s, 'a = \n123');
    await Deno.remove(p);
});

Deno.test('[file reader] reading emoji', async () => {
    const p = await Deno.makeTempFile();
    await Deno.writeTextFile(p, 'a = \r\n"👍"');
    const r = new FileSourceReader(p, { chunkSize: 4 });
    let s = await r.advance();
    let c = await r.advance();
    while(c){
        s += c;
        c = await r.advance();
    }
    assertEquals(s, 'a = \r\n"👍"');
    await Deno.remove(p);
});

Deno.test('[file reader] reading invalid utf-8', async () => {
    const p = await Deno.makeTempFile();
    await Deno.writeFile(p, new Uint8Array([ 255, 254, 255, 254, 255, 254 ]));
    const r = new FileSourceReader(p);
    await assertThrowsAsync(() => r.advance());
    await Deno.remove(p);
});

Deno.test('[file reader] reading empty file', async () => {
    const p = await Deno.makeTempFile();
    const r = new FileSourceReader(p);
    await r.advance();
    await Deno.remove(p);
});

Deno.test('[file reader] bad trailing bits', async () => {
    const p = await Deno.makeTempFile();
    const r = new FileSourceReader(p);
    await Deno.writeTextFile(p, 'a = "👍"');
    await Deno.writeFile(p, new Uint8Array([ 243, 254 ]), { append: true });
    let s = await r.advance();
    let c = await r.advance();
    while(c){
        s += c;
        c = await r.advance();
    }
    assertEquals(s, 'a = "👍"');
    await Deno.remove(p);
});