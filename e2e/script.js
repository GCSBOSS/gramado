import { assertEquals, assert } from "https://deno.land/std@0.92.0/testing/asserts.ts";

const Reno = {
    test(name, fn){
        Deno.test({ name, fn, only: true })
    }
};
Reno;

const TEST_BIN = Deno.env.get("ONE_TEST_BIN") || "./bin/one.exe";

async function ev(str){
    const f = await Deno.makeTempFile();
    await Deno.writeTextFile(f, str);
    const p = Deno.run({
        cmd: [ TEST_BIN, "dump", f ],
        stdout: "piped"
    });
    const { code } = await p.status();
    assertEquals(code, 0);
    const src = new TextDecoder().decode(await p.output());

    p.close();

    // console.log(src);

    await Deno.remove(f);
    return eval(src);
}

Deno.test('[scripter] string scapes', async () => {
    await ev('log "abc\\ndef"');
});

Deno.test('[scripter] string interpolation', async () => {

    let r = await ev('var a = "foo \$\{ 3 + 4 } bar"\n a');
    assert(r, 'foo 7 bar');

    r = await ev('var b = 5 \n "abc $b foobar $- "');
    assertEquals(r, 'abc 5 foobar $- ');
});

Deno.test('[scripter] specific order binary operations', async () => {
    const r = await ev('var b = 3 * (2 + 4) \n b');
    assertEquals(r, 18);
});

Deno.test('[scripter] not operator', async () => {
    const r = await ev('!0');
    assertEquals(r, true);
});

Deno.test('[scripter] unary minus operator', async () => {
    const r = await ev('-23.3');
    assertEquals(r, -23.3);
});

Deno.test('[scripter] property access', async () => {
    const r = await ev('(3 + 5).toString()');
    assertEquals(r, '8');
});

Deno.test('[scripter] index access', async () => {
    const r = await ev('"abc".split("")[1]');
    assertEquals(r, 'b');
});

Deno.test('[scripter] array literal', async () => {
    const r = await ev('[ 2, "c" ]');
    assertEquals(r[0], 2);
    assertEquals(r[1], 'c');
});

Deno.test('[scripter] object literal', async () => {
    const r = await ev('{ a: 3, "r": "abc", [1 + 2]: true }');
    assertEquals(r.a, 3);
    assertEquals(r.r, 'abc');
    assertEquals(r[3], true);
});

Deno.test('[scripter] add assignment', async () => {
    const r = await ev('var a = 0\n a += 2\na');
    assertEquals(r, 2);
});

Deno.test('[scripter] subtract assignment', async () => {
    const r = await ev('var a = 0\n a -= 2\na');
    assertEquals(r, -2);
});

Deno.test('[scripter] space oriented function call', async () => {
    const r = await ev('"a=b".split "="');
    assertEquals(r[0], 'a');
    assertEquals(r[1], 'b');
});

Deno.test('[scripter] function definition', async () => {
    const r = await ev('fx foobar(a) {\n return 24 } foobar() ');
    assertEquals(r, 24);
});

Deno.test('[scripter] if statement', async () => {
    const r = await ev('if true \n "cool"');
    assertEquals(r, 'cool');
});

Deno.test('[scripter] if elsif statement', async () => {
    const r = await ev('var a = 3 \n if a == 2 \n 1 \n elsif a == 3 \n ("cool")');
    assertEquals(r, 'cool');
});

Deno.test('[scripter] if else statement', async () => {
    const r = await ev('var a = 3 \n if a == 2 \n 1 \n else \n ("cool")');
    assertEquals(r, 'cool');
});

Deno.test('[scripter] while statement', async () => {
    const r = await ev('var a = 0\n a += 1 while a < 3 \n (a)');
    assertEquals(r, 3);
});

Deno.test('[scripter] until statement', async () => {
    const r = await ev('var a = 0\n a += 1 until a == 2 \n (a)');
    assertEquals(r, 2);
});

Deno.test('[scripter] unless statement', async () => {
    const r = await ev('unless false \n (2)');
    assertEquals(r, 2);
});

Deno.test('[scripter] for statement', async () => {

    let r = await ev('for k: of [ 5, 4, 3 ] \n k');
    assertEquals(r, '2');

    r = await ev('for v of [ 5, 4, 3 ] \n v');
    assertEquals(r, 3);

    r = await ev('for k: v of { a: 5, b: 6 } count i \n i');
    assertEquals(r, 1);
});

Deno.test('[scripter] from statement', async () => {

    let r = await ev('from 0 to 2 \n true');
    assert(r);

    r = await ev('from 0 to 2 count i \n i');
    assertEquals(r, 2);

    r = await ev('from 2 downto 0 count i \n i');
    assertEquals(r, 0);
});

Deno.test('[scripter] arrow function', async () => {

    let r = await ev('var f = () => true \n f()');
    assertEquals(r, true);

    r = await ev('var f = v => { return v } \n f(4)');
    assertEquals(r, 4);
});

Deno.test('[scripter] \'is\' operator', async () => {
    let r = await ev('1.b is null');
    assert(r);

    r = await ev('[] is empty');
    assert(r);

    r = await ev('[ 1 ] is array');
    assert(r);

    r = await ev('(a => 2) is fx');
    assert(r);

    r = await ev('false is boolean');
    assert(r);

    r = await ev('"abc".length is number');
    assert(r);

    r = await ev('"abc" is string');
    assert(r);
});

Deno.test('[scripter] ternary', async () => {
    let r = await ev('true ? 2');
    assertEquals(r, 2);

    r = await ev('true ?     1 & 2 ? 3 : 4');
    assertEquals(r, 3);

    r = await ev('0 | 1 ? 2 : 3');
    assertEquals(r, 2);
});

Deno.test('[scripter] exit statement', async () => {
    let r = await ev('exit 4');
    assert(!r);

    r = await ev('fx foobar(a) {\n exit 4 } foobar()');
    assert(!r);
});
