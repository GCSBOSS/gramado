
use crate::errors::*;
use crate::reader::SourceReader;
use crate::lex::*;

const IS_WORDS: [&str;9] = [ "string", "null", "empty", "array", "object",
    "date", "fx", "boolean", "number" ];

struct ExprOpts { allow_null: bool, oper_level: u8, no_obj_s_call: bool }

pub type ChildNode = Box<Node>;
pub type ChildList = Vec<ChildNode>;

#[derive(Debug)]
pub enum NodeData {

    Leaf (Leaf),
    List { items: ChildList },
    Pair { key: ChildNode, val: Option<ChildNode> },
    Unary { subject: ChildNode },
    Call { items: ChildList, subject: ChildNode },
    Binary { op: ChildNode, left: ChildNode, right: ChildNode },
    Ternary { cond: ChildNode, left: ChildNode, right: Option<ChildNode> },
    CondBlock { block: ChildList, cond: ChildNode },
    Fx { items: ChildList, name: Option<ChildNode>, block: ChildList },
    AFx { items: ChildNode, block: ChildList, expr: bool },

    If {
        block: ChildList,
        cond: ChildNode,
        elsifs: ChildList,
        else_block: Option<ChildList>
    },

    For {
        count: Option<ChildNode>,
        key: Option<ChildNode>,
        val: Option<ChildNode>,
        subject: ChildNode,
        block: ChildList
    },

    From  {
        count: Option<ChildNode>,
        end: ChildNode,
        start: ChildNode,
        op: ChildNode,
        block: ChildList
    }
}

#[derive(Debug)]
pub struct Node {
    pub ty: NodeTy,
    pub data: NodeData,
    pub pos: Pos
}

pub struct ParseResult {
    pub ast: Vec<ChildNode>,
    pub errors: Vec<Err>,

    #[cfg(test)]
    nodes: Vec<NodeInfo>
}

pub struct Parser<'a> {
    pub return_stack: Vec<bool>,
    pub loop_stack: Vec<bool>,
    stacked_tokens: Vec<Node>,
    lexer: Scanner,
    res: &'a mut ParseResult
}

const ENCLOSED: [NodeTy;15] = [ NodeTy::INDEX, NodeTy::PROP, NodeTy::String,
    NodeTy::STRING, NodeTy::Number, NodeTy::Word, NodeTy::ARRAY, NodeTy::OBJECT,
    NodeTy::KwTrue, NodeTy::KwFalse, NodeTy::CALL, NodeTy::RegExp, NodeTy::NOT,
    NodeTy::MINUS, NodeTy::LIST ];

const CALLABLE: [NodeTy;4] = [ NodeTy::Word, NodeTy::INDEX, NodeTy::PROP,
    NodeTy::CALL ];

const EXPR: [NodeTy;14] = [ NodeTy::SymDiv, NodeTy::SymParOp, NodeTy::Word,
    NodeTy::Number, NodeTy::String, NodeTy::SymSqOp, NodeTy::SymCurOp,
    NodeTy::KwTrue, NodeTy::KwFalse, NodeTy::KwNull, NodeTy::SymExMark,
    NodeTy::KwFx, NodeTy::KwLog, NodeTy::SymMinus ];

const RETURNABLE: [NodeTy;19] = [
    NodeTy::INDEX, NodeTy::PROP, NodeTy::String, NodeTy::STRING, NodeTy::Number,
    NodeTy::Word, NodeTy::ARRAY, NodeTy::OBJECT, NodeTy::KwTrue, NodeTy::KwFalse,
    NodeTy::CALL, NodeTy::RegExp, NodeTy::NOT, NodeTy::MINUS, NodeTy::LIST,
    NodeTy::FX, NodeTy::OPER, NodeTy::LOG, NodeTy::SCALL
];

const ASSIGNER: [NodeTy; 3] = [ NodeTy::Word, NodeTy::PROP, NodeTy::INDEX ];

const MODIFIABLES: [NodeTy;9] = [ NodeTy::CALL, NodeTy::SCALL, NodeTy::ASSIGN,
    NodeTy::RETURN, NodeTy::KwExit, NodeTy::ADD,
    NodeTy::SUB, NodeTy::KwNext, NodeTy::KwBreak ];

const MODIFIERS: [NodeTy;6] = [ NodeTy::KwFor, NodeTy::KwFrom, NodeTy::KwIf,
    NodeTy::KwUntil, NodeTy::KwUnless, NodeTy::KwWhile ];

fn get_oper_level(ty: NodeTy) -> u8 {
    match ty {
        NodeTy::SymPipe => { 1 },
        NodeTy::SymAmp => { 2 },
        NodeTy::SymMaj | NodeTy::SymMin | NodeTy::OpEq | NodeTy::OpMajEq
        | NodeTy::OpMinEq | NodeTy::OpDiff => { 3 },
        NodeTy::SymMinus | NodeTy::SymPlus => { 4 },
        NodeTy::SymMul | NodeTy::SymDiv => { 5 },
        NodeTy::OpCoal => { 6 },
        _ => { 0 }
    }
}

impl Node {

    pub fn leaf(&self) -> Leaf {
        if let NodeData::Leaf(leaf) = &self.data {
            Leaf {
                value: leaf.value.clone(),
                break_before: leaf.break_before,
                space_before: leaf.space_before,
                space_after: leaf.space_after
            }
        }
        else {
            Leaf { value: String::new(), break_before: false, space_before: false,
                space_after: false }
        }
    }

    pub fn is(&self, tys: &[NodeTy]) -> bool {
        tys.contains(&self.ty)
    }

}

#[cfg(test)]
struct NodeInfo { ty: NodeTy, data: String }

#[cfg(test)]
fn get_node_info(node: &Node) -> NodeInfo {
    let mut s = String::new();

    match &node.data {
        NodeData::List { items } => {
            s.push('I');
            s.push_str(items.len().to_string().as_str());
        }
        NodeData::Call { items, .. } => {
            s.push('A');
            s.push_str(items.len().to_string().as_str());
        }
        NodeData::If { block, else_block, elsifs, .. } => {
            s.push('B');
            s.push_str(block.len().to_string().as_str());

            if elsifs.len() > 0 {
                s.push('I');
                s.push_str(elsifs.len().to_string().as_str());
            }

            if let Some(b) = else_block {
                s.push('E');
                s.push_str(b.len().to_string().as_str());
            }
        }
        NodeData::Fx { block, items, name } => {
            s.push('P');
            s.push_str(items.len().to_string().as_str());
            s.push('B');
            s.push_str(block.len().to_string().as_str());
            if name.is_some() {
                s.push('N');
            }
        }
        NodeData::AFx { block, expr, .. } => {
            s.push('B');
            s.push_str(block.len().to_string().as_str());
            if *expr {
                s.push('E');
            }
        }
        NodeData::From { block, .. } => {
            s.push('B');
            s.push_str(block.len().to_string().as_str());
        }
        NodeData::CondBlock { block, .. } => {
            s.push('B');
            s.push_str(block.len().to_string().as_str());
        }
        NodeData::Leaf ( leaf ) => { s = leaf.value.clone() }
        _ => {}
    };

    NodeInfo { ty: node.ty, data: s }
}

impl<'a> Parser<'a> {

    fn node(&mut self, ty: NodeTy, data: NodeData, start: Pos) -> ChildNode {
        let sp = start;
        let ep = self.lexer.last_token.1;

        let n = Node {
            ty, data,
            pos: Pos {
                start_line: sp.start_line,
                start_col: sp.start_col,
                end_line: ep.end_line,
                end_col: ep.end_col
            }
        };

        #[cfg(test)]
        {
            let ni = get_node_info(&n);
            self.res.nodes.push(ni);
        }

        return Box::new(n);
    }

    fn new(lexer: Scanner, res: &'a mut ParseResult) -> Self {
        Parser {
            stacked_tokens: Vec::new(),
            loop_stack: vec![false],
            return_stack: vec![false],
            lexer,
            res
        }
    }

    fn return_token(&mut self, node: ChildNode){
        self.stacked_tokens.push(*node);

        #[cfg(test)]
        self.res.nodes.pop();
    }

    fn next_token(&mut self) -> ChildNode {
        let n;

        if self.stacked_tokens.len() > 0 {
            n = self.stacked_tokens.pop().unwrap();
        }
        else {
            let mut t = self.lexer.scan_unknown();
            while t.0 == NodeTy::Comment || t.0 == NodeTy::LineBreak || t.0 == NodeTy::Space {
                t = self.lexer.scan_unknown();
            }
            n = Node { ty: t.0, pos: t.1, data: NodeData::Leaf(t.2) };

            // println!("{}:{}-{}:{} '{}'",
            // n.pos.start_line, n.pos.start_col, n.pos.end_line,
            // n.pos.end_col, n.leaf().value);
        }



        #[cfg(test)]
        {
            let ni = get_node_info(&n);
            self.res.nodes.push(ni);
        }

        return Box::new(n);
    }

    fn safe_error(&mut self, ty: ErrTy, node: &ChildNode) {
        self.res.errors.push(Err { ty,
            token: Some( (node.ty, node.pos, node.leaf()) ) });
    }

    fn parse_enclosed(&mut self) -> ChildNode {

        // Open parenthesis
        let first = self.next_token();

        let mut items = vec![];

        let mut nt;

        loop {

            nt = self.next_token();
            if nt.ty == NodeTy::SymParCl {
                break;
            }
            self.return_token(nt);

            // TODO spread params
            let i = self.parse_expression();
            // TODO default params
            items.push(i);

            nt = self.next_token();
            if nt.ty == NodeTy::SymComma {
                continue;
            }
            else if nt.ty == NodeTy::SymParCl {
                break;
            }
            else {
                self.safe_error(ErrTy::MissingEncloser, &nt);
                break;
            }
        }

        self.node(NodeTy::LIST, NodeData::List { items }, first.pos)
    }

    fn parse_array(&mut self) -> ChildNode{
        let open = self.next_token();
        let mut items = vec![];

        let mut nt = self.next_token();
        if nt.ty != NodeTy::SymSqCl {
            self.return_token(nt);

            loop {
                let el = self.parse_expression();
                items.push(el);

                nt = self.next_token();
                // TODO allow breaking array item with new line if it would error

                if nt.ty != NodeTy::SymComma {
                    break;
                }
            }

            if nt.ty != NodeTy::SymSqCl {
                self.safe_error(ErrTy::MissingArrayClosing, &nt);
                return nt;
            }
        }

        self.node(NodeTy::ARRAY, NodeData::List { items }, open.pos)
    }

    fn parse_object(&mut self) -> ChildNode{
        let mut pairs = vec![];
        let open = self.next_token();

        let mut nt;

        loop {
            let t1 = self.next_token();
            let key;
            if t1.ty == NodeTy::SymCurCl {
                nt = t1;
                break;
            }
            else if t1.ty == NodeTy::SymSqOp {
                key = self.parse_expression();

                let sq = self.next_token();
                if sq.ty != NodeTy::SymSqCl {
                    self.safe_error(ErrTy::MissingKeyClosingBracket, &sq);
                    self.return_token(sq);
                }
            }
            else if [ NodeTy::Word, NodeTy::Number, NodeTy::String ].contains(&t1.ty){
                key = t1;
            }
            else{
                self.safe_error(ErrTy::UnexpectedObjectKey, &t1);
                key = t1;
                // TODO t1 should probably be Stacked back here
            }

            // TODO fix difference between object Word as a Key or Word as an EVAL
            // TODO allow spread syntax

            let mut val = None;
            let colon = self.next_token();
            if colon.ty == NodeTy::SymColon{
                val = Some(self.parse_expression());
            }
            else {
                self.return_token(colon);
            }

            let p = key.pos;

            pairs.push(self.node(NodeTy::PAIR, NodeData::Pair { key, val }, p));

            nt = self.next_token();

            if nt.ty != NodeTy::SymComma{
                break;
            }
        }

        if nt.ty != NodeTy::SymCurCl {
            self.safe_error(ErrTy::MissingObjectClosing, &nt);
            self.return_token(nt);
        }

        self.node(NodeTy::OBJECT, NodeData::List { items: pairs }, open.pos)
    }

    fn parse_string(&mut self) -> ChildNode {

        let node = self.next_token();

        let mut t1 = self.next_token();
        let mut next = t1.leaf();

        if next.break_before || !t1.is(&[ NodeTy::Concat, NodeTy::Scape, NodeTy::String ]) {
            self.return_token(t1);
            return node;
        }

        let fp = node.pos;
        let mut items = vec![ node ];

        while t1.is(&[ NodeTy::Concat, NodeTy::Scape, NodeTy::String ]) {

            if t1.ty == NodeTy::Scape || t1.ty == NodeTy::String && !next.break_before {
                items.push(t1);
            }

            else if t1.ty == NodeTy::Concat && !next.break_before {
                items.push(self.parse_expression());

                let nt = self.next_token();
                if nt.ty != NodeTy::Concat{
                    self.safe_error(ErrTy::MissingInterpolationClose, &nt);
                    return nt;
                }
            }

            // STRING IN A NEW LINE interrupts the composition
            else {
                break;
            }

            t1 = self.next_token();
            next = t1.leaf();
        }

        if t1.ty == NodeTy::BadString{
            self.safe_error(ErrTy::UnfinishedString, &t1);
            return t1;
        }

        self.return_token(t1);

        self.node(NodeTy::STRING, NodeData::List { items }, fp)
    }

    fn parse_function(&mut self) -> ChildNode{
        let kw = self.next_token();

        let mut params = vec![];

        let mut n = self.next_token();
        let leaf = n.leaf();

        let mut name = None;
        if n.ty == NodeTy::Word && !leaf.break_before {
            name = Some(n);
            n = self.next_token();
        }

        if n.ty == NodeTy::SymParOp && !leaf.break_before {
            n = self.next_token();

            if n.ty != NodeTy::SymParCl {
                self.return_token(n);

                loop {

                    // TODO assert non-word

                    let i = self.parse_expression();
                    params.push(i);
                    // TODO default arg
                    // TODO spread params

                    n = self.next_token();

                    if n.ty != NodeTy::SymComma {
                        break;
                    }
                }

                if n.ty != NodeTy::SymParCl {
                    self.safe_error(ErrTy::MissingEncloser, &n);
                    self.return_token(n);
                }
            }
        }
        else {
            self.return_token(n);
        }

        self.return_stack.push(true);
        self.loop_stack.push(false);
        let block = self.parse_block();
        self.return_stack.pop();
        self.loop_stack.pop();

        self.node(NodeTy::FX,
            NodeData::Fx { items: params, name: name, block }, kw.pos)
    }

    fn parse_log(&mut self) -> ChildNode{
        let kw = self.next_token();

        let mut cn;
        let nt = self.next_token();
        let leaf = nt.leaf();

        if !leaf.space_before && nt.ty == NodeTy::SymParOp {
            cn = self.parse_call(kw);
        }

        // space call
        else if leaf.space_before && !leaf.break_before && nt.is(&EXPR) {
            self.return_token(nt);
            cn = self.parse_space_call(kw);
        }
        else {
            self.safe_error(ErrTy::MissingLogArguments, &nt);
            return kw;
        }

        cn.ty = NodeTy::LOG;

        #[cfg(test)] {
            self.res.nodes.last_mut().unwrap().ty = NodeTy::LOG;
        }

        return cn;
    }

    fn parse_unary(&mut self, ty: NodeTy) -> ChildNode{
        let op = self.next_token();
        let subject = self.parse_expression_opts(ExprOpts {
            oper_level: 6,
            no_obj_s_call: false,
            allow_null: false
        });

        self.node(ty, NodeData::Unary { subject: subject }, op.pos)
    }

    fn parse_primary(&mut self, allow_null: bool) -> ChildNode{
        let token = self.next_token();

        if token.ty == NodeTy::KwNull && allow_null{
            return token;
        }

        if token.ty == NodeTy::BadString{
            self.safe_error(ErrTy::UnfinishedString, &token);
            return token;
        }

        let ty = token.ty;

        self.return_token(token);

        let node = match ty {
            NodeTy::SymExMark => self.parse_unary(NodeTy::NOT),
            NodeTy::SymMinus => self.parse_unary(NodeTy::MINUS),
            NodeTy::KwLog => self.parse_log(),
            NodeTy::KwFx => self.parse_function(),
            NodeTy::SymParOp => self.parse_enclosed(),
            NodeTy::SymSqOp => self.parse_array(),
            NodeTy::SymCurOp => self.parse_object(),
            NodeTy::String => self.parse_string(),
            _ => self.next_token()
        };

        if !node.is(&RETURNABLE) {
            self.safe_error(ErrTy::InvalidOperand, &node);
        }

        return node;
    }

    fn parse_call(&mut self, subject: ChildNode) -> ChildNode {
        // here first parenthesis should be consumed already
        let mut args = vec![];

        let mut nt;
        loop {

            nt = self.next_token();
            let leaf = nt.leaf();
            if nt.ty == NodeTy::SymParCl {
                break;
            }
            else {
                self.return_token(nt);
            }

            args.push(self.parse_expression_opts(ExprOpts {
                no_obj_s_call: false,
                oper_level: 0,
                allow_null: true
            }));

            nt = self.next_token();

            if nt.ty == NodeTy::EOF {
                break;
            }

            if nt.ty != NodeTy::SymParCl && nt.ty != NodeTy::SymComma && !leaf.break_before {
                self.safe_error(ErrTy::MissingCallSeparator, &nt);
            }

            if nt.ty != NodeTy::SymComma {
                self.return_token(nt);
            }
        }

        // here 'nt' is closing parenthesis
        let sp = subject.pos;
        self.node(NodeTy::CALL, NodeData::Call { subject: subject, items: args }, sp)
    }

    fn parse_space_call(&mut self, subject: ChildNode) -> ChildNode {

        let mut args = vec![];
        let mut last;

        loop {

            last = self.parse_expression_opts(ExprOpts {
                no_obj_s_call: false,
                oper_level: 0,
                allow_null: true
            });

            args.push(last);

            let nt = self.next_token();

            if nt.ty != NodeTy::SymComma {
                self.return_token(nt);
                break;
            }
        }

        let sp = subject.pos;
        self.node(NodeTy::SCALL,
            NodeData::Call { subject: subject, items: args }, sp)
    }

    fn parse_ternary(&mut self, cond: ChildNode) -> ChildNode{
        let left = self.parse_expression();

        let nt = self.next_token();
        let mut right = None;
        if nt.ty == NodeTy::SymColon {
            right = Some(self.parse_expression());
        }
        else {
            self.return_token(nt);
        }

        let sp = cond.pos;
        self.node(NodeTy::COND,
            NodeData::Ternary { cond: cond, right, left: left }, sp)
    }

    fn parse_arrow(&mut self, params: ChildNode) -> ChildNode{

        if let NodeData::List { items } = &params.data {
            let mut bad_params = vec![];
            for i in items {
                if i.ty != NodeTy::Word {
                    bad_params.push(i);
                }
            }
            for p in bad_params {
                self.safe_error(ErrTy::UnexpectedParam, &p);
            }
        }
        else if params.ty != NodeTy::Word {
            self.safe_error(ErrTy::UnexpectedParam, &params);
        }

        let nt = self.next_token();
        let ty = nt.ty;
        self.return_token(nt);
        let block;

        let expr = ty != NodeTy::SymCurOp;
        if ty != NodeTy::SymCurOp{
            block = vec![ self.parse_expression() ];
        }
        else{
            self.loop_stack.push(false);
            self.return_stack.push(true);
            block = self.parse_block();
            self.return_stack.pop();
            self.loop_stack.pop();
        }

        let pp = params.pos;
        self.node(NodeTy::AFX, NodeData::AFx { items: params, block, expr }, pp)
    }

    fn parse_expression(&mut self) -> ChildNode{
        return self.parse_expression_opts(ExprOpts {
            allow_null: false,
            oper_level: 0,
            no_obj_s_call: false
        });
    }

    fn parse_expression_opts(&mut self, opts: ExprOpts) -> ChildNode{

        let mut node = self.parse_primary(opts.allow_null);
        let mut next;

        loop {
            let p = node.pos;

            let ol = opts.oper_level;

            next = self.next_token();

            let l = next.leaf();

            let level = get_oper_level(next.ty);

            if next.ty == NodeTy::OpArrow {
                node = self.parse_arrow(node);
            }

            else if next.ty == NodeTy::KwIs && 4 >= ol {
                let rn = self.next_token();
                let rl = rn.leaf();

                if !IS_WORDS.contains(&rl.value.as_str()){
                    self.safe_error(ErrTy::UnknownIsParameter, &rn);
                    return rn;
                }

                node = self.node(NodeTy::IS, NodeData::Binary {
                    left: node, right: rn, op: next
                }, p);
            }

            // Space before means a function execution with array parameter
            else if next.ty == NodeTy::SymSqOp && 7 >= ol && !l.space_before &&
                node.is(&ENCLOSED)
            {
                let property = self.parse_expression_opts(ExprOpts {
                    oper_level: 8,
                    allow_null: false,
                    no_obj_s_call: false
                });

                let cb = self.next_token();
                if cb.ty != NodeTy::SymSqCl{
                    self.safe_error(ErrTy::MissingIndexClosing, &cb);
                    return cb;
                }

                node = self.node(NodeTy::INDEX, NodeData::Binary {
                    op: next, left: node, right: property
                }, p);
            }

            else if next.ty == NodeTy::SymAssign && 0 >= ol && node.is(&ASSIGNER) {
                let rn = self.parse_expression_opts(ExprOpts {
                    oper_level: 0,
                    allow_null: true,
                    no_obj_s_call: false
                });

                let ty = rn.ty;

                node = self.node(NodeTy::ASSIGN, NodeData::Binary {
                    left: node, right: rn, op: next
                }, p);

                // TODO remove null in this case
                if ty == NodeTy::KwNull{
                    return node;
                }
            }

            else if (next.ty == NodeTy::OpSub || next.ty == NodeTy::OpAdd) &&
                0 >= ol && node.is(&ASSIGNER)
            {
                let right = self.parse_expression();
                let ty = if next.ty == NodeTy::OpAdd
                    { NodeTy::ADD } else { NodeTy::SUB };

                node = self.node(ty, NodeData::Binary {
                    left: node, right: right, op: next
                }, p);
            }

            else if next.ty == NodeTy::SymDot && 7 >= ol &&
                !l.space_before && node.is(&ENCLOSED)
            {

                let rn = self.parse_expression_opts(ExprOpts {
                    oper_level: 8,
                    no_obj_s_call: false,
                    allow_null: false
                });

                if l.space_after {
                    self.safe_error(ErrTy::UnexpectedSpaceAfterDot, &rn);
                    return rn;
                }

                if rn.ty != NodeTy::Word {
                    self.safe_error(ErrTy::UnexpectedTokenAfterDot, &rn);
                    return rn;
                }

                node = self.node(NodeTy::PROP, NodeData::Binary {
                    left: node, right: rn, op: next
                }, p);
            }

            else if next.ty == NodeTy::SymParOp && 6 >= ol &&
                !l.space_before && node.is(&CALLABLE)
            {
                node = self.parse_call(node);
            }

            else if next.ty == NodeTy::SymQMark && 0 == ol {
                node = self.parse_ternary(node);
            }

            else if level > 0 && (next.ty != NodeTy::SymMinus || l.space_after) && level >= ol {
                let right = self.parse_expression_opts(ExprOpts {
                    no_obj_s_call: opts.no_obj_s_call,
                    oper_level: 1 + level,
                    allow_null: false
                });

                node = self.node(NodeTy::OPER, NodeData::Binary {
                    op: next, left: node, right: right
                }, p);
            }

            // space call
            else if (!opts.no_obj_s_call || next.ty != NodeTy::SymCurOp) &&
                l.space_before && !l.break_before && node.is(&CALLABLE) &&
                next.is(&EXPR) && 7 > ol
            {
                self.return_token(next);
                node = self.parse_space_call(node);
            }

            else {
                break;
            }
        }

        self.return_token(next);
        return node;
    }

    fn parse_var(&mut self) -> ChildNode{
        let mut items = vec![];

        let kw = self.next_token();
        let mut nt;

        loop {
            let left = self.next_token();
            if left.ty != NodeTy::Word {
                self.safe_error(ErrTy::MissingVarWord, &left);
                return left;
            }

            nt = self.next_token();
            if nt.ty == NodeTy::SymComma {
                items.push(left);
                continue;
            }

            if nt.ty != NodeTy::SymAssign {
                items.push(left);
                break;
            }

            let right = self.parse_expression();
            let lp = left.pos;
            items.push(self.node(NodeTy::ASSIGN, NodeData::Binary {
                op: nt, left: left, right: right
            }, lp));

            nt = self.next_token();

            if nt.ty != NodeTy::SymComma {
                break;
            }
        }

        self.return_token(nt);

        self.node(NodeTy::VAR, NodeData::List { items }, kw.pos)
    }

    fn parse_block(&mut self) -> ChildList {
        let mut nt = self.next_token();
        let l = nt.leaf();

        if nt.ty != NodeTy::SymCurOp && l.break_before {
            self.return_token(nt);
            return vec![ self.parse_statement() ];
        }

        if nt.ty != NodeTy::SymCurOp {
            self.safe_error(ErrTy::MissingBlockOpen, &nt);
            return vec![ nt ];
        }

        let mut array = vec![];
        nt = self.next_token();

        while nt.ty != NodeTy::SymCurCl {
            self.return_token(nt);

            let s = self.parse_statement();

            if s.ty == NodeTy::EOF{
                return array;
            }
            array.push(s);

            nt = self.next_token();
        }

        return array;
    }

    fn parse_modifier(&mut self, kwt: NodeTy, statement: ChildNode) -> ChildNode{
        let s = Some(statement);

        match kwt {
            NodeTy::KwIf => self.parse_if(s),
            NodeTy::KwUnless => self.parse_unless(s),
            NodeTy::KwWhile => self.parse_while(s),
            NodeTy::KwUntil => self.parse_until(s),
            NodeTy::KwFor => self.parse_for(s),
            NodeTy::KwFrom => self.parse_from(s),
            _ => panic!()
        }
    }

    fn parse_if(&mut self, modify: Option<ChildNode>) -> ChildNode{
        let kw = self.next_token();

        let cond = self.parse_expression_opts(ExprOpts {
            no_obj_s_call: true,
            allow_null: false,
            oper_level: 0
        });

        if let Some(s) = modify {
            let sp = s.pos;
            return self.node(NodeTy::IF,
                NodeData::CondBlock { block: vec![s], cond }, sp);
        }

        let block = self.parse_block();

        let mut else_block = None;
        let mut elsifs = vec![];
        let mut nt = self.next_token();
        if nt.ty == NodeTy::KwElsif {
            elsifs = vec![];
            while nt.ty == NodeTy::KwElsif {
                let cond = self.parse_expression_opts(ExprOpts {
                    no_obj_s_call: true,
                    allow_null: false,
                    oper_level: 0
                });
                let block = self.parse_block();
                elsifs.push(self.node(NodeTy::ELSIF,
                    NodeData::CondBlock { block, cond: cond }, nt.pos));
                nt = self.next_token();
            }
        }

        if nt.ty == NodeTy::KwElse {
            else_block = Some(self.parse_block());
            nt = self.next_token();
        }

        self.return_token(nt);

        self.node(NodeTy::IF,
            NodeData::If { block, cond: cond, else_block, elsifs }, kw.pos)
    }

    fn parse_unless(&mut self, modify: Option<ChildNode>) -> ChildNode{
        let kw = self.next_token();

        let cond = self.parse_expression_opts(ExprOpts {
            no_obj_s_call: true,
            allow_null: false,
            oper_level: 0
        });

        let ( sp, block ) = match modify {
            Some(s) => ( s.pos, vec![s] ),
            None => ( kw.pos, self.parse_block() )
        };

        self.node(NodeTy::UNLESS, NodeData::CondBlock { block, cond: cond }, sp)
    }

    fn parse_while(&mut self, modify: Option<ChildNode>) -> ChildNode{
        let kw = self.next_token();

        let cond = self.parse_expression_opts(ExprOpts {
            no_obj_s_call: true,
            allow_null: false,
            oper_level: 0
        });

        self.loop_stack.push(true);

        let ( sp, block ) = match modify {
            Some(s) => ( s.pos, vec![s] ),
            None => ( kw.pos, self.parse_block() )
        };

        self.loop_stack.pop();

        self.node(NodeTy::WHILE, NodeData::CondBlock { block, cond: cond }, sp)
    }

    fn parse_until(&mut self, modify: Option<ChildNode>) -> ChildNode{
        let kw = self.next_token();

        let cond = self.parse_expression_opts(ExprOpts {
            no_obj_s_call: true,
            allow_null: false,
            oper_level: 0
        });

        self.loop_stack.push(true);

        let ( sp, block ) = match modify {
            Some(s) => ( s.pos, vec![s] ),
            None => ( kw.pos, self.parse_block() )
        };

        self.loop_stack.pop();

        self.node(NodeTy::UNTIL, NodeData::CondBlock { block, cond: cond }, sp)
    }

    fn parse_for(&mut self, modify: Option<ChildNode>)-> ChildNode{

        let kw = self.next_token();

        let f = self.next_token();
        if f.ty != NodeTy::Word{
            self.safe_error(ErrTy::MissingForSpecifier, &f);
            // TODO recover from this
            return f;
        }

        let mut key = None;
        let mut val = None;
        let mut count = None;

        let mut n = self.next_token();

        if n.ty == NodeTy::SymColon {
            key = Some(f);
            n = self.next_token();

            if n.ty == NodeTy::Word {
                val = Some(n);
                n = self.next_token();
            }
        }
        else {
            val = Some(f);
        }

        if n.ty != NodeTy::KwOf {
            self.safe_error(ErrTy::MissingOfKeyword, &n);
            self.return_token(n);
        }

        let subject = self.parse_expression_opts(ExprOpts {
            no_obj_s_call: true,
            allow_null: false,
            oper_level: 0
        });

        n = self.next_token();
        if n.ty == NodeTy::KwCount {
            let c = self.next_token();
            let cl = c.leaf();
            if c.ty != NodeTy::Word || cl.break_before{
                self.safe_error(ErrTy::MissingForCounter, &c);
                // TODO recover from this
                return c;
            }
            count = Some(c);
        }
        else {
            self.return_token(n);
        }

        self.loop_stack.push(true);

        let ( sp, block ) = match modify {
            Some(s) => ( s.pos, vec![s] ),
            None => ( kw.pos, self.parse_block() )
        };

        self.loop_stack.pop();

        self.node(NodeTy::FOR,
            NodeData::For { key, val, subject, count, block }, sp)
    }

    fn parse_from(&mut self, modify: Option<ChildNode>)-> ChildNode{
        let kw = self.next_token();
        let start = self.parse_expression();
        let n = self.next_token();
        let op;

        if n.ty == NodeTy::KwTo || n.ty == NodeTy::KwDownto{
            op = n;
        }
        else {
            self.safe_error(ErrTy::MissingTo, &n);
            op = self.node(n.ty, NodeData::Leaf (n.leaf()), n.pos);
            self.return_token(n);
        }

        let end = self.parse_expression_opts(ExprOpts {
            no_obj_s_call: true,
            allow_null: false,
            oper_level: 0
        });

        let mut count = None;
        let c = self.next_token();
        if c.ty == NodeTy::KwCount {
            let c = self.next_token();
            let lc = c.leaf();
            if c.ty != NodeTy::Word || lc.break_before{
                self.safe_error(ErrTy::MissingForCounter, &c);
                // TODO recover from this
                return c;
            }
            count = Some(c);
        }
        else {
            self.return_token(c);
        }

        self.loop_stack.push(true);

        let ( fp, block ) = match modify {
            Some(s) => ( s.pos, vec![s] ),
            None => ( kw.pos, self.parse_block() )
        };

        self.loop_stack.pop();

        self.node(NodeTy::FROM,
            NodeData::From { start, end, op, count, block }, fp)
    }

    fn parse_next(&mut self) -> ChildNode{
        let kw = self.next_token();
        if !self.loop_stack.last().unwrap() {
            self.safe_error(ErrTy::NextNotAllowed, &kw);
        }
        return kw;
    }

    fn parse_break(&mut self) -> ChildNode{
        let kw = self.next_token();
        if !self.loop_stack.last().unwrap() {
            self.safe_error(ErrTy::BreakNotAllowed, &kw);
        }
        return kw;
    }

    fn parse_return(&mut self) -> ChildNode{
        let kw = self.next_token();
        if !self.return_stack.last().unwrap() {
            self.safe_error(ErrTy::ReturnNotAllowed, &kw);
        }
        let subject = self.parse_expression();
        self.node(NodeTy::RETURN, NodeData::Unary { subject }, kw.pos)
    }

    fn parse_statement(&mut self) -> ChildNode {

        let t1 = self.next_token();


        let ty = t1.ty;
        let ll = t1.leaf();
        let expr = t1.is(&EXPR);

        if !ll.break_before && MODIFIERS.contains(&ty) {
            self.safe_error(ErrTy::Unexpected, &t1);
            // TODO probably ignore rest of line
            // TODO add function to skip line for ignoring irrelevant tokens
            return t1;
        }

        self.return_token(t1);

        let mut s = match ty {
            NodeTy::KwVar => self.parse_var(),
            NodeTy::KwFrom => self.parse_from(None),
            NodeTy::KwFor => self.parse_for(None),
            NodeTy::KwIf => self.parse_if(None),
            NodeTy::KwUnless => self.parse_unless(None),
            NodeTy::KwWhile => self.parse_while(None),
            NodeTy::KwUntil => self.parse_until(None),
            NodeTy::KwReturn => self.parse_return(),
            NodeTy::KwNext => self.parse_next(),
            NodeTy::KwBreak => self.parse_break(),
            NodeTy::KwExit => self.next_token(),
            NodeTy::EOF => self.next_token(),
            NodeTy::BadString => {
                let t = self.next_token();
                self.safe_error(ErrTy::Unexpected, &t);
                t
            }
            _ => {
                if expr {
                    self.parse_expression()
                } else {
                    let t = self.next_token();
                    self.safe_error(ErrTy::Unexpected, &t);
                    t
                }
            }
        };

        // Modifiers
        if MODIFIABLES.contains(&s.ty) {
            let kw = self.next_token();
            let kwt = kw.ty;
            let l = kw.leaf();
            self.return_token(kw);

            if !l.break_before && MODIFIERS.contains(&kwt) {
                s = self.parse_modifier(kwt, s);
            }
        }

        return s;
    }

}

pub fn parse(reader: Box<dyn SourceReader>) -> ParseResult {
    let l = Scanner::new(reader);
    let mut ast = vec![];
    let mut r = ParseResult {
        ast: vec![],
        errors: vec![],

        #[cfg(test)]
        nodes: vec![]
    };
    let mut p = Parser::new(l, &mut r);

    let mut s = p.parse_statement();

    while s.ty != NodeTy::EOF {
        ast.push(s);
        s = p.parse_statement();
    }

    r.ast = ast;

    return r;
}

#[cfg(test)]
mod syntatic {
    use super::*;
    use crate::reader::StringSourceReader;

    use ntest::*;

    fn p(st: &str) -> Vec<NodeInfo> {
        let r = StringSourceReader::new(st);
        let r = parse(Box::new(r));

        println!("{:?}", r.errors);
        assert_eq!(r.errors.len(), 0);

        return r.nodes;
    }

    fn f(st: &str, ety: ErrTy) {

        let r = StringSourceReader::new(st);
        let r = parse(Box::new(r));

        assert!(r.errors.len() > 0);
        assert_eq!(r.errors[0].ty, ety);
    }

    #[test]
    #[timeout(500)]
    fn string_scapes() {
        let r = p("\'abc\\ndef\'");
        assert_eq!(r[1].ty, NodeTy::Scape);
        assert_eq!(r[3].ty, NodeTy::STRING);
        assert_eq!(r[3].data, "I3");
    }

    #[test]
    #[timeout(500)]
    fn string_inter() {
        let r = p("\"abc${ 3 + \n 4 }tsk \r foobar\"");
        assert_eq!(r[5].ty, NodeTy::OPER);
        assert_eq!(r[8].data, "I3");

        let a = p("\"abc $b foobar $- \"");
        assert_eq!(a[0].data, "\"abc ");
        assert_eq!(a[4].data, " foobar $- \"");
        assert_eq!(a[5].ty, NodeTy::STRING);
        assert_eq!(a[5].data, "I3");
        assert_eq!(a[2].ty, NodeTy::Word);
        assert_eq!(a[2].data, "b");
    }

    #[test]
    #[timeout(500)]
    fn unterminated_interpolation() {
        f("a = \"abc ${ a + b abc", ErrTy::MissingInterpolationClose);
    }

    #[test]
    #[timeout(500)]
    fn one_string_at_each_line() {
        let r = p("  \"foo\" \n \"bar\"  ");
        assert_eq!(r[0].ty, NodeTy::String);
        assert_eq!(r[1].ty, NodeTy::String);
    }

    #[test]
    #[timeout(500)]
    fn bad_statement() {
        f("$", ErrTy::Unexpected);
    }

    #[test]
    #[timeout(500)]
    fn booleans() {
        let r1 = &p("true")[0];
        assert_eq!(r1.ty, NodeTy::KwTrue);

        let r2 = &p("false")[0];
        assert_eq!(r2.ty, NodeTy::KwFalse);
    }

    #[test]
    #[timeout(500)]
    fn binary_operation() {
        let r = p("b - c");
        assert_eq!(r[0].ty, NodeTy::Word);
        assert_eq!(r[1].ty, NodeTy::SymMinus);
        assert_eq!(r[2].ty, NodeTy::Word);
        assert_eq!(r[3].ty, NodeTy::OPER);
    }

    #[test]
    #[timeout(500)]
    fn unterminated_binary_operation() {
        f("b +", ErrTy::InvalidOperand);
    }

    #[test]
    #[timeout(500)]
    fn stacked_binary_operations() {
        let r = p("a + b + c");
        assert_eq!(r[0].ty, NodeTy::Word);
        assert_eq!(r[1].ty, NodeTy::SymPlus);
        assert_eq!(r[2].ty, NodeTy::Word);
        assert_eq!(r[3].ty, NodeTy::OPER);
        assert_eq!(r[6].ty, NodeTy::OPER);
    }

    #[test]
    #[timeout(500)]
    fn precedenting_binary_operations() {
        let r = p("a * \'abc\' + c");
        assert_eq!(r[0].ty, NodeTy::Word);
        assert_eq!(r[1].ty, NodeTy::SymMul);
        assert_eq!(r[2].ty, NodeTy::String);
        assert_eq!(r[3].ty, NodeTy::OPER);
        assert_eq!(r[6].ty, NodeTy::OPER);

        let a = p("a + \'abc\' * c");
        assert_eq!(a[0].ty, NodeTy::Word);
        assert_eq!(a[1].ty, NodeTy::SymPlus);
        assert_eq!(a[2].ty, NodeTy::String);
        assert_eq!(a[3].ty, NodeTy::SymMul);
        assert_eq!(a[4].ty, NodeTy::Word);
        assert_eq!(a[5].ty, NodeTy::OPER);
        assert_eq!(a[6].ty, NodeTy::OPER);
    }

    #[test]
    #[timeout(500)]
    fn specific_order_binary_operations() {
        let r = p("a * (3 + c)");
        assert_eq!(r[0].ty, NodeTy::Word);
        assert_eq!(r[1].ty, NodeTy::SymMul);
        assert_eq!(r[6].ty, NodeTy::OPER);
        assert_eq!(r[8].ty, NodeTy::LIST);
        assert_eq!(r[9].ty, NodeTy::OPER);

        let a = p("(3 + c) * a");
        assert_eq!(a[4].ty, NodeTy::OPER);
        assert_eq!(a[6].ty, NodeTy::LIST);
        assert_eq!(a[9].ty, NodeTy::OPER);
    }

    #[test]
    #[timeout(500)]
    fn missing_closing_parenthesis() {
        f("(a + b", ErrTy::MissingEncloser);
    }

    #[test]
    #[timeout(500)]
    fn not_operator() {
        let r = p("!b");
        assert_eq!(r[1].ty, NodeTy::Word);
        assert_eq!(r[2].ty, NodeTy::NOT);

        let a = p("!b[0]");
        assert_eq!(a[5].ty, NodeTy::INDEX);
        assert_eq!(a[6].ty, NodeTy::NOT);

        let b = p("!\'a\'.b");
        assert_eq!(b[4].ty, NodeTy::PROP);
        assert_eq!(b[5].ty, NodeTy::NOT);

        let c = p("!(a + b)");
        assert_eq!(c[7].ty, NodeTy::LIST);
        assert_eq!(c[8].ty, NodeTy::NOT);
    }

    #[test]
    #[timeout(500)]
    fn unary_minus_operator() {
        let r = p("-b");
        assert_eq!(r[1].ty, NodeTy::Word);
        assert_eq!(r[2].ty, NodeTy::MINUS);

        let a = p("-b[0]");
        assert_eq!(a[5].ty, NodeTy::INDEX);
        assert_eq!(a[6].ty, NodeTy::MINUS);

        let b = p("-\'a\'.b");
        assert_eq!(b[4].ty, NodeTy::PROP);
        assert_eq!(b[5].ty, NodeTy::MINUS);

        let c = p("-(a + b)");
        assert_eq!(c[7].ty, NodeTy::LIST);
        assert_eq!(c[8].ty, NodeTy::MINUS);
    }

    #[test]
    #[timeout(500)]
    fn assignment() {
        let r = p("a = 2");
        assert_eq!(r[3].ty, NodeTy::ASSIGN);
    }

    #[test]
    #[timeout(500)]
    fn assignment_of_assignment() {
        let r = p("a = b = 3");
        assert_eq!(r[5].ty, NodeTy::ASSIGN);
        assert_eq!(r[6].ty, NodeTy::ASSIGN);
    }

    #[test]
    #[timeout(500)]
    fn assign_null_value() {
        let r = p("a = null");
        assert_eq!(r[3].ty, NodeTy::ASSIGN);
    }

    #[test]
    #[timeout(500)]
    fn property_access() {
        let r = p("(3 + 5).b.c");
        assert_eq!(r[4].ty, NodeTy::OPER);
        assert_eq!(r[6].ty, NodeTy::LIST);
        assert_eq!(r[9].ty, NodeTy::PROP);
        assert_eq!(r[12].ty, NodeTy::PROP);
    }

    #[test]
    #[timeout(500)]
    fn space_after_dot_operator() {
        f("a. b", ErrTy::UnexpectedSpaceAfterDot);
    }

    #[test]
    #[timeout(500)]
    fn bad_property_token_after_dot() {
        f("a.\'foo\'", ErrTy::UnexpectedTokenAfterDot);
    }

    #[test]
    #[timeout(500)]
    fn property_access_inside_expressions() {
        let r = p("abc + 3.b.c");
        assert_eq!(r[5].ty, NodeTy::PROP);
        assert_eq!(r[8].ty, NodeTy::PROP);
        assert_eq!(r[9].ty, NodeTy::OPER);

        let a = p("abc = 5 + 3.b.c");
        assert_eq!(a[7].ty, NodeTy::PROP);
        assert_eq!(a[10].ty, NodeTy::PROP);
        assert_eq!(a[11].ty, NodeTy::OPER);
        assert_eq!(a[12].ty, NodeTy::ASSIGN);

        let b = p("abc = 3.b.c + 7");
        assert_eq!(b[5].ty, NodeTy::PROP);
        assert_eq!(b[8].ty, NodeTy::PROP);
        assert_eq!(b[11].ty, NodeTy::OPER);
        assert_eq!(b[12].ty, NodeTy::ASSIGN);
    }

    #[test]
    #[timeout(500)]
    fn assignment_to_property_access() {
        let r = p("a.b.c = a.b");
        assert_eq!(r[6].ty, NodeTy::PROP);
        assert_eq!(r[11].ty, NodeTy::PROP);
        assert_eq!(r[12].ty, NodeTy::ASSIGN);
    }

    #[test]
    #[timeout(500)]
    fn index_access() {
        let r = p("a.b[\"c\"]");
        assert_eq!(r[3].ty, NodeTy::PROP);
        assert_eq!(r[7].ty, NodeTy::INDEX);
    }

    #[test]
    #[timeout(500)]
    fn assignment_to_index_access() {
        let r = p("a[b][\'c\'] = a[0]");
        assert_eq!(r[4].ty, NodeTy::INDEX);
        assert_eq!(r[8].ty, NodeTy::INDEX);
        assert_eq!(r[14].ty, NodeTy::INDEX);
        assert_eq!(r[15].ty, NodeTy::ASSIGN);
    }

    #[test]
    #[timeout(500)]
    fn missing_closing_bracket_on_index_access() {
        f("abc[0", ErrTy::MissingIndexClosing);
    }

    #[test]
    #[timeout(500)]
    fn array_literal() {
        let r = p("[ 1, 2, 3 ]");
        assert_eq!(r[5].ty, NodeTy::Number);
        assert_eq!(r[7].ty, NodeTy::ARRAY);

        let a = p("[ ]");
        assert_eq!(a[2].ty, NodeTy::ARRAY);
    }

    #[test]
    #[timeout(500)]
    fn array_of_arrays() {
        let r = p("[ [ 1 ], [ 2, 3 ] ]");
        assert_eq!(r[4].ty, NodeTy::ARRAY);
        assert_eq!(r[11].ty, NodeTy::ARRAY);
        assert_eq!(r[13].ty, NodeTy::ARRAY);
    }

    #[test]
    #[timeout(500)]
    fn missing_array_closing() {
        f("[a, b, 1", ErrTy::MissingArrayClosing);
    }

    #[test]
    #[timeout(500)]
    fn object_literal() {
        let r = p("{ a: 3, \"r\": abc, [1 + 2]: true }");
        assert_eq!(r[4].ty, NodeTy::PAIR);
        assert_eq!(r[21].ty, NodeTy::OBJECT);
    }

    #[test]
    #[timeout(500)]
    fn missing_closing_bracket_in_object_key() {
        f("{ [a + b: \'a\' }", ErrTy::MissingKeyClosingBracket);
    }

    #[test]
    #[timeout(500)]
    fn nested_objects() {
        let r = p("{ a: { b }, c: 1, }");
        assert_eq!(r[5].ty, NodeTy::PAIR);
        assert_eq!(r[7].ty, NodeTy::OBJECT);
        assert_eq!(r[8].ty, NodeTy::PAIR);
        assert_eq!(r[16].ty, NodeTy::OBJECT);
    }

    #[test]
    #[timeout(500)]
    fn bad_object_key() {
        f("{ true: \"abc\" }", ErrTy::UnexpectedObjectKey);
    }

    #[test]
    #[timeout(500)]
    fn missing_object_closing_brace() {
        f("{ cool: \"abc\"", ErrTy::MissingObjectClosing);
    }

    #[test]
    #[timeout(500)]
    fn is_operator() {
        let r = p("a is empty");
        assert_eq!(r[3].ty, NodeTy::IS);
    }

    #[test]
    #[timeout(500)]
    fn is_null_operation() {
        let r = p("a + b is null");
        assert_eq!(r[3].ty, NodeTy::OPER);
        assert_eq!(r[5].ty, NodeTy::KwNull);
        assert_eq!(r[6].ty, NodeTy::IS);
    }

    #[test]
    #[timeout(500)]
    fn is_with_wrong_parameter() {
        f("a is foobar", ErrTy::UnknownIsParameter);
    }

    #[test]
    #[timeout(500)]
    fn function_call() {
        let r = p("abc()");
        assert_eq!(r[3].ty, NodeTy::CALL);
    }

    #[test]
    #[timeout(500)]
    fn function_call_arguments() {
        let r = p("abc(a)");
        assert_eq!(r[4].ty, NodeTy::CALL);
        assert_eq!(r[4].data, "A1");

        let a = p("foo.abc(a, [ 2, 5 ], (true), null)");
        assert_eq!(a[21].ty, NodeTy::CALL);
        assert_eq!(a[21].data, "A4");
    }

    #[test]
    #[timeout(500)]
    fn function_call_of_a_call() {
        let r = p("abc(a)(a, b, c)");
        assert_eq!(r[4].ty, NodeTy::CALL);
        assert_eq!(r[4].data, "A1");
        assert_eq!(r[12].ty, NodeTy::CALL);
        assert_eq!(r[12].data, "A3");
    }

    #[test]
    #[timeout(500)]
    fn malformed_call() {
        f("abc(1, 2\n 4)", ErrTy::MissingCallSeparator);
    }

    #[test]
    #[timeout(500)]
    fn space_oriented_function_call() {
        let r = p("abc a");
        assert_eq!(r[2].ty, NodeTy::SCALL);
        assert_eq!(r[2].data, "A1");

        let a = p("foo.abc a, [ 2, 5 ], (true), null");
        assert_eq!(a[19].ty, NodeTy::SCALL);
        assert_eq!(a[19].data, "A4");
    }

    #[test]
    #[timeout(500)]
    fn var_definition() {
        let r = p("var a, b, c");
        assert_eq!(r[6].ty, NodeTy::VAR);
        assert_eq!(r[6].data, "I3");
    }

    #[test]
    #[timeout(500)]
    fn var_assignment() {
        let r = p("var a = 8");
        assert_eq!(r[4].ty, NodeTy::ASSIGN);
        assert_eq!(r[5].ty, NodeTy::VAR);
        assert_eq!(r[5].data, "I1");
    }

    #[test]
    #[timeout(500)]
    fn bad_expression_on_var() {
        f("var a, 5", ErrTy::MissingVarWord);
    }

    #[test]
    #[timeout(500)]
    fn add_assignment() {
        let r = p("a.b += 2");
        assert_eq!(r[3].ty, NodeTy::PROP);
        assert_eq!(r[6].ty, NodeTy::ADD);
    }

    #[test]
    #[timeout(500)]
    fn add_assignment_of_assignment() {
        let r = p("a += b = 3");
        assert_eq!(r[5].ty, NodeTy::ASSIGN);
        assert_eq!(r[6].ty, NodeTy::ADD);
    }

    #[test]
    #[timeout(500)]
    fn subtract_assignment() {
        let r = p("a -= 2");
        assert_eq!(r[3].ty, NodeTy::SUB);
    }

    #[test]
    #[timeout(500)]
    fn subtract_assignment_of_assignment() {
        let r = p("a -= b = 3");
        assert_eq!(r[5].ty, NodeTy::ASSIGN);
        assert_eq!(r[6].ty, NodeTy::SUB);
    }

    #[test]
    #[timeout(500)]
    fn if_statement() {
        let r = p("if a + b { }");
        assert_eq!(r[4].ty, NodeTy::OPER);
        assert_eq!(r[7].ty, NodeTy::IF);
        assert_eq!(r[7].data, "B0");

        let a = p("if test \"123\" {\n a = b \n c = d  }");
        assert_eq!(a[3].ty, NodeTy::SCALL);
        assert_eq!(a[8].ty, NodeTy::ASSIGN);
        assert_eq!(a[12].ty, NodeTy::ASSIGN);
        assert_eq!(a[14].ty, NodeTy::IF);
        assert_eq!(a[14].data, "B2");
    }

    #[test]
    #[timeout(500)]
    fn if_elsif_statement() {
        let r = p("if a \n a = 1 \n elsif b \n c = 2 \n elsif b > a \n c = 0");
        assert_eq!(r[12].ty, NodeTy::ELSIF);
        assert_eq!(r[22].ty, NodeTy::ELSIF);
        assert_eq!(r[22].data, "B1");
        assert_eq!(r[23].ty, NodeTy::IF);
        assert_eq!(r[23].data, "B1I2");
    }

    #[test]
    #[timeout(500)]
    fn if_else_statement() {
        let r = p("if a + b \n a = 1 \n else \n c = true");
        assert_eq!(r[4].ty, NodeTy::OPER);
        assert_eq!(r[14].ty, NodeTy::IF);
        assert_eq!(r[14].data, "B1E1");
    }

    #[test]
    #[timeout(500)]
    fn missing_block_start() {
        f("if a + b }", ErrTy::MissingBlockOpen);
    }

    #[test]
    #[timeout(500)]
    fn issues_inside_blocks() {
        f("if a + b { \n a.b(  }", ErrTy::InvalidOperand);
    }

    #[test]
    #[timeout(500)]
    fn unless_statement() {
        let r = p("unless a + b { }");
        assert_eq!(r[4].ty, NodeTy::OPER);
        assert_eq!(r[7].ty, NodeTy::UNLESS);
        assert_eq!(r[7].data, "B0");

        let a = p("unless test \"123\" {\n a = b \n c = d  }");
        assert_eq!(a[3].ty, NodeTy::SCALL);
        assert_eq!(a[8].ty, NodeTy::ASSIGN);
        assert_eq!(a[12].ty, NodeTy::ASSIGN);
        assert_eq!(a[14].ty, NodeTy::UNLESS);
    }

    #[test]
    #[timeout(500)]
    fn while_statement() {
        let r = p("while a + b { }");
        assert_eq!(r[4].ty, NodeTy::OPER);
        assert_eq!(r[7].ty, NodeTy::WHILE);
        assert_eq!(r[7].data, "B0");

        let a = p("while test \"123\" {\n a = b \n c = d  }");
        assert_eq!(a[3].ty, NodeTy::SCALL);
        assert_eq!(a[8].ty, NodeTy::ASSIGN);
        assert_eq!(a[12].ty, NodeTy::ASSIGN);
        assert_eq!(a[14].ty, NodeTy::WHILE);
    }

    #[test]
    #[timeout(500)]
    fn until_statement() {
        let r = p("until a + b { }");
        assert_eq!(r[4].ty, NodeTy::OPER);
        assert_eq!(r[7].ty, NodeTy::UNTIL);
        assert_eq!(r[7].data, "B0");

        let a = p("until test \"123\" {\n a = b \n c = d  }");
        assert_eq!(a[3].ty, NodeTy::SCALL);
        assert_eq!(a[8].ty, NodeTy::ASSIGN);
        assert_eq!(a[12].ty, NodeTy::ASSIGN);
        assert_eq!(a[14].ty, NodeTy::UNTIL);
    }

    #[test]
    #[timeout(500)]
    fn regexp() {
        let r = p("a = /^$%&%&*[a-z]$/g");
        assert_eq!(r[2].ty, NodeTy::RegExp);

        let a = p("!/abc\\/abc/");
        assert_eq!(a[1].ty, NodeTy::RegExp);
    }

    #[test]
    #[timeout(500)]
    fn arrow_function() {
        let r = p("a = () => 24 + 3");
        assert_eq!(r[4].data, "I0");
        assert_eq!(r[9].ty, NodeTy::OPER);
        assert_eq!(r[10].ty, NodeTy::AFX);
        assert_eq!(r[10].data, "B1E");
        assert_eq!(r[11].ty, NodeTy::ASSIGN);

        let a = p("(e, b) => { \n a = b <= c \n}");
        assert_eq!(a[5].ty, NodeTy::LIST);
        assert_eq!(a[5].data, "I2");
        assert_eq!(a[14].ty, NodeTy::ASSIGN);
        assert_eq!(a[16].ty, NodeTy::AFX);
        assert_eq!(a[16].data, "B1");

        let b = p("e => { \n a = b + c \n b = c + d \n}");
        assert_eq!(b[16].ty, NodeTy::ASSIGN);
        assert_eq!(b[18].ty, NodeTy::AFX);
        assert_eq!(b[18].data, "B2");
    }

    #[test]
    #[timeout(500)]
    fn bad_arrow_function_param() {
        f("(a, 2) => \"what\"", ErrTy::UnexpectedParam);
        f("2 => \"what\"", ErrTy::UnexpectedParam);
    }

    #[test]
    #[timeout(500)]
    fn function_definition() {

        let r = p("fx foobar {\n 24 + 3 }");
        assert_eq!(r[1].ty, NodeTy::Word);
        assert_eq!(r[8].ty, NodeTy::FX);
        assert_eq!(r[8].data, "P0B1N");

        let a = p("fx foobar(a, b) \n 24 + 3 \n a = 2");
        assert_eq!(a[1].ty, NodeTy::Word);
        assert_eq!(a[11].ty, NodeTy::FX);
        assert_eq!(a[11].data, "P2B1N");
        assert_eq!(a[15].ty, NodeTy::ASSIGN);

        let b = p("fx(a, b) \n 24 + 3 \n a = 2");
        assert_eq!(b[10].ty, NodeTy::FX);
        assert_eq!(b[10].data, "P2B1");

        let c = p("fx \n a + 3");
        assert_eq!(c[5].ty, NodeTy::FX);
        assert_eq!(c[5].data, "P0B1");
    }

    #[test]
    #[timeout(500)]
    fn return_statement() {

        let r = p("fx() { return  24 / 3 }");
        assert_eq!(r[8].ty, NodeTy::OPER);
        assert_eq!(r[9].ty, NodeTy::RETURN);

        let a = p("fx(a, b) \n return \n a = 2");
        assert_eq!(a[10].ty, NodeTy::ASSIGN);
        assert_eq!(a[11].ty, NodeTy::RETURN);
    }

    #[test]
    #[timeout(500)]
    fn return_where_its_not_allowed() {
        f("return \"foobar\"", ErrTy::ReturnNotAllowed);
    }

    #[test]
    #[timeout(500)]
    fn break_statement() {
        let r = p("while true \n break");
        assert_eq!(r[2].ty, NodeTy::KwBreak);
    }

    #[test]
    #[timeout(500)]
    fn break_where_its_not_allowed() {
        f("break", ErrTy::BreakNotAllowed);
    }

    #[test]
    #[timeout(500)]
    fn next_statement() {
        let r = p("while true \n next");
        assert_eq!(r[2].ty, NodeTy::KwNext);
    }

    #[test]
    #[timeout(500)]
    fn next_where_its_not_allowed() {
        f("next", ErrTy::NextNotAllowed);
    }

    #[test]
    #[timeout(500)]
    fn bad_function_param_list() {
        f("fx (a, 2 \n \"what\"", ErrTy::MissingEncloser);
    }

    #[test]
    #[timeout(500)]
    fn exit_statement() {
        let r = p("exit");
        assert_eq!(r[0].ty, NodeTy::KwExit);
    }

    #[test]
    #[timeout(500)]
    fn ternary() {
        let r = p("a ? 2");
        assert_eq!(r[3].ty, NodeTy::COND);

        let a = p("a == 3 ? 2 : abc");
        assert_eq!(a[8].ty, NodeTy::COND);

        let b = p("a & 3 ? 2 : abc");
        assert_eq!(b[3].ty, NodeTy::OPER);
        assert_eq!(b[8].ty, NodeTy::COND);

        let c = p("a | 3 ? foo ? bar : \"baz\" : abc");
        assert_eq!(c[3].ty, NodeTy::OPER);
        assert_eq!(c[10].ty, NodeTy::COND);
        assert_eq!(c[13].ty, NodeTy::COND);
    }

    #[test]
    #[timeout(500)]
    fn for_statement() {

        let r = p("for k: of subj { exit }");
        assert_eq!(r[8].ty, NodeTy::FOR);

        let a = p("for v of subj { exit }");
        assert_eq!(a[7].ty, NodeTy::FOR);

        let b = p("for k: v of subj \n exit");
        assert_eq!(b[7].ty, NodeTy::FOR);

        let c = p("for k: v of subj count i { exit }");
        assert_eq!(c[11].ty, NodeTy::FOR);
    }

    #[test]
    #[timeout(500)]
    fn bad_for_speficier() {
        f("for 1 of data {  }", ErrTy::MissingForSpecifier);
    }

    #[test]
    #[timeout(500)]
    fn missing_for_of() {
        f("for a data {  }", ErrTy::MissingOfKeyword);
    }

    #[test]
    #[timeout(500)]
    fn bad_for_counter() {
        f("for a of data count {  }", ErrTy::MissingForCounter);
    }

    #[test]
    #[timeout(500)]
    fn from_statement() {

        let r = p("from 0 to 100 { exit }");
        assert_eq!(r[7].ty, NodeTy::FROM);

        let a = p("from a to 100 { exit }");
        assert_eq!(a[7].ty, NodeTy::FROM);

        let b = p("from a = 0 downto a \n exit");
        assert_eq!(b[8].ty, NodeTy::FROM);

        let c = p("from a downto b = 2 { exit }");
        assert_eq!(c[10].ty, NodeTy::FROM);

        let d = p("from a downto b = 2 count i { exit }");
        assert_eq!(d[12].ty, NodeTy::FROM);
    }

    #[test]
    #[timeout(500)]
    fn bad_from_to() {
        f("from a 2 {  }", ErrTy::MissingTo);
        f("from a to 2 count {  }", ErrTy::MissingForCounter);
    }

    #[test]
    #[timeout(500)]
    fn control_modifiers() {

        let r = p("a() unless c");
        assert_eq!(r[3].ty, NodeTy::CALL);
        assert_eq!(r[6].ty, NodeTy::UNLESS);

        let a = p("a = 2 if c < 2");
        assert_eq!(a[3].ty, NodeTy::ASSIGN);
        assert_eq!(a[9].ty, NodeTy::IF);

        let b = p("exit while true");
        assert_eq!(b[0].ty, NodeTy::KwExit);
        assert_eq!(b[3].ty, NodeTy::WHILE);

        let c = p("a[0] = 2 until abc()");
        assert_eq!(c[7].ty, NodeTy::ASSIGN);
        assert_eq!(c[13].ty, NodeTy::UNTIL);

        let d = p("a -= \"string\" for a: of abc");
        assert_eq!(d[3].ty, NodeTy::SUB);
        assert_eq!(d[9].ty, NodeTy::FOR);

        let e = p("a = null from 1 to 100");
        assert_eq!(e[3].ty, NodeTy::ASSIGN);
        assert_eq!(e[8].ty, NodeTy::FROM);
    }

    #[test]
    #[timeout(500)]
    fn modifying_bad_statement() {
        f("a unless b", ErrTy::Unexpected);
    }

    #[test]
    #[timeout(500)]
    fn log_expression() {
        let r = p("a ? log 1");
        assert_eq!(r[4].ty, NodeTy::LOG);
        assert_eq!(r[4].data, "A1");
        assert_eq!(r[5].ty, NodeTy::COND);

        let a = p("log(abc)");
        assert_eq!(a[4].ty, NodeTy::LOG);
        assert_eq!(a[4].data, "A1");

        let b = p("log(a, [ 2, 5 ], (true), null)");
        assert_eq!(b[18].ty, NodeTy::LOG);
        assert_eq!(b[18].data, "A4");

        let c = p("log a, [ 2, 5 ], (true), null");
        assert_eq!(c[16].ty, NodeTy::LOG);
        assert_eq!(c[16].data, "A4");
    }

    #[test]
    #[timeout(500)]
    fn log_without_arguments() {
        f("log", ErrTy::MissingLogArguments);
    }

}