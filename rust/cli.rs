// import { rgb8, dim } from 'https://deno.land/std@0.95.0/fmt/colors.ts';

const GITLAB_RELEASES: &str = "https://gitlab.com/api/v4/projects/25788257/releases";
const CURRENT_VERSION: &str = "a0";

use std::env;
use std::path::Path;
use std::io::copy;
use std::fs::{File, rename, remove_file};
use flate2::read::GzDecoder;
use tar::Archive;
use clap::{Arg, App, AppSettings};
use serde_json;

mod errors;
mod reader;
mod lex;
mod syn;
mod scripter;

use crate::errors::format_err_list;
use crate::reader::FileSourceReader;
use crate::syn::parse;
use crate::scripter::Scripter;

fn main() {
    let matches = App::new("One")
        .version(CURRENT_VERSION)
        .about("One language to code them all")
        .setting(AppSettings::SubcommandRequiredElseHelp)

        .subcommand(App::new("dump")
            .about("Transpile input file and stream Deno script to stdout")
            .arg(Arg::new("input")
                .required(true)
                .index(1)
                .about("Source file to be transpiled")
                .value_name("FILE")
            )
        )

        .subcommand(App::new("update")
            .about("Update the One binary to the latest version")
            .arg(Arg::new("dry-run")
                .about("Only check latest release without installing it")
                .short('d')
                .long("dry-run")
            )
        )

        .get_matches();


    if let Some(ref matches) = matches.subcommand_matches("dump") {

        let path = matches.value_of("input").unwrap();

        // TODO make timeout optional
        use std::thread;
        use std::time::Duration;
        thread::spawn(|| {
            thread::sleep(Duration::from_millis(50));
            std::process::exit(1);
        });

        let r = FileSourceReader::new(path);
        let r = parse(Box::new(r));

        if r.errors.len() > 0 {
        //     // TODO REMOVE THIS
        //     Deno.writeTextFile('./diag-syntax.log',
        //         JSON.stringify(data.errors) + '\r\n', { append: true });
            eprintln!("{}", format_err_list(r.errors));
            std::process::exit(1);
        }

        // Deno.writeTextFile('./diag-error.log', JSON.stringify(err) +
            // '\r\n', { append: true });

        let s = Scripter::transpile(r.ast);
        println!("{}", s);
    }

    else if let Some(ref matches) = matches.subcommand_matches("update") {

        println!("\n🔎 \u{001b}[2mLooking up latest version\u{001b}[0m");

        let res: serde_json::Value = reqwest::blocking::get(GITLAB_RELEASES).unwrap().json().unwrap();

        let latest = res.get(0).unwrap();
        let tag = latest["tag_name"].as_str().unwrap();

        if tag == CURRENT_VERSION {
            return println!("✔️  Local version \u{001b}[38;5;153m{}\u{001b}[0m is up to date\n", CURRENT_VERSION);
        }

        println!("✨ Found latest version \u{001b}[38;5;40m{}\u{001b}[0m", tag);

        if matches.is_present("dry-run") {
            return println!("\n{}\n", latest["description"].as_str().unwrap());
        }

        println!("🌏 \u{001b}[2mDownloading package\u{001b}[0m");

        let os = env::consts::OS;

        let links = latest["assets"]["links"].as_array().unwrap();
        let link = links.iter().find(|l|
            l["name"].as_str().unwrap().to_owned().contains(&os)
        ).unwrap();

        let url = link["url"].as_str().unwrap();
        let mut source = reqwest::blocking::get(url).unwrap();

        let exe_path_str = std::env::current_exe().unwrap();
        let cur_bin = Path::new(&exe_path_str);
        let parent_dir = cur_bin.parent().unwrap();

        let tar_name = Path::new(&url).file_name().unwrap();
        let tar_path_str = parent_dir.join(tar_name);

        let mut dest = File::create(&tar_path_str).unwrap();
        if let Err(e) = copy(&mut source, &mut dest) {
            println!("\u{001b}[38;5;160m⚠️  {}\u{001b}[0m\n", e);
        };

        println!("📦 \u{001b}[2mUnpacking binary\u{001b}[0m"); // dim('Unpacking binary'));
        let mut new_bin_name = String::new();
        new_bin_name.push_str("one.");
        new_bin_name.push_str(tag);

        let tar_gz = match File::open(&tar_path_str) {
            Ok(r) => r,
            Err(e) => { return println!("\u{001b}[38;5;160m⚠️  {}\u{001b}[0m\n", e) }
        };

        let tar = GzDecoder::new(tar_gz);
        let mut archive = Archive::new(tar);

        if let Err(_e) = archive.unpack(parent_dir) {
            return println!("\u{001b}[38;5;208m👮 Please, try again with admin rights\u{001b}[0m\n");
        }

        if let Err(e) = remove_file(tar_path_str) {
            return println!("\u{001b}[38;5;160m⚠️  {}\u{001b}[0m\n", e);
        }

        println!("⚙️  \u{001b}[2mInstalling new binary\u{001b}[0m");
        let mut old_bin_name = String::new();
        old_bin_name.push_str("one.");
        old_bin_name.push_str(CURRENT_VERSION);

        let old_bin_path = parent_dir.join(old_bin_name);
        if let Err(e) = rename(cur_bin, old_bin_path) {
            return println!("\u{001b}[38;5;160m⚠️  {}\u{001b}[0m\n", e);
        }

        if let Err(e) = rename(new_bin_name, cur_bin) {
            return println!("\u{001b}[38;5;160m⚠️  {}\u{001b}[0m\n", e);
        }

        println!("💍 Update complete. Version is now \u{001b}[38;5;40m{}\u{001b}[0m\n", tag);
    }
}
