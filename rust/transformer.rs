
pub trait Transformer<A, B, T> {

    fn OPER(&self, node: A)-> B;
    fn ASSIGN(&self, node: A)-> B;
    fn IF(&self, node: A)-> B;
    fn FOR(&self, node: A)-> B;
    fn UNLESS(&self, node: A)-> B;
    fn FROM(&self, node: A)-> B;
    fn WHILE(&self, node: A)-> B;
    fn UNTIL(&self, node: A)-> B;
    fn COND(&self, node: A)-> B;
    fn LIST(&self, node: A)-> B;
    fn CALL(&self, node: A)-> B;
    fn SCALL(&self, node: A)-> B;
    fn ARRAY(&self, node: A)-> B;
    fn OBJECT(&self, node: A)-> B;
    fn IS(&self, node: A)-> B;
    fn ADD(&self, node: A)-> B;
    fn SUB(&self, node: A)-> B;
    fn NOT(&self, node: A)-> B;
    fn MINUS(&self, node: A)-> B;
    fn PROP(&self, node: A)-> B;
    fn INDEX(&self, node: A)-> B;
    fn VAR(&self, node: A)-> B;
    fn AFX(&self, node: A)-> B;
    fn RETURN(&self, node: A)-> B;
    fn LOG(&self, node: A)-> B;
    fn FX(&self, node: A)-> B;
    fn PAIR(&self, node: A)-> B;
    fn STRING(&self, node: A)-> B;

    fn get_type(&self, node: A)-> T;
    fn get_value(&self, node: A)-> B;

    fn expand(&self, node: A)-> B;
    fn statements(&self, node: Vec<A>) -> Vec<B>
}

impl Transformer<A, B, T> {

    fn expand(node: A)-> B{

        let t = self.get_type(node);

        if type in this.spec
            return this.spec[type].call(this, token);

        return this.getValue(token);
    }

}



type TransformerSpec<A, B> = Record<string, (token: A) => B>;

pub  abstract class Transformer<A, B> {

    spec-> TransformerSpec<A, B>;
    statementList-> B[][] = [];
    functionContext: boolean[] = [];

    abstract getType(something: unknown): string;
    abstract getValue(token: A)-> B;

    expand(token?: A)-> B{
        if(!token)
            throw new Error('Invalid token');

        const type = this.getType(token);
        if(type in this.spec)
            return this.spec[type].call(this, token);

        return this.getValue(token);
    }

    statements(tokens: A[])-> B[]{
        const list-> B[] = [];
        this.statementList.push(list);

        for(const t of tokens){
            if(this.getType(t) == 'EOF')
                break;
            list.push(this.expand(t));
        }

        this.statementList.pop();
        return list;
    }

}
