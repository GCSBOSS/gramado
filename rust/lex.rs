
#[derive(PartialEq, Copy, Clone, PartialOrd, Debug)]
pub enum NodeTy {
    EOF, LineBreak, Space, Invalid, BadComment, BadString,

    Word, Number, RegExp, Comment, String, Scape, Concat,

    OpAdd, OpSub, OpArrow,

    // Binary Operators
    OpEq = 20, OpDiff = 21, OpMajEq = 22, OpMinEq = 23, OpCoal = 24,
    SymMul = 25, SymDiv = 26, SymPlus = 27, SymMinus = 28, SymPipe = 29,
    SymAmp = 30, SymMin = 31, SymMaj = 32,

    SymExMark, SymAssign, SymParCl, SymParOp, SymCurCl, SymCurOp, SymSqCl,
    SymSqOp, SymComma, SymDot, SymQMark, SymColon,

    KwBreak,   KwCount,  KwDownto,  KwElse,    KwElsif,  KwExit,  KwFalse,  KwFor,
    KwFrom,    KwFx,     KwIf,      KwIs,      KwLog,    KwNext,  KwNull,   KwOf,
    KwReturn,  KwTo,     KwTrue,    KwUnless,  KwUntil,  KwVar,   KwWhile,

    OPER, ASSIGN, IF, FOR, UNLESS, FROM, WHILE, UNTIL, COND, LIST, CALL, SCALL,
    ARRAY, OBJECT, IS, ADD, SUB, NOT, MINUS, PROP, INDEX, VAR, AFX, RETURN, LOG,
    FX, PAIR, STRING, ELSIF
}

#[derive(Debug)]
pub struct Leaf {
    pub value: String,
    pub space_before: bool,
    pub space_after: bool,
    pub break_before: bool
}

const REGEXP_PREV: [NodeTy; 7] = [ NodeTy::SymExMark, NodeTy::SymParOp,
    NodeTy::SymCurCl, NodeTy::SymCurOp, NodeTy::SymSqOp, NodeTy::SymComma,
    NodeTy::SymAssign ];

#[derive(Copy, Clone, Debug)]
pub struct Pos {
    pub end_line: isize,
    pub start_line: isize,
    pub end_col: isize,
    pub start_col: isize
}

pub fn view_token(token: &Token) -> Token {
    let (ty, pos, leaf) = token;
    return (*ty, *pos, Leaf {
        value: leaf.value.clone(),
        space_before: leaf.space_before,
        space_after: leaf.space_after,
        break_before: leaf.break_before
    })
}

use crate::reader::SourceReader;

#[derive(PartialEq)]
enum StringMode { None, String, Inter, Word, DoneWord }

pub struct Scanner {
    string_mode: StringMode,
    string_deli: char,
    just_broke: bool,
    just_spaced: bool,
    still_broke: bool,
    pub last_token: Token,
    last_leaf: Token,
    pub reader: Box<dyn SourceReader>
}

pub type Token = ( NodeTy, Pos, Leaf );

const KEYWORDS: [&str;23] = [ "break",  "count",  "downto",  "else",  "elsif",
    "exit", "false", "for", "from", "fx", "if", "is", "log", "next", "null",
    "of", "return", "to", "true", "unless", "until", "var", "while" ];

const KEYWORDS_TY: [NodeTy;23] = [ NodeTy::KwBreak, NodeTy::KwCount,
    NodeTy::KwDownto, NodeTy::KwElse, NodeTy::KwElsif, NodeTy::KwExit,
    NodeTy::KwFalse, NodeTy::KwFor, NodeTy::KwFrom, NodeTy::KwFx, NodeTy::KwIf,
    NodeTy::KwIs, NodeTy::KwLog, NodeTy::KwNext, NodeTy::KwNull, NodeTy::KwOf,
    NodeTy::KwReturn, NodeTy::KwTo, NodeTy::KwTrue, NodeTy::KwUnless,
    NodeTy::KwUntil, NodeTy::KwVar, NodeTy::KwWhile
];

fn is_word_material(c: char) -> bool{
    return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') || c == '-' || c == '_';
}

fn is_word_starter(c: char) -> bool{
    return  (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_';
}

fn is_num(c: char) -> bool{
    return c >= '0' && c <= '9';
}

pub const BOF: Token = (
    NodeTy::EOF,
    Pos { end_col: -1, end_line: 0, start_col: -1, start_line: 0 },
    Leaf {
        break_before: false,
        space_before: false,
        space_after: false,
        value: String::new()
    }
);

impl Scanner {

    pub fn new(reader: Box<dyn SourceReader>) -> Self{

        let p = Scanner {
            string_mode: StringMode::None,
            string_deli: '\'',
            just_broke: false,
            just_spaced: true,
            still_broke: true,
            last_token: BOF,
            last_leaf: BOF,
            reader
        };

        return p;
    }

    fn create_leaf(&mut self, ty: NodeTy, value: String) -> Token {

        let llp = self.last_leaf.1;
        let v_len = value.len();

        let mut t = (
            ty,
            Pos {
                start_col: llp.end_col + 1,
                start_line: llp.end_line,
                end_line: llp.end_line,
                end_col: llp.end_col + value.len() as isize
            },
            Leaf {
                value,
                // prev: self.last_token,
                break_before: self.still_broke,
                space_after: "\r\n \t".contains(self.reader.lookahead(1)),
                space_before: self.just_spaced
            }
        );

        if self.just_broke {
            t.1.start_line += 1;
            t.1.end_line += 1;
            t.1.start_col = 0;
            t.1.end_col = v_len as isize - 1;
        }

        self.just_broke = false;
        self.just_spaced = false;
        self.still_broke = false;

        if t.0 != NodeTy::LineBreak && t.0 != NodeTy::Space {
            self.last_token = view_token(&t);
        }

        self.last_leaf = view_token(&t);
        return t;
    }

    fn scan_line_break(&mut self) -> Token{
        let mut value = String::new();

        let mut t = self.create_leaf(NodeTy::LineBreak, String::new());

        let mut c = self.reader.advance();
        let mut last_len = 0;
        let mut break_count = 0;

        while c == '\n' || c == '\r' {

            if c == '\r' && self.reader.lookahead(1) != '\n' || c == '\n' {
                break_count += 1;
                last_len = 0;
            }

            value.push(c);
            c = self.reader.advance();
            last_len += 1;
        }

        self.reader.backtrack();

        self.just_broke = true;
        self.still_broke = true;

        t.1.end_col += last_len;
        t.1.end_line += break_count;

        return t;
    }

    fn scan_word(&mut self) -> Token{

        let mut value = String::new();

        let mut c = self.reader.advance();
        while is_word_material(c) {
            value.push(c);
            c = self.reader.advance();
        }

        self.reader.backtrack();

        let kw_index = match KEYWORDS.binary_search(&value.as_str()) {
            Ok(index) => index + 1,
            Err(_)    => 0
        };

        let mut ty = NodeTy::Word;
        if kw_index > 0 {
            ty = KEYWORDS_TY[kw_index - 1];
        }

        return self.create_leaf(ty, value);
    }

    fn scan_scape_seq(&mut self) -> Token {
        // TODO add special multi character escapes (u x ...)
        let mut s = String::new();
        s.push(self.reader.advance());
        s.push(self.reader.advance());
        return self.create_leaf(NodeTy::Scape, s);
    }

    fn scan_interpolation(&mut self) -> Token{

        // Consumes $
        self.reader.advance();
        let c2 = self.reader.lookahead(1);

        if c2 == '{' {
            // Consumes {
            self.reader.advance();
            self.string_mode = StringMode::Inter;
            return self.create_leaf(NodeTy::Concat, "${".to_owned());
        }

        self.string_mode = StringMode::Word;
        return self.create_leaf(NodeTy::Concat, "$".to_owned());
    }

    fn scan_string(&mut self) -> Token{

        // Open quote    OR    first char when composing
        let mut c = self.reader.advance();
        let mut value = String::new();
        let delimit;

        let mut breaks = 0;
        let mut last_line_size = 0;

        if self.string_mode != StringMode::None {
            delimit = self.string_deli;
        }
        else {
            value.push(c);
            delimit = c;
            self.string_deli = c;
            last_line_size += 1;
            c = self.reader.advance();
        }

        while c != delimit {

            if c == '\x00' {
                return self.create_leaf(NodeTy::BadString, value);
            }

            let c2 = self.reader.lookahead(1);

            if c == '\\' ||
            (delimit == '"' && c == '$' && (is_word_starter(c2) || c2 == '{')) {
                self.string_mode = StringMode::String;
                self.reader.backtrack();

                let mut st = self.create_leaf(NodeTy::String, value);
                st.1.end_line += breaks;
                if breaks > 0 {
                    st.1.end_col = last_line_size;
                }
                return st;
            }

            last_line_size += 1;

            if c == '\r' && c2 != '\n' || c == '\n' {
                last_line_size = 0;
                breaks += 1;
            }

            value.push(c);
            c = self.reader.advance();
        }

        self.string_mode = StringMode::None;
        value.push(c);

        let mut st = self.create_leaf(NodeTy::String, value);
        st.1.end_line += breaks;
        if breaks > 0 {
            st.1.end_col = last_line_size;
        }

        self.last_leaf = view_token(&st);
        return st;
    }

    fn scan_comment(&mut self) -> Token{

        let mut value = String::new();

        value.push(self.reader.advance());
        value.push(self.reader.advance());

        if value == "//" {

            let mut c = self.reader.advance();
            while c != '\r' && c != '\n' {
                value.push(c);
                c = self.reader.advance();
            }

            self.reader.backtrack();

            return self.create_leaf(NodeTy::Comment, value);
        }

        // else Multi-line

        let mut breaks = 0;
        let mut last_line_size = 0;
        let mut a1 = self.reader.lookahead(1);
        let mut a2 = self.reader.lookahead(2);

        while a1 != '*' && a2 != '/' {
            let c = self.reader.advance();

            if c == '\r' && a2 != '\n' || c == '\n' {
                last_line_size = 0;
                breaks += 1;
            }

            if c == '\x00' {
                return self.create_leaf(NodeTy::BadComment, value);
            }

            last_line_size += 1;
            value.push(c);

            a1 = a2;
            a2 = self.reader.lookahead(2);
        }

        value.push(self.reader.advance());
        value.push(self.reader.advance());
        let mut ct = self.create_leaf(NodeTy::Comment, value);

        ct.1.end_line += breaks;
        if breaks > 0 {
            ct.1.end_col = last_line_size;
        }

        self.last_leaf = view_token(&ct);
        return ct;
    }

    fn scan_regexp(&mut self) -> Token {

        let mut value = String::new();
        value.push(self.reader.advance());

        let mut c = self.reader.advance();

        while c != '/' {
            if c == '\\' {
                value.push(c);
                c = self.reader.advance();
            }
            value.push(c);
            c = self.reader.advance();
        }

        value.push(c);

        c = self.reader.advance();
        if "dgimsuy".contains(c) {
            while "dgimsuy".contains(c) {
                value.push(c);
                c = self.reader.advance();
            }
        }

        self.reader.backtrack();

        return self.create_leaf(NodeTy::RegExp, value);
    }

    fn scan_numeric_seq(&mut self) -> String{

        let mut value = String::new();
        let mut c = self.reader.advance();
        while is_num(c) {
            value.push(c);
            c = self.reader.advance();
        }

        self.reader.backtrack();
        return value;
    }

    fn scan_symbol(&mut self) -> Token {
        let c1 = self.reader.lookahead(1);
        let c2 = self.reader.lookahead(2);

        if [c1, c2] == ['/', '*' ] || [c1, c2] == ['/', '/' ] {
            return self.scan_comment();
        }

        if c1 == '.' && is_num(c2) {
            let mut value = String::new();
            value.push(self.reader.advance());
            value.extend(self.scan_numeric_seq().chars());
            return self.create_leaf(NodeTy::Number, value);
        }

        let lt = self.last_token.0;
        if c1 == '/' && (
            /* bin ops */lt >= NodeTy::OpEq && lt <= NodeTy::SymMaj ||
            REGEXP_PREV.contains(&lt)
        ) {
            return self.scan_regexp();
        }

        let mut value = String::new();
        value.push(self.reader.advance());

        let mut ty = match [ c1, c2 ] {
            [ '=', '=' ] => NodeTy::OpEq,
            [ '!', '=' ] => NodeTy::OpDiff,
            [ '>', '=' ] => NodeTy::OpMajEq,
            [ '<', '=' ] => NodeTy::OpMinEq,
            [ '?', '?' ] => NodeTy::OpCoal,
            [ '+', '=' ] => NodeTy::OpAdd,
            [ '-', '=' ] => NodeTy::OpSub,
            [ '=', '>' ] => NodeTy::OpArrow,
            _ => NodeTy::EOF
        };

        if ty == NodeTy::EOF {
            ty = match c1 {
                '!' => NodeTy::SymExMark,
                '*' => NodeTy::SymMul,
                '/' => NodeTy::SymDiv,
                '+' => NodeTy::SymPlus,
                '-' => NodeTy::SymMinus,
                '=' => NodeTy::SymAssign,
                ')' => NodeTy::SymParCl,
                '(' => NodeTy::SymParOp,
                '}' => NodeTy::SymCurCl,
                '{' => NodeTy::SymCurOp,
                ']' => NodeTy::SymSqCl,
                '[' => NodeTy::SymSqOp,
                ',' => NodeTy::SymComma,
                '.' => NodeTy::SymDot,
                '<' => NodeTy::SymMin,
                '>' => NodeTy::SymMaj,
                '?' => NodeTy::SymQMark,
                ':' => NodeTy::SymColon,
                '|' => NodeTy::SymPipe,
                '&' => NodeTy::SymAmp,
                _ => NodeTy::EOF
            }
        }
        else {
            value.push(self.reader.advance());
        }

        return self.create_leaf(ty, value);
    }

    fn scan_number(&mut self) -> Token {
        let mut string = self.scan_numeric_seq();

        let c1 = self.reader.lookahead(1);
        let c2 = self.reader.lookahead(2);
        if c1 == '.' && is_num(c2) {
            string.push(self.reader.advance());
            string.extend(self.scan_numeric_seq().chars());
        }

        return self.create_leaf(NodeTy::Number, string);
    }

    fn scan_space(&mut self) -> Token{
        let mut value = String::new();
        let mut c = self.reader.advance();

        while c == ' ' || c == '\t' {
            value.push(c);
            c = self.reader.advance();
        }
        self.reader.backtrack();
        let bb = self.just_broke;
        let t = self.create_leaf(NodeTy::Space, value);
        self.just_spaced = true;
        self.still_broke = bb;
        return t;
    }

    pub fn scan_unknown(&mut self) -> Token {
        let ch = self.reader.lookahead(1);

        if ch == '\x00' {
            return self.create_leaf(NodeTy::EOF, String::new());
        }

        if self.string_mode == StringMode::String {
            if ch == '\\' {
                return self.scan_scape_seq();
            }

            if self.string_deli == '"' && ch == '$' {
                return self.scan_interpolation();
            }

            // Middle and closing string
            return self.scan_string();
        }

        if self.string_mode == StringMode::Inter && ch == '}' {
            self.string_mode = StringMode::String;
            self.reader.advance();
            return self.create_leaf(NodeTy::Concat, "}".to_owned());
        }

        if self.string_mode == StringMode::Word {
            self.string_mode = StringMode::DoneWord;
            return self.scan_word();
        }

        if self.string_mode == StringMode::DoneWord {
            self.string_mode = StringMode::String;
            return self.create_leaf(NodeTy::Concat, String::new());
        }

        // number
        if "0123456789".contains(ch) {
            return self.scan_number();
        }

        // word (letter or undercore)
        if is_word_starter(ch) {
            return self.scan_word();
        }

        // string (quotes)
        if ch == '\'' || ch == '"' {
            return self.scan_string();
        }

        // symbols
        if "!*/+-=)(}{][,.<>?:|&".contains(ch) {
            return self.scan_symbol();
        }

        // new line
        if ch == '\n' || ch == '\r' {
            return self.scan_line_break();
        }

        if ch == ' ' || ch == '\t' {
            return self.scan_space();
        }

        let v = self.reader.advance().to_string();
        return self.create_leaf(NodeTy::Invalid, v);
    }


}

#[cfg(test)]
mod lexer {
    use crate::lex::*;
    use crate::reader::StringSourceReader;

    fn s(st: &str) -> Vec<Token>{
        let mut tokens = vec![];
        let r = StringSourceReader::new(st);
        let mut p = Scanner::new(Box::new(r));

        let mut t = p.scan_unknown();

        while t.0 != NodeTy::EOF {

            println!("({}) => {}:{}-{}:{} '{}'", tokens.len(),
                t.1.start_line, t.1.start_col, t.1.end_line,
                t.1.end_col, t.2.value);

            tokens.push(t);
            t = p.scan_unknown();
        }

        return tokens;
    }

    #[test]
    fn simple_tokens() {
        let r = s("this-Is9_Word + null \"test\"  \n \'string\' 23 4.66 .23 is \"sdsd");
        assert_eq!(r[0].0, NodeTy::Word);
        assert_eq!(r[0].1.start_col, 0);
        assert_eq!(r[1].0, NodeTy::Space);
        assert_eq!(r[2].0, NodeTy::SymPlus);
        assert_eq!(r[2].1.start_col, 14);
        assert_eq!(r[4].0, NodeTy::KwNull);
        assert_eq!(r[6].0, NodeTy::String);
        assert_eq!(r[6].1.end_col, 26);
        assert_eq!(r[10].0, NodeTy::String);
        assert!(r[10].2.break_before);
        assert_eq!(r[12].0, NodeTy::Number);
        assert_eq!(r[14].0, NodeTy::Number);
        assert_eq!(r[16].0, NodeTy::Number);
        assert_eq!(r[20].0, NodeTy::BadString);
    }

    #[test]
    fn composed_strings() {
        let r = s(" 'abc \\r def \\n ghi' \"abc $def ghi\" \"abc ${ d + f } ghi\" ");
        assert_eq!(r[1].0, NodeTy::String);
        assert_eq!(r[2].0, NodeTy::Scape);
        assert_eq!(r[3].0, NodeTy::String);
        assert_eq!(r[4].0, NodeTy::Scape);
        assert_eq!(r[5].0, NodeTy::String);

        assert_eq!(r[7].0, NodeTy::String);
        assert_eq!(r[8].0, NodeTy::Concat);
        assert_eq!(r[9].0, NodeTy::Word);
        assert_eq!(r[10].0, NodeTy::Concat);
        assert_eq!(r[11].0, NodeTy::String);

        assert_eq!(r[13].0, NodeTy::String);
        assert_eq!(r[14].0, NodeTy::Concat);
        assert_eq!(r[16].0, NodeTy::Word);
        assert_eq!(r[18].0, NodeTy::SymPlus);
        assert_eq!(r[20].0, NodeTy::Word);
        assert_eq!(r[22].0, NodeTy::Concat);
        assert_eq!(r[23].0, NodeTy::String);
    }

    #[test]
    fn comments() {
        let r = s(" // foobar \r\n /* single */ \r /* Multi \n line */ /* bad one ");
        assert_eq!(r[1].0, NodeTy::Comment);

        assert_eq!(r[4].0, NodeTy::Comment);
        assert_eq!(r[4].1.end_col, 12);
        assert_eq!(r[4].1.end_line, 1);

        assert_eq!(r[8].0, NodeTy::Comment);
        assert_eq!(r[8].1.end_line, 3);
        assert_eq!(r[8].1.end_col, 7);

        assert_eq!(r[10].0, NodeTy::BadComment);
        assert_eq!(r[10].1.start_col, 9);
    }

    #[test]
    fn multi_line_strings() {
        let r = s(" 'abc\ndef' \"abc ${ \r\nd + f } ghi\" ");

        assert_eq!(r[1].0, NodeTy::String);
        assert_eq!(r[1].1.end_col, 3);
        assert_eq!(r[1].1.end_line, 1);

        assert_eq!(r[3].0, NodeTy::String);

        assert_eq!(r[7].0, NodeTy::Word);
        assert_eq!(r[7].1.end_col, 0);
        assert_eq!(r[7].1.end_line, 2);

        assert_eq!(r[15].1.end_line, 2);
    }

}