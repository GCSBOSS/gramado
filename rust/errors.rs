
use crate::lex::*;

    /*

    Error message UX
        > probably belongs to another phase of the pipeline
        > ie.: we should output JS objects for now since this
        > code will be used inside other code rather than face a
        > human directly

    ---

    Hi! I could not parse some of the code.

    --- my-file/path.gr ----------------------------

    In this line state what the problem found was (computer talking here)

    23|      This is = 'a raw line of code';
                ^^^^

    - This is 1 solution or insight that might help the user

    - This is the next solution

        > We can have nested solutions for ease

    */


#[derive(Copy, Clone, Debug, PartialEq)]
pub enum ErrTy {
    UnfinishedString = 1000,
    MissingEncloser = 1001,
    MissingKeyClosingBracket = 1002,
    MissingArrayClosing = 1003,
    UnexpectedObjectKey = 1004,
    MissingObjectClosing = 1005,
    UnknownIsParameter = 1006,
    MissingIndexClosing = 1007,
    UnexpectedSpaceAfterDot = 1008,
    UnexpectedTokenAfterDot = 1009,
    MissingVarWord = 1010,
    Unexpected = 1011,
    InvalidOperand = 1012,
    MissingCallSeparator = 1013,
    MissingBlockOpen = 1014,
    MissingInterpolationClose = 1015,
    UnexpectedParam = 1016,
    MissingForSpecifier = 1017,
    MissingOfKeyword = 1018,
    MissingForCounter = 1019,
    MissingTo = 1020,
    MissingLogArguments = 1021,
    ReturnNotAllowed = 1022,
    BreakNotAllowed = 1023,
    NextNotAllowed = 1024
}


#[derive(Debug)]
pub struct Err {
    pub ty: ErrTy,
    pub token: Option<Token>,
    // prev
}

impl Err {

    pub fn format(&self) -> String{

        let mut o = String::new();
        // TODO In this line state what the problem found was (computer talking here)
        o.push_str("=>");
        o.push_str((self.ty as u64).to_string().as_str());
        o.push_str("<=\n\n");

        if let Some((_tt, pos, _l)) = &self.token {
            let sline = (pos.start_line + 1).to_string();
            let sl = sline.as_str();
            o.push_str(sl);
            o.push_str("| ");
            // o.push_str(self.lexer.reader.lines().get(pos.start_line as usize).unwrap());
            o.push_str("\n ");
            o.push_str(format!("{: >wid$}", wid = sl.len()).as_str());
            o.push_str(format!("{: >wid$}", wid = pos.start_col as usize).as_str());
            o.push_str("\u{001b}[38;5;203m");
            o.push_str(format!("{:^>wid$}", wid = (pos.end_col - pos.start_col) as usize).as_str());
            o.push_str("\u{001b}[0m\n\n");

            //     - This is 1 solution or insight that might help the user
            //     - This is the next solution
            //         > We can have nested solutions for ease
        }
        else {
            panic!();
        }

        return o;
    }

}

pub fn format_err_list(list: Vec<Err>) -> String{

    let mut o = String::new();
    o.push_str("\n\u{001b}[38;5;153mHi! I could not parse some of the code.\u{001b}[0m\n\n");

    for err in list {
        o.push_str("------------------------------------------------\n\n");
        o.push_str(err.format().as_str());
    }

    return o;
}