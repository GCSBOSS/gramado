
use crate::lex::*;
use crate::syn::*;

pub struct Scripter {
    need_is_null: bool,
    need_is_empty: bool,
    need_halt: bool,
    // block_stack: Vec<Vec<String>>,
    function_context: Vec<bool>
}

impl Scripter {

    fn get_value(&mut self, n: &ChildNode)-> String {
        match n.ty {
            NodeTy::KwExit => {
                if !self.function_context.last().unwrap() {
                    self.need_halt = true;
                }
                "return".to_owned()
            },
            NodeTy::KwLog => "console.log".to_owned(),
            _ => n.leaf().value
        }
    }

    fn oper(&mut self, t: &ChildNode)-> String{

        if let NodeData::Binary { left, right, op } = &t.data {
            let os = match op.ty {
                NodeTy::SymPipe => "||".to_owned(),
                NodeTy::SymAmp => "&&".to_owned(),
                _ => self.get_value(&op)
            };

            let mut s = String::new();

            if os == "." && left.ty == NodeTy::Number {
                s.push('(');
            }

            let lty = left.ty;
            s.push_str(&*self.expand(&left));

            if os == "." && lty == NodeTy::Number {
                s.push(')');
            }

            s.push_str(os.as_str());
            s.push_str(&*self.expand(&right));
            return s;
        }
        else {
            panic!();
        }
    }

    fn unary(&mut self, t: &ChildNode)-> String{

        let mut s = String::new();
        if let NodeData::Unary { subject } = &t.data {
            s.push_str(match t.ty {
                NodeTy::NOT => "!",
                NodeTy::MINUS => "-",
                NodeTy::RETURN => "return",
                _ => panic!()
            });
            s.push('(');
            s.push_str(&*self.expand(&subject));
            s.push(')');
            return s;
        }
        else {
            panic!();
        }
    }

    fn list(&mut self, t: &ChildNode, o: &str, c: &str)-> String{
        if let NodeData::List { items } = &t.data {
            let mut s = String::new();
            s.push_str(o);

            if items.len() > 0 {
                s.push_str(&*self.expand(&items[0]));

                for i in &items.as_slice()[1..] {
                    s.push(',');
                    s.push_str(&*self.expand(&i));
                }
            }

            s.push_str(c);
            return s;
        }
        else {
            panic!();
        }
    }

    fn index(&mut self, t: &ChildNode)-> String{
        if let NodeData::Binary { left, right, .. } = &t.data {
            let mut s = String::new();
            s.push_str(&*self.expand(&left));
            s.push('[');
            s.push_str(&*self.expand(&right));
            s.push(']');
            return s;
        }
        else {
            panic!();
        }
    }

    fn call(&mut self, t: &ChildNode)-> String{

        if let NodeData::Call { items, subject } = &t.data {
            let mut s = String::new();
            s.push_str(&*self.expand(&subject));
            s.push('(');
            if items.len() > 0 {
                s.push_str(&*self.expand(&items[0]));
                for i in &items.as_slice()[1..] {
                    s.push(',');
                    s.push_str(&*self.expand(&i));
                }
            }
            s.push(')');
            return s;
        }
        else {
            panic!();
        }
    }

    fn var(&mut self, t: &ChildNode)-> String{
        let mut s = String::new();
        s.push_str("var ");

        if let NodeData::List { items } = &t.data {
            s.push_str(&*self.expand(&items[0]));
            for i in &items.as_slice()[1..] {
                s.push(',');
                s.push_str(&*self.expand(&i));
            }
            return s;
        }
        else {
            panic!();
        }
    }

    fn is(&mut self, t: &ChildNode)-> String{

        if let NodeData::Binary { left, right, .. } = &t.data {
            let v = right.leaf().value;
            let lstr = &*self.expand(&left);
            let mut s = String::new();

            if v == "null" {
                self.need_is_null = true;
                s.push_str("$$isNull(");
                s.push_str(lstr);
                s.push(')');
            }

            else if v == "empty" {
                self.need_is_empty = true;
                s.push_str("$$isEmpty(");
                s.push_str(lstr);
                s.push(')');
            }

            else if v == "array" {
                s.push_str("Array.isArray(");
                s.push_str(lstr);
                s.push(')');
            }

        //     // if(token.right!.value == 'date')
        //         // return '(' + left + ' instanceof Date)';

            else if v == "string" {
                s.push_str("(typeof ");
                s.push_str(lstr);
                s.push_str(" == \"string\")");
            }

            else if v == "fx" {
                s.push_str("(typeof ");
                s.push_str(lstr);
                s.push_str(" == \"function\")");
            }

            else if v == "boolean" {
                s.push_str("(typeof ");
                s.push_str(lstr);
                s.push_str(" == \"boolean\")");
            }

            else if v == "number" {
                s.push_str("(typeof ");
                s.push_str(lstr);
                s.push_str(" == \"number\")");
            }

            else {
                panic!()
            }

            return s;
        }
        else {
            panic!();
        }
    }

    fn string(&mut self, t: &ChildNode)-> String{
        let mut s = String::new();

        if let NodeData::List { items } = &t.data {

            if let [a, middle @ .., b] = items.as_slice() {
                s.push('`');
                s.push_str(&a.leaf().value[1..]);
                s.push('`');

                for i in middle {
                    s.push('+');
                    if i.ty == NodeTy::String || i.ty == NodeTy::Scape {
                        s.push('`');
                        s.push_str(&*i.leaf().value);
                        s.push('`');
                    }
                    else {
                        s.push('(');
                        s.push_str(&*self.expand(i));
                        s.push(')');
                    }
                }

                s.push('+');
                let last = b.leaf().value;
                s.push('`');
                s.push_str(&last[0..last.len() - 1]);
                s.push('`');
            }

            return s;
        }
        else {
            panic!();
        }
    }

    fn whiley(&mut self, t: &ChildNode)-> String{
        let mut s = String::new();
        s.push_str(" while(");

        if let NodeData::CondBlock { cond, block } = &t.data {
            s.push_str(&*self.expand(&cond));
            s.push_str("){");
            s.push_str(&*self.statements(block));
            s.push_str("}");
            return s;
        }
        else {
            panic!();
        }
    }

    fn until(&mut self, t: &ChildNode)-> String{
        let mut s = String::new();
        s.push_str(" while(!(");

        if let NodeData::CondBlock { cond, block } = &t.data {
            s.push_str(&*self.expand(&cond));
            s.push_str(")){");
            s.push_str(&*self.statements(block));
            s.push_str("}");
            return s;
        }
        else {
            panic!();
        }
    }

    fn unless(&mut self, t: &ChildNode)-> String{
        let mut s = String::new();
        s.push_str(" if(!(");

        if let NodeData::CondBlock { cond, block } = &t.data {
            s.push_str(&*self.expand(&cond));
            s.push_str(")){");
            s.push_str(&*self.statements(block));
            s.push_str("}");
            return s;
        }
        else {
            panic!();
        }
    }

    fn elsif(&mut self, t: &ChildNode)-> String{
        let mut s = String::new();
        s.push_str(" else if(");

        if let NodeData::CondBlock { cond, block } = &t.data {
            s.push_str(&*self.expand(&cond));
            s.push_str("){");
            s.push_str(&*self.statements(block));
            s.push_str("}");
            return s;
        }
        else {
            panic!();
        }
    }

    fn ify(&mut self, t: &ChildNode)-> String{
        let mut s = String::new();

        if let NodeData::If { cond, block, elsifs, else_block } = &t.data {
            s.push_str(" if(");
            s.push_str(&*self.expand(&cond));
            s.push_str("){");
            s.push_str(&*self.statements(block));
            s.push_str("}");

            for ei in elsifs {
                s.push_str(&*self.expand(&ei));
            }

            if let Some(b) = else_block {
                s.push_str(" else {");
                s.push_str(&*self.statements(b));
                s.push_str("}");
            }

            return s;
        }
        else {
            panic!();
        }
    }

    fn fx(&mut self, t: &ChildNode)-> String{
        let mut s = String::new();

        if let NodeData::Fx { name, block, items } = &t.data {
            s.push_str(" function");

            if let Some(n) = name {
                s.push(' ');
                s.push_str(&*self.expand(&n));
            }
            s.push_str("(");

            for i in items {
                s.push_str(&*self.expand(&i));
                s.push(',');
            }

            s.push_str("){");
            self.function_context.push(true);
            s.push_str(&*self.statements(block));
            self.function_context.pop();
            s.push_str("}");

            return s;
        }
        else {
            panic!();
        }
    }

    fn afx(&mut self, t: &ChildNode)-> String{
        let mut s = String::new();

        if let NodeData::AFx { expr, block, items } = &t.data {

            if items.ty != NodeTy::LIST {
                s.push('(');
            }

            s.push_str(&*self.expand(&items));

            if items.ty != NodeTy::LIST {
                s.push(')');
            }

            s.push_str("=>");

            if *expr {
                s.push('(');
                s.push_str(&*self.expand(&block.first().unwrap()));
                s.push(')');
            }
            else {
                s.push_str("{");
                self.function_context.push(true);
                s.push_str(&*self.statements(block));
                self.function_context.pop();
                s.push_str("}");
            }

            return s;
        }
        else {
            panic!();
        }
    }

    fn pair(&mut self, t: &ChildNode)-> String{
        let mut s = String::new();

        if let NodeData::Pair { key, val } = &t.data {

            let bracket = !key.is(&[ NodeTy::Word, NodeTy::Number, NodeTy::String ]);

            if bracket {
                s.push('[');
            }

            s.push_str(&*self.expand(&key));

            if bracket {
                s.push(']');
            }

            s.push(':');

            s.push_str(&match val {
                Some(v) => self.expand(&v),
                None => self.expand(&key)
            });

            return s;
        }
        else {
            panic!();
        }
    }

    fn fory(&mut self, t: &ChildNode)-> String{
        let mut s = String::new();

        if let NodeData::For { count, key, val, subject, block } = &t.data {

            s.push_str("const $$subject = ");
            s.push_str(&self.expand(&subject));
            s.push_str("; ");

            if let Some(c) = count {
                s.push_str("let ");
                s.push_str(&self.expand(&c));
                s.push_str(" = -1; ");
            }

            s.push_str(" for(");

            if let Some(k) = key {
                s.push_str("let ");
                s.push_str(&self.expand(&k));
            } else {
                s.push_str("const $$fakeKey");
            }

            s.push_str(" in $$subject){");

            if let Some(v) = val {
                s.push_str("let ");
                s.push_str(&self.expand(&v));
                s.push_str(" = $$subject[");

                if let Some(k) = key {
                    s.push_str(&self.expand(&k));
                } else {
                    s.push_str("$$fakeKey");
                }

                s.push_str("]; ");
            }

            if let Some(c) = count {
                s.push_str(&self.expand(&c));
                s.push_str("++; ");
            }

            s.push_str(&*self.statements(block));
            s.push_str("}");
            return s;
        }
        else {
            panic!();
        }
    }

    fn from(&mut self, t: &ChildNode)-> String{
        let mut s = String::new();

        if let NodeData::From { op, start, end, count, block } = &t.data {

            let cn = match count {
                Some(c) => self.expand(&c),
                None => "$$count".to_owned()
            };

            let e = &*self.expand(&end);

            s.push_str(" for(let ");
            s.push_str(&*cn);
            s.push_str(" = ");
            s.push_str(&*self.expand(&start));
            s.push_str("; ");
            s.push_str(&*cn);

            match op.ty {
                NodeTy::KwTo => {
                    s.push_str(" <= ");
                    s.push_str(e);
                    s.push_str("; ");
                    s.push_str(&*cn);
                    s.push_str("++");
                }
                NodeTy::KwDownto => {
                    s.push_str(" >= ");
                    s.push_str(e);
                    s.push_str("; ");
                    s.push_str(&*cn);
                    s.push_str("--");
                }
                _ => panic!()
            }

            s.push_str("){");
            s.push_str(&*self.statements(block));
            s.push_str("}");
            return s;
        }
        else {
            panic!();
        }
    }

    fn cond(&mut self, t: &ChildNode)-> String{
        let mut s = String::new();
        s.push('(');

        if let NodeData::Ternary { cond, right, left } = &t.data {
            s.push_str(&*self.expand(&cond));
            s.push('?');
            s.push_str(&*self.expand(&left));
            s.push(':');
            if let Some(r) = right {
                s.push_str(&*self.expand(&r));
            }
            else {
                s.push_str("null");
            }
        }
        else {
            panic!();
        }

        s.push(')');
        return s;
    }

    fn expand(&mut self, n: &ChildNode) -> String{

        return match n.ty {
            NodeTy::OPER => self.oper(n),
            NodeTy::ASSIGN => self.oper(n),
            NodeTy::IF => self.ify(n),
            NodeTy::FOR => self.fory(n),
            NodeTy::FROM => self.from(n),
            NodeTy::PAIR => self.pair(n),
            NodeTy::ELSIF => self.elsif(n),
            NodeTy::UNLESS => self.unless(n),
            NodeTy::WHILE => self.whiley(n),
            NodeTy::UNTIL => self.until(n),
            NodeTy::COND => self.cond(n),
            NodeTy::LIST => self.list(n, "(", ")"),
            NodeTy::CALL => self.call(n),
            NodeTy::SCALL => self.call(n),
            NodeTy::ARRAY => self.list(n, "[", "]"),
            NodeTy::OBJECT => self.list(n, "({", "})"),
            NodeTy::IS => self.is(n),
            NodeTy::ADD => self.oper(n),
            NodeTy::SUB => self.oper(n),
            NodeTy::NOT => self.unary(n),
            NodeTy::MINUS => self.unary(n),
            NodeTy::PROP => self.oper(n),
            NodeTy::INDEX => self.index(n),
            NodeTy::VAR => self.var(n),
            NodeTy::AFX => self.afx(n),
            NodeTy::RETURN => self.unary(n),
            NodeTy::LOG => self.call(n),
            NodeTy::FX => self.fx(n),
            NodeTy::STRING => self.string(n),
            _ => self.get_value(n)
        }
    }

    fn statements(&mut self, nodes: &ChildList)-> String{
        let mut list = vec![];
        // self.block_stack.push(&list);

        for t in nodes {
            if t.ty == NodeTy::EOF {
                break;
            }
            list.push(self.expand(&t));
        }

        // self.block_stack.pop();
        return list.join(";");
    }

    pub fn transpile(ast: ChildList) -> String {
        let mut t = Scripter {
            // block_stack: vec![],
            need_is_empty: false,
            need_is_null: false,
            need_halt: false,
            function_context: vec![false]
        };

        let mut script = String::new();

        let main = t.statements(&ast);

        if t.need_is_empty {
            script.push_str("const $$isEmpty = v => v === null");
            script.push_str(" || typeof v == 'undefined' || v === ''");
            script.push_str(" || (Array.isArray(v) && v.length == 0);");
        }

        if t.need_is_null {
            script.push_str("const $$isNull = v => v === null || typeof v == 'undefined';");
        }

        if t.need_halt {
            script.push_str("(function(){");
        }

        script.push_str(main.as_str());

        if t.need_halt {
            script.push_str("})()");
        }

        return script;
    }
}
/*
#[cfg(test)]
mod scripter {
    use super::*;
    use crate::reader::StringSourceReader;

    use ntest::*;

    fn p(st: &str) -> String {

        let r = StringSourceReader::new(st);
        let r = parse(Box::new(r));

        assert_eq!(r.errors.len(), 0);

        return Scripter::transpile(r.ast);
    }

    #[test]
    #[timeout(500)]
    fn string_scapes() {
        let r = p("\n\nlog \"abc\\ndef\"");
        assert_eq!(r, "console.lof(`abc`+`\\n`+`def`)");
    }

}
*/
/*

Deno.test('[scripter] string interpolation', async () => {

    let r = await ev('var a = "foo \$\{ 3 + 4 } bar"\n a');
    assert(r, 'foo 7 bar');

    r = await ev('var b = 5 \n "abc $b foobar $- "');
    assertEquals(r, 'abc 5 foobar $- ');
});

Deno.test('[scripter] specific order binary operations', async () => {
    const r = await ev('var b = 3 * (2 + 4) \n b');
    assertEquals(r, 18);
});

Deno.test('[scripter] not operator', async () => {
    const r = await ev('!0');
    assertEquals(r, true);
});

Deno.test('[scripter] unary minus operator', async () => {
    const r = await ev('-23.3');
    assertEquals(r, -23.3);
});

Deno.test('[scripter] property access', async () => {
    const r = await ev('(3 + 5).toString()');
    assertEquals(r, '8');
});

Deno.test('[scripter] index access', async () => {
    const r = await ev('"abc".split("")[1]');
    assertEquals(r, 'b');
});

Deno.test('[scripter] array literal', async () => {
    const r = await ev('[ 2, "c" ]');
    assertEquals(r[0], 2);
    assertEquals(r[1], 'c');
});

Deno.test('[scripter] object literal', async () => {
    const r = await ev('{ a: 3, "r": "abc", [1 + 2]: true }');
    assertEquals(r.a, 3);
    assertEquals(r.r, 'abc');
    assertEquals(r[3], true);
});

Deno.test('[scripter] add assignment', async () => {
    const r = await ev('var a = 0\n a += 2\na');
    assertEquals(r, 2);
});

Deno.test('[scripter] subtract assignment', async () => {
    const r = await ev('var a = 0\n a -= 2\na');
    assertEquals(r, -2);
});

Deno.test('[scripter] space oriented function call', async () => {
    const r = await ev('"a=b".split "="');
    assertEquals(r[0], 'a');
    assertEquals(r[1], 'b');
});

Deno.test('[scripter] function definition', async () => {
    const r = await ev('fx foobar(a) {\n return 24 } foobar() ');
    assertEquals(r, 24);
});

Deno.test('[scripter] if statement', async () => {
    const r = await ev('if true \n "cool"');
    assertEquals(r, 'cool');
});

Deno.test('[scripter] if elsif statement', async () => {
    const r = await ev('var a = 3 \n if a == 2 \n 1 \n elsif a == 3 \n ("cool")');
    assertEquals(r, 'cool');
});

Deno.test('[scripter] if else statement', async () => {
    const r = await ev('var a = 3 \n if a == 2 \n 1 \n else \n ("cool")');
    assertEquals(r, 'cool');
});

Deno.test('[scripter] while statement', async () => {
    const r = await ev('var a = 0\n a += 1 while a < 3 \n (a)');
    assertEquals(r, 3);
});

Deno.test('[scripter] until statement', async () => {
    const r = await ev('var a = 0\n a += 1 until a == 2 \n (a)');
    assertEquals(r, 2);
});

Deno.test('[scripter] unless statement', async () => {
    const r = await ev('unless false \n (2)');
    assertEquals(r, 2);
});

Deno.test('[scripter] for statement', async () => {

    let r = await ev('for k: of [ 5, 4, 3 ] \n k');
    assertEquals(r, '2');

    r = await ev('for v of [ 5, 4, 3 ] \n v');
    assertEquals(r, 3);

    r = await ev('for k: v of { a: 5, b: 6 } count i \n i');
    assertEquals(r, 1);
});

Deno.test('[scripter] from statement', async () => {

    let r = await ev('from 0 to 2 \n true');
    assert(r);

    r = await ev('from 0 to 2 count i \n i');
    assertEquals(r, 2);

    r = await ev('from 2 downto 0 count i \n i');
    assertEquals(r, 0);
});

Deno.test('[scripter] arrow function', async () => {

    let r = await ev('var f = () => true \n f()');
    assertEquals(r, true);

    r = await ev('var f = v => { return v } \n f(4)');
    assertEquals(r, 4);
});

Deno.test('[scripter] \'is\' operator', async () => {
    let r = await ev('1.b is null');
    assert(r);

    r = await ev('[] is empty');
    assert(r);

    r = await ev('[ 1 ] is array');
    assert(r);

    r = await ev('(a => 2) is fx');
    assert(r);

    r = await ev('false is boolean');
    assert(r);

    r = await ev('"abc".length is number');
    assert(r);

    r = await ev('"abc" is string');
    assert(r);
});

Deno.test('[scripter] ternary', async () => {
    let r = await ev('true ? 2');
    assertEquals(r, 2);

    r = await ev('true ?     1 & 2 ? 3 : 4');
    assertEquals(r, 3);

    r = await ev('0 | 1 ? 2 : 3');
    assertEquals(r, 2);
});

Deno.test('[scripter] exit statement', async () => {
    let r = await ev('exit 4');
    assert(!r);

    r = await ev('fx foobar(a) {\n exit 4 } foobar()');
    assert(!r);
});

*/