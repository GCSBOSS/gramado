
pub trait SourceReader {
    fn advance(&mut self) -> char;
    fn lookahead(&mut self, amount: usize) -> char;
    fn backtrack(&mut self);
    fn lines(&self, from: usize, to: usize) -> Vec<String>;
}

pub struct StringSourceReader {
    source: String,
    pub lines: Vec<String>,
    index: usize
}

impl StringSourceReader {

    pub fn new(source: &str) -> Self{
        StringSourceReader {
            index: 0,
            source: source.to_owned(),
            lines: source.lines().map(|s| s.to_owned()).collect()
        }
    }

}

impl SourceReader for StringSourceReader {

    fn advance(&mut self) -> char {
        self.index += 1;
        let c = self.source.chars().nth(self.index - 1);
        return if c.is_none() { '\x00' } else { c.unwrap() };
    }

    fn backtrack(&mut self){
        self.index -= 1;
    }

    fn lookahead(&mut self, amount: usize) -> char {
        let c = self.source.chars().skip(self.index + amount - 1).next();
        return if c.is_none() { '\x00' } else { c.unwrap() };
    }

    fn lines(&self, from: usize, to: usize) -> Vec<String> {
        self.lines.iter().skip(from).take(to - from).map(|s| s.clone()).collect()
    }

}

#[cfg(test)]
mod string_reader {
    use super::*;

    #[test]
    fn new() {
        let s = "abcd\nefg";
        let ssr = StringSourceReader::new(s);
        assert_eq!(ssr.lines.len(), 2);
    }

    #[test]
    fn advance() {
        let s = "abcd\nefg";
        let mut ssr = StringSourceReader::new(s);
        assert_eq!(ssr.advance(), 'a');
        assert_eq!(ssr.advance(), 'b');
    }

    #[test]
    fn backtrack() {
        let s = "abcd\nefg";
        let mut ssr = StringSourceReader::new(s);
        ssr.advance();
        ssr.advance();
        ssr.advance();
        ssr.backtrack();
        assert_eq!(ssr.advance(), 'c');
    }

    #[test]
    fn lookahead() {
        let s = "abcd\nefg";
        let mut ssr = StringSourceReader::new(s);
        ssr.advance();
        ssr.advance();
        ssr.backtrack();
        assert_eq!(ssr.lookahead(1), 'b');
        assert_eq!(ssr.advance(), 'b');
    }

}

use std::collections::VecDeque;
use std::io::prelude::*;
use std::fs::File;
use std::str;

pub struct FileSourceReader {
    line_buffer: String,
    pub lines: Vec<String>,
    file: File,
    pub bytes_read: usize,
    buffer: [u8; CHUNK_SIZE],
    done: bool,
    consumed: VecDeque<char>,
    queue: VecDeque<char>
}

const CHUNK_SIZE: usize = 2000; // bytes
const A: u8 = 0b10000000;
const C: u8 = 0b11100000;
const D: u8 = 0b11110000;
const E: u8 = 0b11111000;

fn get_char_len(byte: u8) -> &'static usize {
    if (byte & A) == 0b00000000 { return &0 };
    if (byte & C) == 0b11000000 { return &1 };
    if (byte & D) == 0b11100000 { return &2 };
    if (byte & E) == 0b11110000 { return &3 };
    panic!();
}

impl FileSourceReader {

    pub fn new(path: &str) -> Self{

        FileSourceReader {
            line_buffer: String::new(),
            bytes_read: 0,
            done: false,
            consumed: VecDeque::with_capacity(CHUNK_SIZE),
            queue: VecDeque::with_capacity(CHUNK_SIZE),
            lines: vec![],
            buffer: [0; CHUNK_SIZE],
            file: File::open(path).unwrap(),
        }
    }

    fn read(&mut self){

        if self.done {
            return;
        }

        let chunk_len = self.file.read(&mut self.buffer).unwrap();

        if chunk_len < CHUNK_SIZE {
            self.done = true;
        }

        let mut expected: usize;
        let mut last_byte: u8;
        let mut index: usize = 0;
        let mut byte: u8 = 0;

        while index < chunk_len {
            self.bytes_read += 1;
            last_byte = byte;
            byte = self.buffer[index];
            expected = *get_char_len(byte) + 1;

            if expected == 1 {

                // Ensure \n after \r don't disappear
                if last_byte == 13 && byte == 10 {
                    self.line_buffer.push('\n');
                }

                // Only pushes lines for \n if not following \r
                if byte == 13 || byte == 10 && last_byte != 13 {
                    self.lines.push(String::from(&self.line_buffer));
                    self.line_buffer = String::new();
                }
            }

            let remain_count = chunk_len - index;
            let missing_count: i8 = expected as i8 - remain_count as i8;
            let mut left_bytes = vec![];

            let bytes = if missing_count > 0 {

                // Ignore trailing if stream ended
                if self.done {
                    return;
                }

                // Read expected bytes
                let found_bytes = &mut [0; 4];
                if self.file.read(found_bytes).unwrap() != missing_count as usize {
                    return;
                }

                // Slice trailing remainder bytes
                left_bytes.copy_from_slice(&self.buffer[index..chunk_len]);
                left_bytes.extend_from_slice(&found_bytes[..missing_count as usize]);

                index = chunk_len;

                &left_bytes[0..]
            }
            else {
                &self.buffer[index..index + expected]
            };

            let s = str::from_utf8(bytes).unwrap();
            let ch = s.chars().next().unwrap();

            self.line_buffer.push(ch);
            self.queue.push_back(ch);
            index += expected;
        }

        // Add last line even if there's no break
        if self.done {
            self.lines.push(String::from(&self.line_buffer));
        }
    }

}

impl SourceReader for FileSourceReader {

    fn advance(&mut self) -> char {
        if self.queue.len() == 0 {
            self.read();
        }

        let c = match self.queue.pop_front() {
            Some(c) => c,
            None => '\x00'
        };

        self.consumed.push_back(c);
        return c;
    }

    fn backtrack(&mut self){
        self.queue.push_front(self.consumed.pop_back().unwrap());
    }

    fn lookahead(&mut self, amount: usize) -> char {
        if self.queue.len() < amount {
            self.read();
        }

        let i = self.queue.iter().skip(amount - 1).next();
        return match i {
            Some(c) => *c,
            None => '\x00'
        };
    }

    fn lines(&self, from: usize, to: usize) -> Vec<String> {
        self.lines.iter().skip(from).take(to - from).map(|s| s.clone()).collect()
    }
}

#[cfg(test)]
mod file_reader {
    use super::*;
    use std::fs;

    fn create_temp_file(text: &str) -> String{
        use std::iter;
        use rand::{Rng,thread_rng};
        use rand::distributions::Alphanumeric;

        let mut rng = thread_rng();
        let p: String = iter::repeat(()).map(|()| rng.sample(Alphanumeric))
            .map(char::from).take(7).collect();

        let s = p.to_owned();
        fs::write(p, text).unwrap();
        return s
    }

    #[test]
    fn new() {
        let p = create_temp_file("abc\r\ncde\nfgh");
        let mut fsr = FileSourceReader::new(p.as_str());
        fsr.advance();
        assert_eq!(fsr.lines.len(), 3);
        fs::remove_file(p).unwrap();
    }

    #[test]
    fn lookahead(){
        let p = create_temp_file("abc");
        let mut fsr = FileSourceReader::new(p.as_str());
        assert_eq!(fsr.lookahead(1), 'a');
        assert_eq!(fsr.lookahead(2), 'b');
        assert_eq!(fsr.lookahead(3), 'c');
        assert_eq!(fsr.lookahead(4), '\x00');
        fs::remove_file(p).unwrap();
    }

    #[test]
    fn advance(){
        let p = create_temp_file("abc");
        let mut fsr = FileSourceReader::new(p.as_str());
        fsr.advance();
        assert_eq!(fsr.advance(), 'b');
        fs::remove_file(p).unwrap();
    }

    #[test]
    fn backtrack(){
        let p = create_temp_file("abcdef");
        let mut fsr = FileSourceReader::new(p.as_str());
        fsr.advance();
        fsr.advance();
        fsr.advance();
        fsr.advance();
        fsr.backtrack();
        assert_eq!(fsr.lookahead(1), 'd');
        fs::remove_file(p).unwrap();
    }

    #[test]
    fn reading_emoji(){
        let p = create_temp_file("a = \r\n\"👍\"");
        let mut fsr = FileSourceReader::new(p.as_str());
        let mut s = String::new();
        let mut c = fsr.advance();
        while c != '\x00' {
            s.push(c);
            c = fsr.advance();
        }
        assert_eq!(s, "a = \r\n\"👍\"");
        fs::remove_file(p).unwrap();
    }

    // Deno.test('[file reader] reading invalid utf-8', async () => {
    //     const p = await Deno.makeTempFile();
    //     await Deno.writeFile(p, new Uint8Array([ 255, 254, 255, 254, 255, 254 ]));
    //     const r = new FileSourceReader(p);
    //     await assertThrowsAsync(() => r.advance());
    // });

    // Deno.test('[file reader] reading empty file', async () => {
    //     const p = await Deno.makeTempFile();
    //     const r = new FileSourceReader(p);
    //     await r.advance();
    // });

    // Deno.test('[file reader] bad trailing bits', async () => {
    //     const p = await Deno.makeTempFile();
    //     const r = new FileSourceReader(p);
    //     await Deno.writeTextFile(p, 'a = "👍"');
    //     await Deno.writeFile(p, new Uint8Array([ 243, 254 ]), { append: true });
    //     let s = await r.advance();
    //     let c = await r.advance();
    //     while(c){
    //         s += c;
    //         c = await r.advance();
    //     }
    //     assertEquals(s, 'a = "👍"');
    //     await Deno.remove(p);
    // });
}
